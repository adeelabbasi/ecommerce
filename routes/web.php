<?php

/* 
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();


Route::get('/', 'Home\HomeController@index');
Route::get('/faqs', 'Home\HomeController@faq');
Route::get('/contact', 'Home\HomeController@contact');
Route::get('/about', 'Home\HomeController@about');
Route::get('/terms-conditions', 'Home\HomeController@termsConditions');
Route::get('/privacy-policy', 'Home\HomeController@privacyPolicy');
Route::get('/returns-and-exchanges', 'Home\HomeController@returnsExchanges');

Route::post('/search-product/{search}', 'Home\ProductController@searchProduct');
Route::get('/product-category/{slug}', 'Home\ProductController@getCategoryProducts');
Route::get('/product/{slug}', 'Home\ProductController@getProductDetail');

Route::post('/remove-cart/{id}', 'Home\CartController@deleteCart');
Route::get('/cart', 'Home\CartController@index');
Route::post('/cart/{slug}', 'Home\CartController@addToCart');
Route::post('/update-cart', 'Home\CartController@updateCart');

Route::get('/country/cities/{CountryID}', 'Home\CheckoutController@getCities');
Route::resource('/checkout', 'Home\CheckoutController');

Route::post('payment', 'Home\PaymentController@getCheckout');
Route::get('paypal/payment/done', 'Home\PaymentController@getDone');
Route::get('paypal/payment/cancel', 'Home\PaymentController@getCancel');

Route::post('review', 'Home\ReviewController@store');

Route::get('/success', function () {
    return view('success');
});

Route::get('/fail', function () {
    return view('fail');
});

Route::group(['prefix' => 'cms'], function () {
  Route::get('/login', 'CmsAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'CmsAuth\LoginController@login');
  Route::post('/logout', 'CmsAuth\LoginController@logout')->name('logout');

  Route::get('/register', 'CmsAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'CmsAuth\RegisterController@register');

  Route::post('/password/email', 'CmsAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'CmsAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'CmsAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'CmsAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'cms', 'middleware' => 'admin'], function() {
    Route::resource('/', 'Cms\HomeController');
	
	Route::get('/showImportProducts', 'Cms\ProductController@showImportProducts');
	Route::post('/importProducts', 'Cms\ProductController@importProducts');
	Route::get('/product/grid', 'Cms\ProductController@grid');
	Route::resource('/product', 'Cms\ProductController');
	
	Route::get('/brand/grid/', 'Cms\BrandController@grid');
	Route::resource('/brand', 'Cms\BrandController');
	
	Route::get('/category/subcategory/{id}', 'Cms\CategoryController@categorySubcategories');
	Route::get('/category/grid', 'Cms\CategoryController@grid');
	Route::resource('category', 'Cms\CategoryController');
	
	Route::get('/subcategory/grid', 'Cms\SubcategoryController@grid');
	Route::resource('subcategory', 'Cms\SubcategoryController');
	
	//Route::post('/account/changepassword', 'Cms\AccountsController@resetPassword');
	//Route::get('/account/changepassword', 'Cms\AccountsController@showChangePassword');	
	
	Route::get('/order/grid/{type}', 'Cms\OrderController@grid');
	Route::get('/order/print/{id}', 'Cms\OrderController@printOrder');
	Route::resource('/order', 'Cms\OrderController');
		
	Route::post('/customer/export', 'Cms\CustomerController@exportSubscribers');
	Route::get('/customer/grid', 'Cms\CustomerController@grid');
	Route::resource('/customer', 'Cms\CustomerController');
});
