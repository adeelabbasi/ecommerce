<?php

namespace App\Imports;

use App\Models\Product_import;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
 


class ProductsImport implements ToModel, WithHeadingRow, WithBatchInserts, WithChunkReading 
{
    /**
     * @param array $row
     *
     * @return User|null
     */
    public function model(array $row)
    {
		if($row["title"]!="")
		{
			return new Product_import([
				'Title' 		=> utf8_encode($row["title"]),
				'Parent' 		=> utf8_encode($row["parent"]),
				'Slug' 			=> utf8_encode($row["slug"]),
				'SKU' 			=> utf8_encode($row["sku"]),
				'Description' 	=> utf8_encode($row["description"]),
				'Feature' 		=> utf8_encode($row["feature"]),
				'Image1' 		=> utf8_encode($row["image1"]),
				'Image2' 		=> utf8_encode($row["image2"]),
				'Image3' 		=> utf8_encode($row["image3"]),
				'Image4' 		=> utf8_encode($row["image4"]),
				'Image5' 		=> utf8_encode($row["image5"]),
				'Color' 		=> utf8_encode($row["color"]),
				'Size' 			=> utf8_encode($row["size"]),
				'Brand' 		=> utf8_encode($row["brand"]),
				'Category' 		=> utf8_encode($row["category"]),
				'Subcategory' 	=> utf8_encode($row["subcategory"]),
				'Quantity' 		=> utf8_encode($row["quantity"]),
				'Price' 		=> utf8_encode($row["price"]),
				'Price_2' 		=> utf8_encode($row["price_2"]),
				'ShippingCost' 	=> utf8_encode($row["shippingcost"]),
				'Warranty' 		=> utf8_encode($row["warranty"]),
				'Weight' 		=> utf8_encode($row["weight"]),
				'Dimension' 	=> utf8_encode($row["dimension"]),
				'Material' 		=> utf8_encode($row["material"]),
				'Manufacturer' 	=> utf8_encode($row["manufacturer"]),
				'Status' 		=> utf8_encode($row["status"]),
			]);
		}
    }
	
	public function batchSize(): int
    {
        return 1000;
    }
	
	public function chunkSize(): int
    {
        return 1000;
    }
}