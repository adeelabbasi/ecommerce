<?php
namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ProductRepository;
use Gloudemans\Shoppingcart\Cart;
use App\Models\Product;
use App\Models\State;
use App\Models\City;
use Illuminate\Support\Facades\Input;
use App\Models\Country;
use Response;
use URL;
use Auth;
use Session;

class CheckoutController extends Controller
{

    public function __construct()
    {
        
    }
	
	/**
     * Pass data to checkout view.
     * @return View
     */
    public function index()
    {
		$Countries = Country::all();
        return view('checkout', array('Countries' => $Countries));
    }
	
	public function getCities($CountryID)
    {
        $info_Cities = City::WhereIn('state_id',State::Where('country_id',$CountryID)->Select('id')->Get())->pluck("name","id");
		return response()->json($info_Cities);
    }
}