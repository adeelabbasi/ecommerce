<?php
namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\CategoryRepository;
use App\Repositories\ColorRepository;
use App\Repositories\BrandRepository;
use App\Repositories\SizeRepository;
use App\Repositories\ProductRepository;
use App\Http\Requests\ProductRequest;
use App\Models\Product;
use Maatwebsite\Excel\Excel;
use DataTables;
use URL;
use Auth;
use Session;
use Response;

class ProductController extends Controller
{
	protected $productRps;
	protected $categoryRps;
	protected $colorRps;
	protected $sizeRps;
	protected $brandRps;
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ProductRepository $productRps, CategoryRepository $categoryRps, ColorRepository $colorRps, SizeRepository $sizeRps, BrandRepository $brandRps)
    { 
        $this->productRps = $productRps;
		$this->categoryRps = $categoryRps;
		$this->colorRps = $colorRps;
		$this->sizeRps = $sizeRps;
		$this->brandRps = $brandRps;
    }
    
	public function getCategoryProducts(Request $request, $slug)
	{
		//return $request->all();
		$info_SubCategory = $this->categoryRps->getCategoryBySlug($slug);
		if($info_SubCategory)
		{
			$category_name = $info_SubCategory->category;
			$category_img = $info_SubCategory->m_img;
			
			$orderby=(isset($request->orderby)?$request->orderby:'');
			$filter_color=(isset($request->filter_color)?$request->filter_color:'');
			$filter_size=(isset($request->filter_size)?$request->filter_size:'');
			
			$price_from=(isset($request->price_from)?$request->price_from:'');
			$price_to=(isset($request->price_to)?$request->price_to:'');
			
			if($price_from!="")
			{
				if($price_from>=0 && $price_to<=1000)
				{
					$info_SubCategories = $this->categoryRps->getProductsSubCategoryCounts();
					$info_Products = $this->productRps->getProductByPrice($price_from, $price_to)->paginate(60)->withPath('?price_from=' . $price_from. '&price_to=' . $price_to);
					$info_Colors = $this->colorRps->getColorByPrice($price_from, $price_to);
					$info_Sizes = $this->sizeRps->getSizeByPrice($price_from, $price_to);
				}
				else
				{
					return "404";
				}
				
			}
			else
			if($filter_color!="")
			{
				$info_Color = $this->colorRps->getColor($filter_color);
				if($info_Color)
				{
					$info_SubCategories = $this->categoryRps->getProductsSubCategoryCounts();
					$info_Products = $this->productRps->getProductByCategoryColor($info_SubCategory->id, $filter_color)->paginate(60)->withPath('?filter_color=' . $filter_color);
					$info_Colors = $this->colorRps->getColorByCategorySingle($info_SubCategory->id, $filter_color);
					$info_Sizes = $this->sizeRps->getSizeByCategory($info_SubCategory->id);
				}
				else
				{
					return "404";
				}
				
			}
			else
			if($filter_size!="")
			{
				$info_Size = $this->sizeRps->getSize($filter_size);
				if($info_Size)
				{
					$info_SubCategories = $this->categoryRps->getProductsSubCategoryCounts();
					$info_Products = $this->productRps->getProductByCategorySize($info_SubCategory->id, $filter_size)->paginate(60)->withPath('?filter_size=' . $filter_size);
					$info_Colors = $this->colorRps->getColorByCategory($info_SubCategory->id);
					$info_Sizes = $this->sizeRps->getSizeByCategorySingle($info_SubCategory->id,$filter_size);
				}
				else
				{
					return "404";
				}
			}
			else
			if($orderby!="")
			{
				$info_SubCategories = $this->categoryRps->getProductsSubCategoryCounts();
				$info_Products = $this->productRps->getProductByCategory($info_SubCategory->id,$orderby)->paginate(60)->withPath('?orderby=' . $orderby);
				$info_Colors = $this->colorRps->getColorByCategory($info_SubCategory->id);
				$info_Sizes = $this->sizeRps->getSizeByCategory($info_SubCategory->id);
			}
			else
			{
				$info_SubCategories = $this->categoryRps->getProductsSubCategoryCounts();
				$info_Products = $this->productRps->getProductByCategory($info_SubCategory->id)->paginate(60);
				$info_Colors = $this->colorRps->getColorByCategory($info_SubCategory->id);
				$info_Sizes = $this->sizeRps->getSizeByCategory($info_SubCategory->id);
			}
			
			
			return view('category_detail',array('info_SubCategories' => $info_SubCategories, 'info_Products' => $info_Products, 'info_Colors' => $info_Colors, 'info_Sizes' => $info_Sizes), compact('slug', 'filter_color', 'filter_size', 'category_name', 'category_img'));
			
		}
		else
		{
			return "404";
		}
	}

	public function searchProduct(Request $request, $search)
	{
		$info_Products = $this->productRps->getProductSearch($search)->paginate(20);
		$searchData = array();
		$i=0;
		foreach($info_Products as $info_Product)
		{
			$searchData[$i] = array('title' => $info_Product->title , 'slug' => $info_Product->slug ,'image' => $info_Product->a_img, 'price' => $info_Product->price);
			$i++;
		}
        
		return Response::json([
            'searchData' => $searchData
        ], 200);
	}
	
    public function getProductDetail(Request $request, $slug)
	{
		$info_Product = $this->productRps->getProductBySlug($slug);
		$info_Products = $this->productRps->getProductByCategory($info_Product->sub_category_id)->paginate(8);
		return view('product_detail',array('info_Products' => $info_Products, 'info_Product' => $info_Product));
	}
}
