<?php
namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ProductRepository;
use Gloudemans\Shoppingcart\Cart;
use App\Models\Product;
use Illuminate\Support\Facades\Input;
use Response;
use URL;
use Auth;
use Session;

class CartController extends Controller
{
	protected $productRps;
	protected $cart;
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ProductRepository $productRps, Cart $cart)
    { 
		$this->cart = $cart;
        $this->productRps = $productRps;
    }
    
	public function index()
    { 
		return view('cart');
    }
	
    public function addToCart(Request $request, $slug)
	{
		$inputs = Input::all();
		
		$size = "";
		if(isset($inputs['size']))
		{
			$size = $inputs['size'];
		}
		$color = "";
		if(isset($inputs['color']))
		{
			$color = $inputs['color'];
		}
		
		$quantity = 1;
		if(isset($inputs['quantity']))
		{
			$quantity = $inputs['quantity'];
		}
		
		$info_Product = $this->productRps->getProductBySlug($slug);
		
		$cartData = $this->cart->add(['id' => $info_Product->id, 'name' => $info_Product->title, 'qty' => $quantity, 'price' => $info_Product->price , 'options' => ['slug' => $info_Product->slug ,'image' => $info_Product->a_img , 'size' => $size, 'color' => $color, 'shipping_cost' => $info_Product->shipping_cost, 'slug' => $info_Product->slug]])->associate('Product');
		
		$cartData = array();
		$i=0;
		foreach($this->cart->content() as $row)
		{
			$cartData[$i] = array('id' => $row->id , 'name' => $row->name , 'slug' => $row->options->slug ,'image' => $row->options->image , 'size' => $row->options->size , 'color' => $row->options->color , 'shipping_cost' => $row->options->shipping_cost ,'qty' => $row->qty , 'price' => $row->price);
			$i++;
		}
        
		return Response::json([
            'cartsData' => $cartData,
			'totalAmount' => $this->cart->subtotal(),
			'counts'  => $this->cart->count(),
        ], 200);
	}
	
	public function updateCart(Request $request)
	{				
		foreach($request->id as $index => $product_id)
		{
			if($request->quantity[$index]<0 || $request->quantity[$index]>10)
			{
				return redirect('cart');
			}
			
			$rowID = $this->cart->content()->where('id', $product_id)->first()->rowId;
			$cartData = $this->cart->update($rowID , $request->quantity[$index]);
		}
		return redirect('cart');
		$cartData = array();
		$i=0;
		foreach($this->cart->content() as $row)
		{
			$cartData[$i] = array('id' => $row->id , 'name' => $row->name , 'slug' => $row->options->slug ,'image' => $row->options->image , 'size' => $row->options->size , 'color' => $row->options->color , 'shipping_cost' => $row->options->shipping_cost ,'qty' => $row->qty , 'price' => $row->price);
			$i++;
		}
        
		return Response::json([
            'cartsData' => $cartData,
			'totalAmount' => $this->cart->subtotal(),
			'counts'  => $this->cart->count(),
        ], 200);
	}
	
	public function deleteCart($product_id)
	{			
		$rowID = $this->cart->content()->where('id', $product_id)->first()->rowId;
		$cartData = $this->cart->remove($rowID);
		$cartData = array();
		$i=0;
		foreach($this->cart->content() as $row)
		{
			$cartData[$i] = array('id' => $row->id , 'name' => $row->name , 'slug' => $row->options->slug ,'image' => $row->options->image , 'size' => $row->options->size , 'color' => $row->options->color , 'shipping_cost' => $row->options->shipping_cost ,'qty' => $row->qty , 'price' => $row->price);
			$i++;
		}
        
		return Response::json([
            'cartsData' => $cartData,
			'totalAmount' => $this->cart->subtotal(),
			'counts'  => $this->cart->count(),
        ], 200);
	}
}
