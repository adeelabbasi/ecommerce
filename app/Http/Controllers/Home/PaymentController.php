<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use PayPal;
use Redirect;
use Illuminate\Http\Request;
use App\Repositories\OrderRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\UserRepository;
use Gloudemans\Shoppingcart\Cart;
use Stripe\Stripe;
use Stripe\Charge;
use Session;

class PaymentController extends Controller
{
    private $_apiContext;
	protected $orderRps;
	protected $orderDetailRps;
	protected $userRps;
	
    public function __construct(OrderRepository $orderRps, OrderDetailRepository $orderDetailRps, UserRepository $userRps)
    {
		$this->orderRps = $orderRps;
		$this->orderDetailRps = $orderDetailRps;
		$this->userRps = $userRps;
       	$this->_apiContext = PayPal::ApiContext(
            config('services.paypal.client_id'),
            config('services.paypal.secret'));
		
		$this->_apiContext->setConfig(array(
			'mode' => 'sandbox',
			'service.EndPoint' => 'https://api.sandbox.paypal.com',
			'http.ConnectionTimeOut' => 30,
			'log.LogEnabled' => true,
			'log.FileName' => storage_path('logs/paypal.log'),
			'log.LogLevel' => 'FINE'
		));

    }
	
    /**
     * @param Cart $cart
     * @return mixed
     */
    public function getCheckout(Request $request, Cart $cart)
    {
		//return $request->all();
		$content = $cart->content();
		
		$info_User = $this->userRps->getUserbyEmail($request->email);
		if($info_User)
		{
			$info_User = $this->userRps->updateUser($request->all(), $info_User->id);
		}
		else
		{
			$info_User = $this->userRps->addUser($request->all());
		}
		
		$OrderArr = array('user_id' => $info_User->id, 'amount' => $cart->subtotal(), 'items' => $cart->count(),  'status' => 0);
		$info_Order = $this->orderRps->addOrder($OrderArr);
		
		$shipping_cost = 0;
        //dd($cart);
		$shipping_amount = 0;
		$productIDs = "";
        foreach ($content as $item) 
		{
			$productIDs = "Product ID: ". $item->id.", ";
			$shipping_amount += $item->options->shipping_cost;
			$OrderDetailArr = array('order_id' => $info_Order->id, 'product_id' => $item->id, 'size' => $item->options->size, 'img' => $item->options->image, 'color' => $item->options->color, 'quantity' => $item->qty, 'amount' => ($item->price + $item->options->shipping_cost) );
			$info_OrderDetail = $this->orderDetailRps->addOrderDetail($OrderDetailArr);
        }
		
		if($request->paymentType==1)
		{
			Session::put('Order_Key', $info_Order->id);

			$grand_total = $shipping_amount + $cart->subtotal();
			
			$payer = PayPal::Payer();
			$payer->setPaymentMethod('paypal');
		
			$amount = PayPal:: Amount();
			$amount->setCurrency('USD');
			$amount->setTotal($grand_total); // This is the simple way,
			// you can alternatively describe everything in the order separately;
			// Reference the PayPal PHP REST SDK for details.
		
			$transaction = PayPal::Transaction();
			$transaction->setAmount($amount);
			$transaction->setDescription($productIDs);
		
			$redirectUrls = PayPal:: RedirectUrls();
			$redirectUrls->setReturnUrl(url('payment/done'));
			$redirectUrls->setCancelUrl(url('payment/cancel'));
		
			$payment = PayPal::Payment();
			$payment->setIntent('sale');
			$payment->setPayer($payer);
			$payment->setRedirectUrls($redirectUrls);
			$payment->setTransactions(array($transaction));
		
			$response = $payment->create($this->_apiContext);
			$redirectUrl = $response->links[1]->href;
			
			return Redirect::to( $redirectUrl );
		}
		else
		{
			Stripe::setApiKey(env('STRIPE_SECRET_KEY'));
			try 
			{
				$charge = Charge::create ( array (
					"amount" => (($cart->subtotal() + $shipping_amount)*100),
					"currency" => "USD",
					"source" => $request->stripeToken, // obtained with Stripe.js
					"description" => $info_User->email,
				));
				//Update order status
				$info_Order->status=1;
				$info_Order->save();
				$content = $cart->destroy();
				return redirect('success');
			} 
			catch ( \Exception $e ) 
			{
				$this->orderRps->deleteOrder($info_Order->id);
				$this->orderDetailRps->deleteOrderDetailByOrder($info_Order->id);
				return redirect('fail');
			}	
		}
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDone()
    {
        $id = request()->get('paymentId');
        $token = request()->get('token');
        $payer_id = request()->get('PayerID');

        $payment = PayPal::getById($id, $this->_apiContext);

        $paymentExecution = PayPal::PaymentExecution();

        $paymentExecution->setPayerId($payer_id);
        $result = $payment->execute($paymentExecution, $this->_apiContext);
        //customer info
        $payer = $result->getPayer()
            ->getPayerInfo();
        $data['email'] = $payer->email;
        $data['first_name'] = $payer->first_name;
        $data['last_name'] = $payer->last_name;
        
		//Update order status
		$Order_Key = Session::get('Order_Key');
		$info_Order = $this->orderRps->getOrder($Order_Key);
		$info_Order->status=1;
		$info_Order->save();
		//transaction info
        /*$transaction = $result->getTransactions();
        dd($transaction);
        //    ->getAmount();
        $data['total'] = $transaction->total;
        $data['currency'] = $transaction->currency;*/
        //dd($customer->email);

        // Clear the shopping cart, write to database, send notifications, etc.
        //$cart->instance(auth()->id())->destroy();
        //event(new ForgetSession($request));
		$content = $cart->destroy();
        return redirect('success');
    }

    /**
     * @return Redirect
     */
    public function getCancel()
    {
        // Curse and humiliate the user for cancelling this most sacred payment (yours)
        $Order_Key = Session::get('Order_Key');
		$this->orderRps->deleteOrder($Order_Key);
		$this->orderDetailRps->deleteOrderDetailByOrder($Order_Key);
		return redirect('fail');
    }

    /**
     * @return mixed
     */
    public function createWebProfile()
    {
        //$flowConfig = PayPal::FlowConfig();
        $presentation = PayPal::Presentation();
        //$inputFields = PayPal::InputFields();
        $webProfile = PayPal::WebProfile();
        //$flowConfig->setLandingPageType("Billing"); //Set the page type
        //NB: Paypal recommended to use https for the logo's address and the size set to 190x60.
        $presentation->setLogoImage(asset('/images/payment.jpg'))->setBrandName(config('app.name'));
        //$inputFields->setAllowNote(true)->setNoShipping(1)->setAddressOverride(1);
        $webProfile->setName(config('app.name') . uniqid())
            //->setFlowConfig($flowConfig)
            // Parameters for style and presentation.
            ->setPresentation($presentation);
        // Parameters for input field customization.
        //->setInputFields($inputFields);
        $createProfileResponse = $webProfile->create($this->_apiContext);
        return $createProfileResponse->getId(); //The new webprofile's id
    }
}