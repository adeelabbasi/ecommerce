<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {	
		$review = new Review();	
		$review->name = $request->name;
		$review->product_id = $request->pid;
		$review->rating = $request->rating;
		$review->message = $request->message;
		$review->save();
		return redirect()->back();
    }
}
