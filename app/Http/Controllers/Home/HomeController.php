<?php
namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	//
    }

    public function index()
    {
        return view('welcome');
    }
	
	public function faq()
    {
        return view('faq');
    }
	
	public function about()
    {
        return view('about');
    }
	
	public function contact()
    {
        return view('contact');
    }
	
	public function termsConditions()
    {
        return view('terms_conditions');
    }
	
	public function privacyPolicy()
    {
        return view('privacy_policy');
    }
	
	public function returnsExchanges()
    {
        return view('returns_exchanges');
    }
}
