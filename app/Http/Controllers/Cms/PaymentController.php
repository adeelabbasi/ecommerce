<?php
namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\PaymentRepository;
use App\Http\Requests\PaymentRequest;
use App\Models\Payment;
use DataTables;
use URL;
use Auth;
use Session;

class PaymentController extends Controller
{
	protected $paymentRps;
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PaymentRepository $paymentRps)
    { 
		$this->middleware('auth');
        $this->paymentRps = $paymentRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Cms.Payment.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Cms.Payment.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$this->paymentRps->addPayment($request->all());
		Session::flash('flash_message', 'Payment successfully added!');
		return view('Cms.Payment.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $info_Payment = $this->paymentRps->getPayment($id);
		return view('Cms.Payment.edit' ,array('info_Payment' => $info_Payment));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $info_Payment = $this->paymentRps->getPayment($id);
		return view('Cms.Payment.edit' ,array('info_Payment' => $info_Payment));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(PaymentRequest $request, $id)
    {
        $this->paymentRps->updatePayment($request->all() , $id);
		Session::flash('flash_message', 'Payment successfully updated!');
		return view('Cms.Payment.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->paymentRps->deletePayment($id);
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   $info_Payment = $this->paymentRps->getPayment();
	   return Datatables::of($info_Payment)
		->addColumn('edit', function ($info_Payment) {
				 return '<div class="btn-group btn-group-action">
								<a class="btn btn-info" style="margin-right:2px;" href="'.url('/Cms/Payment/'.$info_Payment->id.'/edit').'" title="Edit Data"><i class="fa fa-pencil"></i></a> 
								<a class="btn btn-danger" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-remote="/Cms/Payment/' . $info_Payment->id . '"><i class="fa fa-trash"></i></a>
						</div>';
        })
		->editColumn('id', 'JOB-{{ $id }}')
		->escapeColumns([])
		->make(true);
    }
}
