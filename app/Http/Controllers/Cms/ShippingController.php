<?php
namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ShippingRepository;
use App\Http\Requests\ShippingRequest;
use App\Models\Shipping;
use DataTables;
use URL;
use Auth;
use Session;

class ShippingController extends Controller
{
	protected $shippingRps;
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ShippingRepository $shippingRps)
    { 
		$this->middleware('auth');
        $this->shippingRps = $shippingRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Cms.Shipping.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Cms.Shipping.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$this->shippingRps->addShipping($request->all());
		Session::flash('flash_message', 'Shipping successfully added!');
		return view('Cms.Shipping.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Shipping  $shipping
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $info_Shipping = $this->shippingRps->getShipping($id);
		return view('Cms.Shipping.edit' ,array('info_Shipping' => $info_Shipping));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Shipping  $shipping
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $info_Shipping = $this->shippingRps->getShipping($id);
		return view('Cms.Shipping.edit' ,array('info_Shipping' => $info_Shipping));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shipping  $shipping
     * @return \Illuminate\Http\Response
     */
    public function update(ShippingRequest $request, $id)
    {
        $this->shippingRps->updateShipping($request->all() , $id);
		Session::flash('flash_message', 'Shipping successfully updated!');
		return view('Cms.Shipping.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shipping  $shipping
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->shippingRps->deleteShipping($id);
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   $info_Shipping = $this->shippingRps->getShipping();
	   return Datatables::of($info_Shipping)
		->addColumn('edit', function ($info_Shipping) {
				 return '<div class="btn-group btn-group-action">
								<a class="btn btn-info" style="margin-right:2px;" href="'.url('/Cms/Shipping/'.$info_Shipping->id.'/edit').'" title="Edit Data"><i class="fa fa-pencil"></i></a> 
								<a class="btn btn-danger" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-remote="/Cms/Shipping/' . $info_Shipping->id . '"><i class="fa fa-trash"></i></a>
						</div>';
        })
		->editColumn('id', 'JOB-{{ $id }}')
		->escapeColumns([])
		->make(true);
    }
}
