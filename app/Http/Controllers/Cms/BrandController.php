<?php
namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\BrandRepository;
use App\Models\Brand;
use DataTables;
use URL;
use Auth;
use Session;

class BrandController extends Controller
{
	protected $brandRps;
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(BrandRepository $brandRps)
    { 
        $this->brandRps = $brandRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cms.brand.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cms.brand.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$this->validate($request, [
            'brand' => 'required|max:32',
        ]);
		$this->brandRps->addBrand($request->all());
		Session::flash('flash_message', 'Brand successfully added!');
		return view('cms.brand.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $info_Brand = $this->brandRps->getBrand($id);
		return view('cms.brand.edit' ,array('info_Brand' => $info_Brand));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $info_Brand = $this->brandRps->getBrand($id);
		return view('cms.brand.edit' ,array('info_Brand' => $info_Brand));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$this->validate($request, [
            'brand' => 'required|max:32',
        ]);
        $this->brandRps->updateBrand($request->all() , $id);
		Session::flash('flash_message', 'Brand successfully updated!');
		return view('cms.brand.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->brandRps->deleteBrand($id);
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   $info_Brand = $this->brandRps->getBrand();
	   return Datatables::of($info_Brand)
		->addColumn('edit', function ($info_Brand) {
				 return '<div class="btn-group btn-group-action">
								<a class="btn btn-info" style="margin-right:2px;" href="'.url('/cms/brand/'.$info_Brand->id.'/edit').'" title="Edit Data"><i class="fa fa-pencil"></i></a> 
								<a class="btn btn-danger" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-remote="/cms/brand/' . $info_Brand->id . '"><i class="fa fa-trash"></i></a>
						</div>';
        })
		->editColumn('id', 'JOB-{{ $id }}')
		->escapeColumns([])
		->make(true);
    }
}
