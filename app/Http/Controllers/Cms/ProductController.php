<?php
namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\CategoryRepository;
use App\Repositories\ColorRepository;
use App\Repositories\BrandRepository;
use App\Repositories\SizeRepository;
use App\Repositories\ProductRepository;
use App\Http\Requests\ProductRequest;
use App\Models\Product;
use App\Models\Product_import;
use Illuminate\Support\Facades\Input;
use App\Jobs\UploadExcelJob;
use DataTables;
use URL;
use Auth;
use Session;
use Redirect;

class ProductController extends Controller
{
	protected $productRps;
	protected $categoryRps;
	protected $colorRps;
	protected $sizeRps;
	protected $brandRps;
	protected $excel;
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ProductRepository $productRps, CategoryRepository $categoryRps, ColorRepository $colorRps, SizeRepository $sizeRps, BrandRepository $brandRps)
    { 
        $this->productRps = $productRps;
		$this->categoryRps = $categoryRps;
		$this->colorRps = $colorRps;
		$this->sizeRps = $sizeRps;
		$this->brandRps = $brandRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$info_Products = $this->productRps->getProduct()->paginate(10);
        return view('cms.product.index', array('info_Products' => $info_Products));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$info_Categories = $this->categoryRps->getCategory();
		$info_Colors = $this->colorRps->getColor();
		$info_Sizes = $this->sizeRps->getSize();
		$info_Brands = $this->brandRps->getBrand();
		return view('cms.product.add' ,array('info_Categories' => $info_Categories, 'info_Colors' => $info_Colors, 'info_Sizes' => $info_Sizes, 'info_Brands' => $info_Brands));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
		//return $request->all();
		$this->productRps->addProduct($request->all());
		Session::flash('flash_message', 'Product successfully added!');
		return redirect('/cms/product');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $info_Product = $this->productRps->getProduct($id);
		return view('cms.product.edit' ,array('info_Product' => $info_Product));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $info_Product = $this->productRps->getProduct($id);
		$info_Product->color_if_any = $info_Product->Color()->Get();
		$info_Product->size_if_any = $info_Product->Size()->Get();
		$info_Categories = $this->categoryRps->getCategory();
		$info_SubCategories = $this->categoryRps->getCategorySubCategories($info_Product->category_id);
		$info_Colors = $this->colorRps->getColor();
		$info_Sizes = $this->sizeRps->getSize();
		$info_Brands = $this->brandRps->getBrand();
		return view('cms.product.edit' ,array('info_Product' => $info_Product, 'info_Categories' => $info_Categories, 'info_Colors' => $info_Colors, 'info_Sizes' => $info_Sizes, 'info_Brands' => $info_Brands, 'info_SubCategories' => $info_SubCategories));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        $this->productRps->updateProduct($request->all() , $id);
		Session::flash('flash_message', 'Product successfully updated!');
		return redirect('/cms/product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->productRps->deleteProduct($id);
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   $info_Product = $this->productRps->getProduct()->paginate(20);
	   return Datatables::of($info_Product)
		->editColumn('id', '<table><tr><td style="width:30px"><input type="checkbox" name="id[]" value="{{$id}}"></td><td></td></tr></table>')
		->addColumn('edit', function ($info_Product) {
				 return '<div class="btn-group btn-group-action">
								<a class="btn btn-info" style="margin-right:2px;" href="'.url('/SuperAdmin/Product/'.$info_Product->id.'/edit').'" title="Edit Data"><i class="fa fa-pencil"></i></a> 
								<a class="btn btn-danger" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-remote="/SuperAdmin/Product/' . $info_Product->id . '"><i class="fa fa-trash"></i></a>
						</div>';
        })
		->setTotalRecords(20)
		->escapeColumns([])
		->make(true);
    }
	
	public function showImportProducts()
	{	
		return view('cms.product.bulk');
	}
	
	
	public function importProducts(Request $request)
	{
		//try
		{
			if($request->file('importProducts'))
			{
				$filePath = $request->file('importProducts')->store('excels');
				UploadExcelJob::dispatch($filePath);				
				Session::flash('flash_message', 'Products successfully added!');
				return redirect('/cms/product');
			}
			return Redirect::back()->withErrors(['Invalid File']);
		}
		//catch(\Exception $e)
		//{
			
			//return Redirect::back()->withErrors(['Invalid File']);
		//}
	}
}
