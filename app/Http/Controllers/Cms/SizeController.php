<?php
namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\SizeRepository;
use App\Http\Requests\SizeRequest;
use App\Models\Size;
use DataTables;
use URL;
use Auth;
use Session;

class SizeController extends Controller
{
	protected $sizeRps;
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SizeRepository $sizeRps)
    { 
		$this->middleware('auth');
        $this->sizeRps = $sizeRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Cms.Size.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Cms.Size.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$this->sizeRps->addSize($request->all());
		Session::flash('flash_message', 'Size successfully added!');
		return view('Cms.Size.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Size  $size
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $info_Size = $this->sizeRps->getSize($id);
		return view('Cms.Size.edit' ,array('info_Size' => $info_Size));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Size  $size
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $info_Size = $this->sizeRps->getSize($id);
		return view('Cms.Size.edit' ,array('info_Size' => $info_Size));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Size  $size
     * @return \Illuminate\Http\Response
     */
    public function update(SizeRequest $request, $id)
    {
        $this->sizeRps->updateSize($request->all() , $id);
		Session::flash('flash_message', 'Size successfully updated!');
		return view('Cms.Size.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Size  $size
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->sizeRps->deleteSize($id);
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   $info_Size = $this->sizeRps->getSize();
	   return Datatables::of($info_Size)
		->addColumn('edit', function ($info_Size) {
				 return '<div class="btn-group btn-group-action">
								<a class="btn btn-info" style="margin-right:2px;" href="'.url('/Cms/Size/'.$info_Size->id.'/edit').'" title="Edit Data"><i class="fa fa-pencil"></i></a> 
								<a class="btn btn-danger" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-remote="/Cms/Size/' . $info_Size->id . '"><i class="fa fa-trash"></i></a>
						</div>';
        })
		->editColumn('id', 'JOB-{{ $id }}')
		->escapeColumns([])
		->make(true);
    }
}
