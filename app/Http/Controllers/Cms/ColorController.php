<?php
namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ColorRepository;
use App\Http\Requests\ColorRequest;
use App\Models\Color;
use DataTables;
use URL;
use Auth;
use Session;

class ColorController extends Controller
{
	protected $colorRps;
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ColorRepository $colorRps)
    { 
		$this->middleware('auth');
        $this->colorRps = $colorRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Cms.Color.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Cms.Color.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$this->colorRps->addColor($request->all());
		Session::flash('flash_message', 'Color successfully added!');
		return view('Cms.Color.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $info_Color = $this->colorRps->getColor($id);
		return view('Cms.Color.edit' ,array('info_Color' => $info_Color));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $info_Color = $this->colorRps->getColor($id);
		return view('Cms.Color.edit' ,array('info_Color' => $info_Color));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function update(ColorRequest $request, $id)
    {
        $this->colorRps->updateColor($request->all() , $id);
		Session::flash('flash_message', 'Color successfully updated!');
		return view('Cms.Color.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->colorRps->deleteColor($id);
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   $info_Color = $this->colorRps->getColor();
	   return Datatables::of($info_Color)
		->addColumn('edit', function ($info_Color) {
				 return '<div class="btn-group btn-group-action">
								<a class="btn btn-info" style="margin-right:2px;" href="'.url('/Cms/Color/'.$info_Color->id.'/edit').'" title="Edit Data"><i class="fa fa-pencil"></i></a> 
								<a class="btn btn-danger" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-remote="/Cms/Color/' . $info_Color->id . '"><i class="fa fa-trash"></i></a>
						</div>';
        })
		->editColumn('id', 'JOB-{{ $id }}')
		->escapeColumns([])
		->make(true);
    }
}
