<?php
namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\CategoryRepository;
use App\Models\Category;
use DataTables;
use URL;
use Auth;
use Session;

class SubcategoryController extends Controller
{
	protected $categoryRps;
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CategoryRepository $categoryRps)
    { 
        $this->categoryRps = $categoryRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cms.subcategory.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $info_Categories = $this->categoryRps->getCategory();
		return view('cms.subcategory.add' ,array('info_Categories' => $info_Categories));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$this->validate($request, [
            'category' => 'required|max:32',
			'parent_id' => 'required|integer',
        ]);
		$this->categoryRps->addCategory($request->all());
		Session::flash('flash_message', 'Subcategory successfully added!');
		return view('cms.subcategory.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$info_Categories = $this->categoryRps->getCategory();
        $info_Category = $this->categoryRps->getSubCategory($id);
		return view('cms.subcategory.edit' ,array('info_Category' => $info_Category, 'info_Categories' => $info_Categories));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $info_Categories = $this->categoryRps->getCategory();
        $info_Category = $this->categoryRps->getSubCategory($id);
		return view('cms.subcategory.edit' ,array('info_Category' => $info_Category, 'info_Categories' => $info_Categories));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$this->validate($request, [
            'category' => 'required|max:32',
			'parent_id' => 'required|integer',
        ]);
        $this->categoryRps->updateCategory($request->all() , $id);
		Session::flash('flash_message', 'Subcategory successfully updated!');
		return view('cms.subcategory.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->categoryRps->deleteCategory($id);
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   $info_Category = $this->categoryRps->getSubCategory();
	   return Datatables::of($info_Category)
		->editColumn('parent_id', function ($info_Category) {
			return $info_Category->Parent()->First()->category;
        })
		->addColumn('edit', function ($info_Category) {
				 return '<div class="btn-group btn-group-action">
								<a class="btn btn-info" style="margin-right:2px;" href="'.url('/cms/subcategory/'.$info_Category->id.'/edit').'" title="Edit Data"><i class="fa fa-pencil"></i></a> 
								<a class="btn btn-danger" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-remote="/cms/subcategory/' . $info_Category->id . '"><i class="fa fa-trash"></i></a>
						</div>';
        })
		->editColumn('id', 'JOB-{{ $id }}')
		->escapeColumns([])
		->make(true);
    }
}
