<?php
namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\CustomerRepository;
use App\Http\Requests\CustomerRequest;
use App\Models\Customer;
use DataTables;
use URL;
use Auth;
use Session;

class CustomerController extends Controller
{
	protected $customerRps;
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CustomerRepository $customerRps)
    { 
		$this->middleware('auth');
        $this->customerRps = $customerRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Cms.Customer.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Cms.Customer.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$this->customerRps->addCustomer($request->all());
		Session::flash('flash_message', 'Customer successfully added!');
		return view('Cms.Customer.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $info_Customer = $this->customerRps->getCustomer($id);
		return view('Cms.Customer.edit' ,array('info_Customer' => $info_Customer));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $info_Customer = $this->customerRps->getCustomer($id);
		return view('Cms.Customer.edit' ,array('info_Customer' => $info_Customer));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerRequest $request, $id)
    {
        $this->customerRps->updateCustomer($request->all() , $id);
		Session::flash('flash_message', 'Customer successfully updated!');
		return view('Cms.Customer.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->customerRps->deleteCustomer($id);
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   $info_Customer = $this->customerRps->getCustomer();
	   return Datatables::of($info_Customer)
		->addColumn('edit', function ($info_Customer) {
				 return '<div class="btn-group btn-group-action">
								<a class="btn btn-info" style="margin-right:2px;" href="'.url('/Cms/Customer/'.$info_Customer->id.'/edit').'" title="Edit Data"><i class="fa fa-pencil"></i></a> 
								<a class="btn btn-danger" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-remote="/Cms/Customer/' . $info_Customer->id . '"><i class="fa fa-trash"></i></a>
						</div>';
        })
		->editColumn('id', 'JOB-{{ $id }}')
		->escapeColumns([])
		->make(true);
    }
}
