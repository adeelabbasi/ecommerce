<?php
namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\OrderRepository;
use App\Repositories\ProductRepository;

class HomeController extends Controller
{
    protected $orderRps;
	protected $productRps;
	
	/**
     * Create a new controller instance.
     *
     * @return void
     */
	public function __construct(OrderRepository $orderRps , ProductRepository $productRps)
    {    
		$this->orderRps = $orderRps;
		$this->productRps = $productRps;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$info_Orders = $this->orderRps->getPendingOrder();
		if($info_Orders)
		{
			$orderCounts = $info_Orders->count();
			session()->put('orders', $orderCounts);
		}
		else
		{
			$orderCounts = 0;
			session()->put('orders', $orderCounts);
		}
        return view('cms.home');
    }
}
