<?php
namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\OrderRepository;
use App\Http\Requests\OrderRequest;
use App\Models\Order;
use DataTables;
use URL;
use Auth;
use Session;

class OrderController extends Controller
{
	protected $orderRps;
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(OrderRepository $orderRps)
    { 
        $this->orderRps = $orderRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cms.order.index');
    }

    

    
    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $info_Order = $this->orderRps->getOrder($id);
		return view('cms.order.show' ,array('info_Order' => $info_Order));
    }
	
	
	public function printOrder($id)
    {
		$info_Order = $this->orderRps->getOrder($id);
		return view('cms.order.print' ,array('info_Order' => $info_Order));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
	{
		$this->orderRps->updateOrder($request->all() , $id);
		
		$info_Orders = $this->orderRps->getPendingOrder();
		if($info_Orders)
		{
			$orderCounts = $info_Orders->count();
			session()->put('orders', $orderCounts);
		}
		else
		{
			$orderCounts = 0;
			session()->put('orders', $orderCounts);
		}
		return $orderCounts;
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->orderRps->deleteOrder($id);
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid($type)
    {
		if($type=='all')
		{
			$info_Orders = $this->orderRps->getOrder();
		}
		elseif($type=='pending')
		{
	   		$info_Orders = $this->orderRps->getOrder()->where('status' , '=' , '1');
		}
		elseif($type=='shipped')
		{
	   		$info_Orders = $this->orderRps->getOrder()->where('status' , '=' , '2');
		}
		elseif($type=='return')
		{
	   		$info_Orders = $this->orderRps->getOrder()->where('status' , '=' , '3');
		}
		elseif($type=='delivered')
		{
	   		$info_Orders = $this->orderRps->getOrder()->where('status' , '=' , '4');
		}
		elseif($type=='canceled')
		{
	   		$info_Orders = $this->orderRps->getOrder()->where('status' , '=' , '9');
		}
		return Datatables::of($info_Orders)
		->editColumn('status', '
								
					@if($status==1)
					  <select class="select" id="status" name="status" data-id="{{$id}}">
						<option value="1" selected><span class="label label-warning">Pending</span></option>
						<option value="2"><span class="label label-success">Shipped</span></option>
						<option value="9"><span class="label label-success">Canceled</span></option>
					  </select>
					@endif
					
					@if($status==2)
					  <select class="select" id="status" name="status" data-id="{{$id}}">
						<option value="2" selected><span class="label label-success">Shipped</span></option>
						<option value="3"><span class="label label-primary">Return</span></option>
						<option value="4"><span class="label label-primary">Delivered & Cash Received</span></option>
						<option value="9"><span class="label label-success">Canceled</span></option>
					  </select>
					@endif
					
					@if($status==3)
						<span class="label label-danger">Return</span>
					@endif
					
					@if($status==4)
						<span class="label label-success">Delivered & Cash Received</span>
					@endif
					
					@if($status==9)
						<span class="label label-danger">Canceled</span>
					@endif
					
					')
		->editColumn('id', '{{$id}} <a href="{{ url("/cms/order",[$id]) }}" >Detail</a>')
		->addColumn('itemcounts', function ($info_Orders) {
				 return $info_Orders->items;
        })
		->addColumn('name', function ($info_Orders) {
				 return $info_Orders->User()->First()->first_name;
        })
		->addColumn('phone', function ($info_Orders) {
				 return $info_Orders->User()->First()->phone;
        })
		->escapeColumns([])
		->make(true);
    }
}
