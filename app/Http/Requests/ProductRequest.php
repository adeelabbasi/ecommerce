<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:200',
			'slug' => 'required|max:200',
			'sku' => 'required|max:200',
			'description' => 'required|max:1000',
			'feature' => 'required|max:1000',
			'price' => 'required|numeric|max:100000',
			'price_2' => 'nullable|numeric|max:100000',
			'warranty' => 'nullable|max:200',
			'quantity' => 'nullable|numeric|max:100000',
			'weight' => 'nullable|max:200',
			'brand_id' => 'nullable|integer',
			'width' => 'nullable|integer',
			'category_id' => 'required|integer',
			'sub_category_id' => 'required|integer',
			'color_if_any' => 'nullable|max:200',
			'size_if_any' => 'nullable|max:200',
			'dimension' => 'nullable|max:255',
			'material' => 'nullable|max:255',
			'manufacturer' => 'nullable|max:255'
        ];
    }
}
