<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\ProductRepository;
use App\Models\Product_import;

class UploadProducts extends Command
{
	protected $productRps;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upload:products';
	
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload products';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ProductRepository $productRps)
    {
		
        parent::__construct();
		$this->productRps = $productRps;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $info_Product_Imports = Product_import::Take(50)->get();	
		foreach($info_Product_Imports as $info_Product_Import)
		{
			$inputs = [
					"title" => $info_Product_Import->Title,
					"slug" => $info_Product_Import->Slug,
					"category_id" => $info_Product_Import->Category,
					"sub_category_id" => $info_Product_Import->Subcategory,
					"price" => $info_Product_Import->Price,
					"price_2" => $info_Product_Import->Price_2,
					"shipping_cost" => $info_Product_Import->ShippingCost,
					"brand_id" => $info_Product_Import->Brand,
					"quantity" => $info_Product_Import->Quantity,
					"warranty" => $info_Product_Import->Warranty,
					"weight" => $info_Product_Import->Weight,
					"sku" => $info_Product_Import->SKU,
					"dimension" => $info_Product_Import->Dimension,
					"material" => $info_Product_Import->Material,
					"manufacturer" => $info_Product_Import->Manufacturer,
					"parent" => $info_Product_Import->Parent,
					"status" => $info_Product_Import->Status,
					"color_if_any" => explode("|",$info_Product_Import->Color),
					"size_if_any" => explode("|",$info_Product_Import->Size),
					"description" => $info_Product_Import->Description,
					"feature" => $info_Product_Import->Feature,
					"a_img" => $info_Product_Import->Image1,
					"b_img" => $info_Product_Import->Image2,
					"c_img" => $info_Product_Import->Image3,
					"d_img" => $info_Product_Import->Image4,
					"e_img" => $info_Product_Import->Image5,
					];
			$this->productRps->storeProductExcel($inputs);			
			$info_Product_Import->delete();
		}
    }
}
