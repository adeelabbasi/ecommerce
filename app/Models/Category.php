<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';
	public $timestamps = false;
	
    protected $fillable = [
        'category', 'parent_id', 'm_img', 'f_img'
    ];
	
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public function Product()
    {
        return $this->hasMany('App\Models\Product');
    }

    /**
     * Get parent category
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Parent()
    {
        return $this->belongsTo('App\Models\Category', 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Children()
    {
        return $this->hasMany('App\Models\Category', 'parent_id');
    }
}