<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_color extends Model
{
/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_colors';
	public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'product_id', 'color_id'
    ];
	
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}