<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_size extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_sizes';
	public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'product_id', 'size_id'
    ];
    
    public function Product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}