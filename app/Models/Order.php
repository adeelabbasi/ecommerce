<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orders';
	public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'user_id', 'order_date', 'status'
    ];
	
    public function User()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
	
	public function Order_detail()
    {
        return $this->hasMany('App\Models\Order_detail');
    }
}