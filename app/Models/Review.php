<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'reviews';
	
    protected $fillable = [
        'name', 'user_id', 'product_id', 'message', 'rating'
    ];

    /**
     * count sizes
     * @return mixed
     */
    public function Product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
}
