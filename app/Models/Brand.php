<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'brands';
	public $timestamps = false;
	
    protected $fillable = [
        'brand',
    ];
    
    public function Product()
    {
        return $this->hasMany('App\Models\Product');
    }	
    /**
     * Count brands
     * @return mixed
     */
    public function brandCount()
    {
        return $this->product()
            ->selectRaw('id, count(*) as aggregate')
            ->groupBy('id');
    }
}