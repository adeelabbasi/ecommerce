<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */

    protected $table = 'products';

    //protected $primaryKey = 'product_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'parent', 'slug', 'title', 'sku', 'description', 'feature', 'a_img', 'b_img', 'c_img', 'd_img', 'e_img', 'brand_id', 'category_id', 'sub_category_id', 'quantity', 'price', 'price_2', 'shipping_cost', 'warranty', 'weight', 'dimension', 'material', 'manufacturer', 'status'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     * Just example with singular name of the method
     */
    public function Brand()
    {
        return $this->belongsTo('App\Models\Brand', 'brand_id');
    }

    public function Category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }
	
	public function Subcategory()
    {
        return $this->belongsTo('App\Models\Category', 'sub_category_id');
    }

    public function Order()
    {
        return $this->belongsTo('App\Models\Order', 'product_id', 'id');
    }

    public function ProductSize()
    {
        return $this->hasMany('App\Models\Product_size');
    }

    public function Size()
    {
        return $this->belongsToMany('App\Models\Size', 'product_sizes');
    }

    public function ProductColor()
    {
        return $this->hasMany('App\Models\Product_color');
    }

    public function Color()
    {
        return $this->belongsToMany('App\Models\Color', 'product_colors');
    }
	
	public function Review()
    {
        return $this->hasMany('App\Models\Review')->orderBy('created_at','desc');
    }
}