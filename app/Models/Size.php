<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sizes';
	public $timestamps = false;
	
    protected $fillable = [
        'size',
    ];

    /**
     * count sizes
     * @return mixed
     */
    public function sizeCount()
    {
        return $this->hasOne('App\Models\Products_sizes')
            ->selectRaw('id, count(*) as aggregate')
            ->groupBy('id');
    }
}