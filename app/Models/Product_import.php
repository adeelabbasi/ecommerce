<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product_import extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */

    protected $table = 'product_imports';
	public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'Title', 'Parent', 'Slug', 'SKU', 'Description', 'Feature', 'Image1', 'Image2', 'Image3', 'Image4', 'Image5', 'Color', 'Size', 'Brand', 'Category', 'Subcategory', 'Quantity', 'Price', 'Price_2', 'ShippingCost', 'Warranty', 'Weight', 'Dimension', 'Material', 'Manufacturer', 'Status'
    ];
}