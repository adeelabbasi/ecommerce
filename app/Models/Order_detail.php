<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order_detail extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order_details';
	public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'order_id', 'product_id', 'size', 'img', 'color', 'quantity', 'amount' 
    ];

    public function Product()
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }
	
    public function Order()
    {
        return $this->belongsTo('App\Models\Order', 'order_id');
    }
}