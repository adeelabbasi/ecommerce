<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'colors';
	public $timestamps = false;

    protected $fillable = [
        'color',
    ];


    /**
     * count colors
     * @return mixed
     */
    public function colorCount()
    {
        return $this->hasOne('App\Models\Product_color')
            ->selectRaw('id, count(*) as aggregate')
            ->groupBy('id');
    }
}