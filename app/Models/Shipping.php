<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shippings';
	public $timestamps = false;


    protected $fillable = [
        'method', 'img', 'rate'
    ];
  
}