<?php

namespace App\Repositories;

use App\Models\Product;
use Illuminate\Support\Facades\Input;
use App\Repositories\ImageRepository;
use DB;
use Illuminate\Support\Facades\Hash;

class ProductRepository {
	
	/**
	 * @var App\Models\Product
	 */
	protected $db_product;
	protected $imageRps;
	protected $uploadFolder = 'images/products/';
		
    public function __construct(Product $db_product, ImageRepository $imageRps) 
    {
        $this->db_product = $db_product;
		$this->imageRps = $imageRps;
    }
	
	public function addProduct($inputs)
    {
        $db_product = $this->storeProduct(new $this->db_product ,  $inputs);
        return $db_product;
    }
	
	public function updateProduct($inputs, $id)
	{
		$db_product = $this->db_product->findOrFail($id);
		$product_id = $this->storeProduct($db_product, $inputs, $id);
		return $product_id;
	}
	
	public function deleteProduct($id)
    {
		$db_product = $this->db_product->findOrFail($id);
        $db_product->delete();
        return true;
    }

	function storeProduct($db_product , $inputs, $id = null)
	{	
		if ($id) 
		{
            $db_product->id = $id;
        }		
		
		$db_product->slug = $inputs['slug'];
		$db_product->title = $inputs['title'];
		$db_product->sku = $inputs['sku'];
		$db_product->parent = $inputs['parent'];
		$db_product->description = $inputs['description'];
		$db_product->feature = $inputs['feature'];
		$db_product->brand_id = $inputs['brand_id'];
		$db_product->category_id = $inputs['category_id'];
		$db_product->sub_category_id = $inputs['sub_category_id'];
		$db_product->quantity = $inputs['quantity'];
		$db_product->price = $inputs['price'];
		$db_product->price_2 = $inputs['price_2'];
		$db_product->shipping_cost = $inputs['shipping_cost'];
		$db_product->warranty = $inputs['warranty'];
		$db_product->weight = $inputs['weight'];
		$db_product->dimension = $inputs['dimension'];
		$db_product->material = $inputs['material'];
		$db_product->manufacturer = $inputs['manufacturer'];
		$db_product->status = $inputs['status'];
		$db_product->save();
		
		if(isset($inputs['color_if_any']))
		{
			$color_if_anys = $inputs['color_if_any'];
			foreach($color_if_anys as $color_if_any)
			{
				$db_product->Color()->attach($color_if_any);
			}
		}
		
		if(isset($inputs['size_if_any']))
		{
			$size_if_anys = $inputs['size_if_any'];
			foreach($size_if_anys as $size_if_any)
			{
				$db_product->Size()->attach($size_if_any);
			}
		}
		
		if(isset($inputs['a_img']))
		{
			if($db_product->a_img!="")
				$this->imageRps->delete($db_product->a_img, $this->uploadFolder);
			$db_product->a_img = $this->imageRps->upload($inputs['a_img'], $db_product->id, $this->uploadFolder, '555', '480');
			$db_product->save();
		}
		if(isset($inputs['b_img']))
		{
			if($db_product->a_img!="")
				$this->imageRps->delete($db_product->b_img, $this->uploadFolder);
			$db_product->b_img = $this->imageRps->upload($inputs['b_img'], $db_product->id, $this->uploadFolder, '555', '480');
			$db_product->save();
		}
		if(isset($inputs['c_img']))
		{
			if($db_product->c_img!="")
				$this->imageRps->delete($db_product->a_img, $this->uploadFolder);
			$db_product->c_img = $this->imageRps->upload($inputs['c_img'], $db_product->id, $this->uploadFolder, '555', '480');
			$db_product->save();
		}
		if(isset($inputs['d_img']))
		{
			if($db_product->d_img!="")
				$this->imageRps->delete($db_product->d_img, $this->uploadFolder);
			$db_product->d_img = $this->imageRps->upload($inputs['d_img'], $db_product->id, $this->uploadFolder, '555', '480');
			$db_product->save();
		}
		if(isset($inputs['e_img']))
		{
			if($db_product->e_img!="")
				$this->imageRps->delete($db_product->e_img, $this->uploadFolder);
			$db_product->e_img = $this->imageRps->upload($inputs['e_img'], $db_product->id, $this->uploadFolder, '555', '480');
			$db_product->save();
		}
		
		
		return $db_product;
	}
	
	function storeProductExcel($inputs, $id = null)
	{	
		$db_product = new $this->db_product;
		
		$db_product->slug = $inputs['slug'];
		$db_product->title = $inputs['title'];
		$db_product->sku = $inputs['sku'];
		$db_product->parent = $inputs['parent'];
		$db_product->description = $inputs['description'];
		$db_product->feature = $inputs['feature'];
		$db_product->brand_id = $inputs['brand_id'];
		$db_product->category_id = $inputs['category_id'];
		$db_product->sub_category_id = $inputs['sub_category_id'];
		$db_product->quantity = $inputs['quantity'];
		$db_product->price = $inputs['price'];
		$db_product->price_2 = $inputs['price_2'];
		$db_product->shipping_cost = $inputs['shipping_cost'];
		$db_product->warranty = $inputs['warranty'];
		$db_product->weight = $inputs['weight'];
		$db_product->dimension = $inputs['dimension'];
		$db_product->material = $inputs['material'];
		$db_product->manufacturer = $inputs['manufacturer'];
		$db_product->status = $inputs['status'];
		
		$db_product->a_img = $inputs['a_img'];
		$db_product->b_img = $inputs['b_img'];
		$db_product->c_img = $inputs['c_img'];
		$db_product->d_img = $inputs['d_img'];
		$db_product->e_img = $inputs['e_img'];
		
		$db_product->save();
		
		if(isset($inputs['color_if_any']))
		{
			$color_if_anys = $inputs['color_if_any'];
			foreach($color_if_anys as $color_if_any)
			{
				$db_product->Color()->attach($color_if_any);
			}
		}
		
		if(isset($inputs['size_if_any']))
		{
			$size_if_anys = $inputs['size_if_any'];
			foreach($size_if_anys as $size_if_any)
			{
				$db_product->Size()->attach($size_if_any);
			}
		}
		
	
		
		
		return $db_product;
	}
	
	public function getProduct($id = null)
    {
		if($id==null)
		{
			$info_Product = $this->db_product->select('id', 'parent', 'slug', 'title', 'sku', 'description', 'feature', 'a_img', 'b_img', 'c_img', 'd_img', 'e_img', 'brand_id', 'category_id', 'sub_category_id', 'quantity', 'price', 'price_2', 'shipping_cost', 'warranty', 'weight', 'dimension', 'material', 'manufacturer', 'status', 'created_at', 'updated_at')->orderBy('id', 'ASC');
		}
		else
		{
			$info_Product = $this->db_product->select('id', 'parent', 'slug', 'title', 'sku', 'description', 'feature', 'a_img', 'b_img', 'c_img', 'd_img', 'e_img', 'brand_id', 'category_id', 'sub_category_id', 'quantity', 'price', 'price_2', 'shipping_cost', 'warranty', 'weight', 'dimension', 'material', 'manufacturer', 'status', 'created_at', 'updated_at')->findOrFail($id);
		}
        return $info_Product;
    }
	
	public function getProductByCategory($sub_category_id,$orderby=null)
    {
		
		switch($orderby)
		{
			case "date":
				$info_Product = $this->db_product->select('id', 'parent', 'slug', 'title', 'sku', 'description', 'feature', 'a_img', 'b_img', 'c_img', 'd_img', 'e_img', 'brand_id', 'category_id', 'sub_category_id', 'quantity', 'price', 'price_2', 'shipping_cost', 'warranty', 'weight', 'dimension', 'material', 'manufacturer', 'status', 'created_at', 'updated_at')->where('sub_category_id', $sub_category_id)->orderBy('created_at', 'ASC');
			case "price":
				$info_Product = $this->db_product->select('id', 'parent', 'slug', 'title', 'sku', 'description', 'feature', 'a_img', 'b_img', 'c_img', 'd_img', 'e_img', 'brand_id', 'category_id', 'sub_category_id', 'quantity', 'price', 'price_2', 'shipping_cost', 'warranty', 'weight', 'dimension', 'material', 'manufacturer', 'status', 'created_at', 'updated_at')->where('sub_category_id', $sub_category_id)->orderBy('price', 'ASC');
			case "price-desc":
				$info_Product = $this->db_product->select('id', 'parent', 'slug', 'title', 'sku', 'description', 'feature', 'a_img', 'b_img', 'c_img', 'd_img', 'e_img', 'brand_id', 'category_id', 'sub_category_id', 'quantity', 'price', 'price_2', 'shipping_cost', 'warranty', 'weight', 'dimension', 'material', 'manufacturer', 'status', 'created_at', 'updated_at')->where('sub_category_id', $sub_category_id)->orderBy('price', 'DESC');
			default:
				$info_Product = $this->db_product->select('id', 'parent', 'slug', 'title', 'sku', 'description', 'feature', 'a_img', 'b_img', 'c_img', 'd_img', 'e_img', 'brand_id', 'category_id', 'sub_category_id', 'quantity', 'price', 'price_2', 'shipping_cost', 'warranty', 'weight', 'dimension', 'material', 'manufacturer', 'status', 'created_at', 'updated_at')->where('sub_category_id', $sub_category_id)->orderBy('id', 'ASC');
		}
        return $info_Product;
    }
	
	public function getProductByCategoryColor($sub_category_id, $color_id)
    {
		
		$info_Product = $this->db_product->select('products.id', 'slug', 'title', 'sku', 'description', 'feature', 'a_img', 'b_img', 'c_img', 'd_img', 'e_img', 'brand_id', 'category_id', 'sub_category_id', 'quantity', 'price', 'price_2', 'shipping_cost', 'warranty', 'weight', 'dimension', 'material', 'manufacturer', 'status', 'created_at', 'updated_at')
		->join( 'product_colors', 'products.id', '=', 'product_colors.product_id' )
		->where('product_colors.color_id', $color_id)
		->where('sub_category_id', $sub_category_id);
		
        return $info_Product;
    }
	
	public function getProductByCategorySize($sub_category_id, $size_id)
    {
		
		$info_Product = $this->db_product->select('products.id', 'slug', 'title', 'sku', 'description', 'feature', 'a_img', 'b_img', 'c_img', 'd_img', 'e_img', 'brand_id', 'category_id', 'sub_category_id', 'quantity', 'price', 'price_2', 'shipping_cost', 'warranty', 'weight', 'dimension', 'material', 'manufacturer', 'status', 'created_at', 'updated_at')
		->join( 'product_sizes', 'products.id', '=', 'product_sizes.product_id' )
		->where('product_sizes.size_id', $size_id)
		->where('sub_category_id', $sub_category_id);
		
        return $info_Product;
    }
	
	public function getProductByPrice($price_from, $price_to)
    {
		
		$info_Product = $this->db_product->select('products.id', 'slug', 'title', 'sku', 'description', 'feature', 'a_img', 'b_img', 'c_img', 'd_img', 'e_img', 'brand_id', 'category_id', 'sub_category_id', 'quantity', 'price', 'price_2', 'shipping_cost', 'warranty', 'weight', 'dimension', 'material', 'manufacturer', 'status', 'created_at', 'updated_at')
		->whereBetween('price', [$price_from, $price_to]);
		
        return $info_Product;
    }
	
	public function getProductBySlug($slug)
    {
		$info_Product = $this->db_product->select('id', 'slug', 'title', 'sku', 'description', 'feature', 'a_img', 'b_img', 'c_img', 'd_img', 'e_img', 'brand_id', 'category_id', 'sub_category_id', 'quantity', 'price', 'price_2', 'shipping_cost', 'warranty', 'weight', 'dimension', 'material', 'manufacturer', 'status', 'created_at', 'updated_at')->where('slug', $slug)->First();

        return $info_Product;
    }
	
	public function getProductSearch($search)
    {
		$info_Product = $this->db_product->select('id', 'slug', 'title', 'sku', 'description', 'feature', 'a_img', 'b_img', 'c_img', 'd_img', 'e_img', 'brand_id', 'category_id', 'sub_category_id', 'quantity', 'price', 'price_2', 'shipping_cost', 'warranty', 'weight', 'dimension', 'material', 'manufacturer', 'status', 'created_at', 'updated_at')->where('title', 'like' , '%'.$search.'%');
        return $info_Product;
    }
}

