<?php

namespace App\Repositories;

use App\Models\Customer;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class CustomerRepository {
	
	/**
	 * @var App\Models\Customer
	 */
	protected $db_customer;
		
    public function __construct(Customer $db_customer) 
    {
        $this->db_customer = $db_customer;
    }
	
	public function addCustomer($inputs)
    {
        $db_customer = $this->storeCustomer(new $this->db_customer ,  $inputs);
        return $db_customer;
    }
	
	public function updateCustomer($inputs, $id)
	{
		$db_customer = $this->db_customer->findOrFail($id);
		$customer_id = $this->storeCustomer($db_customer, $inputs, $id);
		return $customer_id;
	}
	
	public function deleteCustomer($id)
    {
		$db_customer = $this->db_customer->findOrFail($id);
        $db_customer->delete();
        return true;
    }

	function storeCustomer($db_customer , $inputs, $id = null)
	{	
		$db_customer->user_id = $inputs['user_id'];
		$db_customer->country = $inputs['country'];
		$db_customer->city = $inputs['city'];
		$db_customer->postcode = $inputs['postcode'];
		$db_customer->name = $inputs['name'];
		$db_customer->email = $inputs['email'];
		$db_customer->address = $inputs['address'];
		$db_customer->phone = $inputs['phone'];
		$db_customer->save();
		return $db_customer;
	}
	
	public function getCustomer($id = null)
    {
		if($id==null)
		{
			$info_Customer = $this->db_customer->select('id', 'user_id', 'country', 'city', 'postcode', 'name', 'email', 'address', 'phone', 'created_at', 'updated_at')->orderBy('created_at', 'DESC')->get();
		}
		else
		{
			$info_Customer = $this->db_customer->select('id', 'user_id', 'country', 'city', 'postcode', 'name', 'email', 'address', 'phone', 'created_at', 'updated_at')->findOrFail($id);
		}
        return $info_Customer;
    }
}

