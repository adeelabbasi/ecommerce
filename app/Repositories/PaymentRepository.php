<?php

namespace App\Repositories;

use App\Models\Payment;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class PaymentRepository {
	
	/**
	 * @var App\Models\Payment
	 */
	protected $db_payment;
		
    public function __construct(Payment $db_payment) 
    {
        $this->db_payment = $db_payment;
    }
	
	public function addPayment($inputs)
    {
        $db_payment = $this->storePayment(new $this->db_payment ,  $inputs);
        return $db_payment;
    }
	
	public function updatePayment($inputs, $id)
	{
		$db_payment = $this->db_payment->findOrFail($id);
		$payment_id = $this->storePayment($db_payment, $inputs, $id);
		return $payment_id;
	}
	
	public function deletePayment($id)
    {
		$db_payment = $this->db_payment->findOrFail($id);
        $db_payment->delete();
        return true;
    }

	function storePayment($db_payment , $inputs, $id = null)
	{	
		$db_payment->method = $inputs['method'];
		$db_payment->img = $inputs['img'];
		$db_payment->sku = $inputs['sku'];
		$db_payment->save();
		return $db_payment;
	}
	
	public function getPayment($id = null)
    {
		if($id==null)
		{
			$info_Payment = $this->db_payment->select('id', 'method', 'img')->get();
		}
		else
		{
			$info_Payment = $this->db_payment->select('id', 'method', 'img')->findOrFail($id);
		}
        return $info_Payment;
    }
}

