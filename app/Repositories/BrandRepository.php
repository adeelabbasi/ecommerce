<?php

namespace App\Repositories;

use App\Models\Brand;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class BrandRepository {
	
	/**
	 * @var App\Models\Brand
	 */
	protected $db_brand;
		
    public function __construct(Brand $db_brand) 
    {
        $this->db_brand = $db_brand;
    }
	
	public function addBrand($inputs)
    {
        $db_brand = $this->storeBrand(new $this->db_brand ,  $inputs);
        return $db_brand;
    }
	
	public function updateBrand($inputs, $id)
	{
		$db_brand = $this->db_brand->findOrFail($id);
		$brand_id = $this->storeBrand($db_brand, $inputs, $id);
		return $brand_id;
	}
	
	public function deleteBrand($id)
    {
		$db_brand = $this->db_brand->findOrFail($id);
        $db_brand->delete();
        return true;
    }

	function storeBrand($db_brand , $inputs, $id = null)
	{	
		$db_brand->brand = $inputs['brand'];
		$db_brand->save();
		return $db_brand;
	}
	
	public function getBrand($id = null)
    {
		if($id==null)
		{
			$info_Brand = $this->db_brand->select('id', 'brand')->get();
		}
		else
		{
			$info_Brand = $this->db_brand->select('id', 'brand')->findOrFail($id);
		}
        return $info_Brand;
    }
}

