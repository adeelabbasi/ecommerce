<?php

namespace App\Repositories;

use App\Models\Size;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class SizeRepository {
	
	/**
	 * @var App\Models\Size
	 */
	protected $db_size;
		
    public function __construct(Size $db_size) 
    {
        $this->db_size = $db_size;
    }
	
	public function addSize($inputs)
    {
        $db_size = $this->storeSize(new $this->db_size ,  $inputs);
        return $db_size;
    }
	
	public function updateSize($inputs, $id)
	{
		$db_size = $this->db_size->findOrFail($id);
		$size_id = $this->storeSize($db_size, $inputs, $id);
		return $size_id;
	}
	
	public function deleteSize($id)
    {
		$db_size = $this->db_size->findOrFail($id);
        $db_size->delete();
        return true;
    }

	function storeSize($db_size , $inputs, $id = null)
	{	
		$db_size->size = $inputs['size'];
		$db_size->save();
		return $db_size;
	}
	
	public function getSize($id = null)
    {
		if($id==null)
		{
			$info_Size = $this->db_size->select('id', 'size')->get();
		}
		else
		{
			$info_Size = $this->db_size->select('id', 'size')->findOrFail($id);
		}
        return $info_Size;
    }
	
	public function getSizeByCategory($sub_category_id)
    {
		
		$info_Color = DB::select('SELECT sizes.id as id, sizes.size as size, count(*) as count FROM products join product_sizes on (product_sizes.product_id = products.id) join sizes on (product_sizes.size_id=sizes.id) where products.sub_category_id='.$sub_category_id.' group by sizes.id, sizes.size order by sizes.size');
		
        return $info_Color;
    }
	
	public function getSizeByCategorySingle($sub_category_id, $size_id)
    {
		
		$info_Color = DB::select('SELECT sizes.id as id, sizes.size as size, count(*) as count FROM products join product_sizes on (product_sizes.product_id = products.id) join sizes on (product_sizes.size_id=sizes.id) where products.sub_category_id='.$sub_category_id.' and sizes.id='.$size_id.' group by sizes.id, sizes.size order by sizes.size');
		
        return $info_Color;
    }
	
	public function getSizeByPrice($price_from, $price_to)
    {
		
		$info_Color = DB::select('SELECT sizes.id as id, sizes.size as size, count(*) as count FROM products join product_sizes on (product_sizes.product_id = products.id) join sizes on (product_sizes.size_id=sizes.id) where products.price between '.$price_from.' and '.$price_to.' group by sizes.id, sizes.size order by sizes.size');
		
        return $info_Color;
    }
}

