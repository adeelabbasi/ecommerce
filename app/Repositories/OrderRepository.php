<?php

namespace App\Repositories;

use App\Models\Order;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class OrderRepository {
	
	/**
	 * @var App\Models\Order
	 */
	protected $db_order;
		
    public function __construct(Order $db_order) 
    {
        $this->db_order = $db_order;
    }
	
	public function addOrder($inputs)
    {
        $db_order = $this->storeOrder(new $this->db_order ,  $inputs);
        return $db_order;
    }
	
	public function updateOrder($inputs, $id)
    {
		$db_order = $this->db_order->findOrFail($id);
		$db_order->status = $inputs['status'];
		$db_order->id = $id;
		$db_order->save();
        return true;
    }
	
	public function deleteOrder($id)
    {
		$db_order = $this->db_order->findOrFail($id);
        $db_order->delete();
        return true;
    }

	function storeOrder($db_order , $inputs, $id = null)
	{	
		$db_order->user_id = $inputs['user_id'];
		$db_order->status = $inputs['status'];
		$db_order->amount = $inputs['amount'];
		$db_order->items = $inputs['items'];
		$db_order->save();
		return $db_order;
	}
	
	public function getOrder($id = null)
    {
		if($id==null)
		{
			$info_Order = $this->db_order->select('id', 'user_id', 'amount', 'items', 'order_date', 'status')->orderBy('order_date', 'DESC')->get();
		}
		else
		{
			$info_Order = $this->db_order->select('id', 'user_id', 'amount', 'items', 'order_date', 'status')->findOrFail($id);
		}
        return $info_Order;
    }
	
	public function getPendingOrder()
    {
		
		return $info_Order = $this->db_order->select('id', 'user_id', 'amount', 'items', 'order_date', 'status')->orderBy('order_date', 'DESC')->where('status',1)->get();
    }
}

