<?php

namespace App\Repositories;

use App\Models\Color;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class ColorRepository {
	
	/**
	 * @var App\Models\Color
	 */
	protected $db_color;
		
    public function __construct(Color $db_color) 
    {
        $this->db_color = $db_color;
    }
	
	public function addColor($inputs)
    {
        $db_color = $this->storeColor(new $this->db_color ,  $inputs);
        return $db_color;
    }
	
	public function updateColor($inputs, $id)
	{
		$db_color = $this->db_color->findOrFail($id);
		$color_id = $this->storeColor($db_color, $inputs, $id);
		return $color_id;
	}
	
	public function deleteColor($id)
    {
		$db_color = $this->db_color->findOrFail($id);
        $db_color->delete();
        return true;
    }

	function storeColor($db_color , $inputs, $id = null)
	{	
		$db_color->color = $inputs['color'];
		$db_color->save();
		return $db_color;
	}
	
	public function getColor($id = null)
    {
		if($id==null)
		{
			$info_Color = $this->db_color->select('id', 'color')->get();
		}
		else
		{
			$info_Color = $this->db_color->select('id', 'color')->findOrFail($id);
		}
        return $info_Color;
    }
	
	public function getColorByCategory($sub_category_id)
    {
		
		$info_Color = DB::select('SELECT colors.id as id, colors.color as color, count(*) as count FROM products join product_colors on (product_colors.product_id = products.id) join colors on (product_colors.color_id=colors.id) where products.sub_category_id='.$sub_category_id.' group by colors.id, colors.color order by colors.color');
		
        return $info_Color;
    }
	
	public function getColorByCategorySingle($sub_category_id, $color_id)
    {
		
		$info_Color = DB::select('SELECT colors.id as id, colors.color as color, count(*) as count FROM products join product_colors on (product_colors.product_id = products.id) join colors on (product_colors.color_id=colors.id) where products.sub_category_id='.$sub_category_id.' and colors.id='.$color_id.' group by colors.id, colors.color order by colors.color');
		
        return $info_Color;
    }
	
	public function getColorByPrice($price_from, $price_to)
    {
		
		$info_Color = DB::select('SELECT colors.id as id, colors.color as color, count(*) as count FROM products join product_colors on (product_colors.product_id = products.id) join colors on (product_colors.color_id=colors.id) where products.price between '.$price_from.' and '.$price_to.' group by colors.id, colors.color order by colors.color');
		
        return $info_Color;
    }
}

