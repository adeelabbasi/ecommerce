<?php

namespace App\Repositories;

use App\Models\Shipping;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class ShippingRepository {
	
	/**
	 * @var App\Models\Shipping
	 */
	protected $db_shipping;
		
    public function __construct(Shipping $db_shipping) 
    {
        $this->db_shipping = $db_shipping;
    }
	
	public function addShipping($inputs)
    {
        $db_shipping = $this->storeShipping(new $this->db_shipping ,  $inputs);
        return $db_shipping;
    }
	
	public function updateShipping($inputs, $id)
	{
		$db_shipping = $this->db_shipping->findOrFail($id);
		$shipping_id = $this->storeShipping($db_shipping, $inputs, $id);
		return $shipping_id;
	}
	
	public function deleteShipping($id)
    {
		$db_shipping = $this->db_shipping->findOrFail($id);
        $db_shipping->delete();
        return true;
    }

	function storeShipping($db_shipping , $inputs, $id = null)
	{	
		$db_shipping->method = $inputs['method'];
		$db_shipping->img = $inputs['img'];
		$db_shipping->rate = $inputs['rate'];
		$db_shipping->save();
		return $db_shipping;
	}
	
	public function getShipping($id = null)
    {
		if($id==null)
		{
			$info_Shipping = $this->db_shipping->select('id', 'method', 'img', 'rate')->get();
		}
		else
		{
			$info_Shipping = $this->db_shipping->select('id', 'method', 'img', 'rate')->findOrFail($id);
		}
        return $info_Shipping;
    }
}

