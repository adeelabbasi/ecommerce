<?php

namespace App\Repositories;

use App\Models\Category;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class CategoryRepository {
	
	/**
	 * @var App\Models\Category
	 */
	protected $db_category;
		
    public function __construct(Category $db_category) 
    {
        $this->db_category = $db_category;
    }
	
	public function addCategory($inputs)
    {
        $db_category = $this->storeCategory(new $this->db_category ,  $inputs);
        return $db_category;
    }
	
	public function updateCategory($inputs, $id)
	{
		$db_category = $this->db_category->findOrFail($id);
		$category_id = $this->storeCategory($db_category, $inputs, $id);
		return $category_id;
	}
	
	public function deleteCategory($id)
    {
		$db_category = $this->db_category->findOrFail($id);
        $db_category->delete();
        return true;
    }

	function storeCategory($db_category , $inputs, $id = null)
	{	
		$db_category->category = $inputs['category'];
		if(isset($inputs['parent_id']))
			$db_category->parent_id = $inputs['parent_id'];
		if(isset($inputs['m_img']))
			$db_category->m_img = $inputs['m_img'];
		if(isset($inputs['f_img']))
			$db_category->f_img = $inputs['f_img'];
		$db_category->save();
		return $db_category;
	}
	
	public function getCategory($id = null)
    {
		if($id==null)
		{
			$info_Category = $this->db_category->select('id', 'category', 'slug', 'parent_id', 'm_img', 'f_img')->Where('parent_id','0')->get();
		}
		else
		{
			$info_Category = $this->db_category->select('id', 'category', 'slug', 'parent_id', 'm_img', 'f_img')->findOrFail($id);
		}
        return $info_Category;
    }
	
	public function getSubCategory($id = null)
    {
		if($id==null)
		{
			$info_Category = $this->db_category->select('id', 'category', 'slug', 'parent_id', 'm_img', 'f_img')->Where('parent_id','<>','0')->get();
		}
		else
		{
			$info_Category = $this->db_category->select('id', 'category', 'slug', 'parent_id', 'm_img', 'f_img')->findOrFail($id);
		}
        return $info_Category;
    }
	
	public function getCategorySubCategories($id)
	{
		return $info_Category = $this->db_category->select('id', 'category', 'slug', 'parent_id', 'm_img', 'f_img')->Where('parent_id',$id)->get();
	}
	
	public function getCategoryBySlug($slug)
	{
		return $info_Category = $this->db_category->select('id', 'category', 'slug', 'parent_id', 'm_img', 'f_img')->Where('slug',$slug)->First();
	}
	
	public function getProductsSubCategoryCounts()
    {
		
		$info_Category = DB::select('SELECT categories.category as category, categories.slug as slug, count(*) as count FROM categories join products on (products.sub_category_id = categories.id) group by categories.category, categories.slug order by categories.category');
		
        return $info_Category;
    }
}

