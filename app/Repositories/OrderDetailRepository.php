<?php

namespace App\Repositories;

use App\Models\Order_detail;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class OrderDetailRepository {
	
	/**
	 * @var App\Models\OrderDetail
	 */
	protected $db_order_detail;
		
    public function __construct(Order_detail $db_order_detail) 
    {
        $this->db_order_detail = $db_order_detail;
    }
	
	public function addOrderDetail($inputs)
    {
        $db_order_detail = $this->storeOrderDetail(new $this->db_order_detail ,  $inputs);
        return $db_order_detail;
    }
	
	public function updateOrderDetail($inputs, $id)
	{
		$db_order_detail = $this->db_order_detail->findOrFail($id);
		$order_detail_detail_id = $this->storeOrderDetail($db_order_detail, $inputs, $id);
		return $order_detail_detail_id;
	}
	
	public function deleteOrderDetail($id)
    {
		$db_order_detail = $this->db_order_detail->findOrFail($id);
        $db_order_detail->delete();
        return true;
    }
	
	public function deleteOrderDetailByOrder($orderID)
    {
		$db_order_details = $this->db_order_detail->where('order_id',$orderID)->Get();
        foreach($db_order_details as $db_order_detail)
		{
			$db_order_detail->delete();
		}
        return true;
    }

	function storeOrderDetail($db_order_detail , $inputs, $id = null)
	{	
		$db_order_detail->order_id = $inputs['order_id'];
		$db_order_detail->product_id = $inputs['product_id'];
		$db_order_detail->size = $inputs['size'];
		$db_order_detail->img = $inputs['img'];
		$db_order_detail->color = $inputs['color'];
		$db_order_detail->quantity = $inputs['quantity'];
		$db_order_detail->amount = $inputs['amount'];
		$db_order_detail->save();
		return $db_order_detail;
	}
	
	public function getOrderDetail($id = null)
    {
		if($id==null)
		{
			$info_OrderDetail = $this->db_order_detail->select('id', 'order_id', 'product_id', 'size', 'img', 'color', 'quantity', 'amount', 'created_at', 'updated_at')->order_detailBy('created_at', 'DESC')->get();
		}
		else
		{
			$info_OrderDetail = $this->db_order_detail->select('id', 'order_id', 'product_id', 'size', 'img', 'color', 'quantity', 'amount', 'created_at', 'updated_at')->findOrFail($id);
		}
        return $info_OrderDetail;
    }
}

