<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class UserRepository {
	
	/**
	 * @var App\Models\User
	 */
	protected $db_user;
		
    public function __construct(User $db_user) 
    {
        $this->db_user = $db_user;
    }
	
	public function addUser($inputs)
    {
        $db_user = $this->storeUser(new $this->db_user ,  $inputs);
        return $db_user;
    }
	
	public function updateUser($inputs, $id)
	{
		$db_user = $this->db_user->findOrFail($id);
		$user_id = $this->storeUser($db_user, $inputs, $id);
		return $user_id;
	}
	
	public function deleteUser($id)
    {
		$db_user = $this->db_user->findOrFail($id);
        $db_user->delete();
        return true;
    }

	function storeUser($db_user , $inputs, $id = null)
	{	
		$db_user->first_name = $inputs['first_name'];
		$db_user->last_name = $inputs['last_name'];
		$db_user->company_name = $inputs['company_name'];
		$db_user->country = $inputs['country'];
		$db_user->city = $inputs['city'];
		$db_user->address = $inputs['address'];
		$db_user->address2 = $inputs['address2'];
		$db_user->zip = $inputs['zip'];
		$db_user->avatar = '';
		$db_user->email = $inputs['email'];
		$db_user->phone = $inputs['phone'];
		$db_user->password = '1230123';
		$db_user->save();
		return $db_user;
	}
	
	public function getUser($id = null)
    {
		if($id==null)
		{
			$info_User = $this->db_user->select('id', 'first_name', 'last_name', 'company_name', 'country', 'city', 'address', 'address2', 'zip', 'avatar', 'email', 'phone', 'password')->get();
		}
		else
		{
			$info_User = $this->db_user->select('id', 'first_name', 'last_name', 'company_name', 'country', 'city', 'address', 'address2', 'zip', 'avatar', 'email', 'phone', 'password')->findOrFail($id);
		}
        return $info_User;
    }
	
	public function getUserbyEmail($email)
    {
		$info_User = $this->db_user->select('id', 'first_name', 'last_name', 'company_name', 'country', 'city', 'address', 'address2', 'zip', 'avatar', 'email', 'phone', 'password')->where('email',$email)->First();
        return $info_User;
    }
}

