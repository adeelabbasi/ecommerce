@extends("layouts.app")
@section('content')
<div class="full-width-page page-title-shown">
   <div id="primary" class="content-area">
      <div id="content" class="site-content" role="main">
         <header class="entry-header " >
            <div class="page_header_overlay"></div>
            <div class="row">
               <div class="large-12 columns">
                  <h1 class="page-title">Returns and Exchanges</h1>
               </div>
            </div>
         </header>
         <!-- .entry-header -->
         <div class="entry-content">
            <div class="boxed-row">
               <div style=""class="normal_height vc_row wpb_row vc_row-fluid vc_custom_1487365316112">
                  <div class="wpb_column vc_column_container vc_col-sm-12">
                     <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                           <div class="row vc_row wpb_row vc_inner vc_row-fluid vc_custom_1410358939666">
                              <div class="wpb_column vc_column_container vc_col-sm-12">
                                 <div class="vc_column-inner vc_custom_1443415906747">
                                    <div class="wpb_wrapper">
                                       <div class="wpb_text_column wpb_content_element " >
                                          <div class="wpb_wrapper">
                                             <p>Here in CustomHomeTex, all our products are unused and in their original packaging which goes through intensive quality check before they get shipped to our valued customers. However, due to manufacturer or logistics errors, defects or problems may occur and we&#8217;ll do our very best to resolve your issue as soon as possible.</p>
                                             <p>You can request a return by contacting us through phone or email within 30 calendar days after delivery.</p>
                                             <p>Please send us back the item in its original packaging and condition with all of the parts and/or accessories. In the case of clothing and/or shoes &#8211; items must be in original packaging with all tags intact and showing no obvious signs of wear.</p>
                                             <p>Unwanted items (no longer needed, better price available, incompatible, ordered wrong size, style not as expected, etc.) returned due to no fault of the seller will have a restocking fee of up to 30% deducted from the refund. Original shipping charges are also not refunded when an item is returned due to no fault of the seller.</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="wpb_text_column wpb_content_element " >
                              <div class="wpb_wrapper">
                                 <h3><span style="color: #2fb77c;"><strong>Requirements for a Full Refund</strong></span></h3>
                              </div>
                           </div>
                           <div class="row vc_row wpb_row vc_inner vc_row-fluid vc_custom_1410358939666">
                              <div class="wpb_column vc_column_container vc_col-sm-12">
                                 <div class="vc_column-inner vc_custom_1443415906747">
                                    <div class="wpb_wrapper">
                                       <div class="wpb_text_column wpb_content_element " >
                                          <div class="wpb_wrapper">
                                             <p>Item(s) must be in new condition</p>
                                             <p>Item(s) must be returned with all original packaging and accessories</p>
                                             <p>Media items (ie: movies, music, video games, software, etc.) must be unopened and still in their plastic wrap</p>
                                             <p>Musical instruments, televisions, and consumer electronics must have a UPC or serial number</p>
                                             <p>Shoes and clothing must be unworn and in original packaging with all tags intact and no obvious signs of wear.</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="boxed-row">
               <div style=""class="normal_height vc_row wpb_row vc_row-fluid vc_custom_1487365316112">
                  <div class="wpb_column vc_column_container vc_col-sm-12">
                     <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                           <div class="wpb_text_column wpb_content_element " >
                              <div class="wpb_wrapper">
                                 <h3><span style="color: #2fb77c;"><strong>Partial Refunds or Restocking Fees </strong></span></h3>
                              </div>
                           </div>
                           <div class="row vc_row wpb_row vc_inner vc_row-fluid vc_custom_1410358939666">
                              <div class="wpb_column vc_column_container vc_col-sm-12">
                                 <div class="vc_column-inner vc_custom_1443415906747">
                                    <div class="wpb_wrapper">
                                       <div class="wpb_text_column wpb_content_element " >
                                          <div class="wpb_wrapper">
                                             <p>Items in original, unused condition past the return window – 30% restocking fee</p>
                                             <p>CDs, DVDs, VHS tapes, video games, cassette tapes, or vinyl records that were opened (taken out of plastic wrap) – 50% restocking fee</p>
                                             <p>Items that are damaged, missing parts, not in the original condition, or have obvious signs of use for reasons NOT due to seller error – 50% restocking fee</p>
                                             <p>Open software for reasons not due to a Seller error – no refund</p>
                                             <p>&nbsp;</p>
                                             <p>Our company do NOT offer exchanges on items for a different size. Please make every effort to ensure the proper size was ordered for the brand you chose. Items returned because of size will be charged a restocking fee of up to 30%.</p>
                                             <p>Our company DO NOT accept returns for any grocery items due to limitation we have to return them to our supplier or sell them again.</p>
                                             <p>As part of our mission to give quality service to our valued customers, we provide prepaid labels to all return requests. Refunds will be processed as soon as the item is received by us. Refunds may take 2-7 business days to show on your credit/debit card statement.</p>
                                             <p>We hope you enjoy your shopping experience with us!</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="boxed-row">
               <div style=""class="normal_height vc_row wpb_row vc_row-fluid vc_custom_1410359860044">
                  <div class="wpb_column vc_column_container vc_col-sm-12">
                     <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                           <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_separator_no_text vc_sep_color_grey" ><span class="vc_sep_holder vc_sep_holder_l"><span  class="vc_sep_line"></span></span><span class="vc_sep_holder vc_sep_holder_r"><span  class="vc_sep_line"></span></span></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- .entry-content -->
      </div>
      <!-- #content -->           
   </div>
   <!-- #primary -->
</div>
<!-- .full-width-page -->
@endsection