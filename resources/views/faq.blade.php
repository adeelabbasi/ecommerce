@extends("layouts.app")
@section('content')
<div class="full-width-page page-title-shown">
   <div id="primary" class="content-area">
      <div id="content" class="site-content" role="main">
         <header class="entry-header " >
            <div class="page_header_overlay"></div>
            <div class="row">
               <div class="large-12 columns">
                  <h1 class="page-title">F.A.Q.s</h1>
               </div>
            </div>
         </header>
         <!-- .entry-header -->
         <div class="entry-content">
            <div class="boxed-row">
               <div style=""class="normal_height vc_row wpb_row vc_row-fluid vc_custom_1487365316112">
                  <div class="wpb_column vc_column_container vc_col-sm-12">
                     <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                           <div class="wpb_text_column wpb_content_element " >
                              <div class="wpb_wrapper">
                                 <h3><span style="color: #2fb77c;"><strong>Delivery Information</strong></span></h3>
                              </div>
                           </div>
                           <div class="row vc_row wpb_row vc_inner vc_row-fluid vc_custom_1410358939666">
                              <div class="wpb_column vc_column_container vc_col-sm-12">
                                 <div class="vc_column-inner vc_custom_1443415906747">
                                    <div class="wpb_wrapper">
                                       <div class="wpb_text_column wpb_content_element " >
                                          <div class="wpb_wrapper">
                                             <p>Welcome to CustomHomeTex. To us, our customers are like our family members. Bringing them the best thing while considering all economical barriers is our top priority.<br />
                                                Working on a motto, to work with you and for you is what team CustomHomeTex believes. Bearing flag of this motto, we leave no stone unturned to offer you exceptional services. Replying you in time isn’t only our professional but ethical responsibility too. In case, you need details for your product or you are not sure what the status of your current order is, you can always reach us by filling out this short form.
                                             </p>
                                             <p><strong>How Long Will It Take To Get My Package?</strong><br />
                                                All items offered by CustomHomeTex are tailor-made and can take up to 4 days within US. In case you want urgent delivery, it should cost you little more than the usual amount.
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="boxed-row">
               <div style=""class="normal_height vc_row wpb_row vc_row-fluid vc_custom_1410359860044">
                  <div class="wpb_column vc_column_container vc_col-sm-12">
                     <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                           <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_separator_no_text vc_sep_color_grey" ><span class="vc_sep_holder vc_sep_holder_l"><span  class="vc_sep_line"></span></span><span class="vc_sep_holder vc_sep_holder_r"><span  class="vc_sep_line"></span></span></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="boxed-row">
               <div style=""class="normal_height vc_row wpb_row vc_row-fluid vc_custom_1487365252231">
                  <div class="wpb_column vc_column_container vc_col-sm-12">
                     <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                           <div class="wpb_text_column wpb_content_element " >
                              <div class="wpb_wrapper">
                                 <h3><span style="color: #2fb77c;">No question return policy</span></h3>
                              </div>
                           </div>
                           <div class="row vc_row wpb_row vc_inner vc_row-fluid vc_custom_1410358939666">
                              <div class="wpb_column vc_column_container vc_col-sm-12">
                                 <div class="vc_column-inner vc_custom_1443415970531">
                                    <div class="wpb_wrapper">
                                       <div class="wpb_text_column wpb_content_element " >
                                          <div class="wpb_wrapper">
                                             <p>Thank you for being one of CustomHomeTex’s prestigious customer. Striving for better customer satisfaction is what we aim for.<br />
                                                It’s unfortunate to see you on this page but we totally understand that discrepancies are part of life. After all, giving you the ultimate satisfaction is what we are here for.<br />
                                                In case you found that the product you got is not what you ordered or it didn’t come up to your expectations. Worry not. Our 30 days No Question Return Policy got you covered.
                                             </p>
                                             <p><strong>Wondering what’s Included to Make a Return</strong><br />
                                                As communicated, CustomHomeTex offers no Question Return Policy. However, in case, you want to make returns please contact us and provide your name and order #.
                                             </p>
                                             <p><strong>What Are Some Costs User Will Have to Bear When Making A Return</strong><br />
                                                All costs associated with the return will be borne by the person who is returning this product.
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="boxed-row">
               <div style=""class="normal_height vc_row wpb_row vc_row-fluid vc_custom_1410359869569">
                  <div class="wpb_column vc_column_container vc_col-sm-12">
                     <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                           <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_separator_no_text vc_sep_color_grey" ><span class="vc_sep_holder vc_sep_holder_l"><span  class="vc_sep_line"></span></span><span class="vc_sep_holder vc_sep_holder_r"><span  class="vc_sep_line"></span></span></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- .entry-content -->
      </div>
      <!-- #content -->           
   </div>
   <!-- #primary -->
</div>
<!-- .full-width-page -->
@endsection