@extends("layouts.app")
@section('content')
<div class="full-width-page page-title-shown">
   <div id="primary" class="content-area">
      <div id="content" class="site-content" role="main">
         <header class="entry-header " >
            <div class="page_header_overlay"></div>
            <div class="row">
               <div class="large-12 columns">
                  <h1 class="page-title">Privacy Policy</h1>
               </div>
            </div>
         </header>
         <!-- .entry-header -->
         <div class="entry-content">
            <div class="boxed-row">
               <div style=""class="normal_height vc_row wpb_row vc_row-fluid vc_custom_1487365260609">
                  <div class="wpb_column vc_column_container vc_col-sm-12">
                     <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                           <div class="wpb_text_column wpb_content_element " >
                              <div class="wpb_wrapper">
                                 <h3><span style="color: #2fb77c;">Privacy</span></h3>
                              </div>
                           </div>
                           <div class="row vc_row wpb_row vc_inner vc_row-fluid vc_custom_1410358939666">
                              <div class="wpb_column vc_column_container vc_col-sm-12">
                                 <div class="vc_column-inner vc_custom_1443415995655">
                                    <div class="wpb_wrapper">
                                       <div class="wpb_text_column wpb_content_element " >
                                          <div class="wpb_wrapper">
                                             <p>Knowing the fact that filing online information could be drastic, team CustomHomeTex nor any of its staff members don’t retain nor produce any personal information from any client at any stage. While buying your favorite shower curtains online, we only require the following information from our esteemed customers:-</p>
                                             <ul>
                                                <li>Your Name</li>
                                                <li>Mailing Address</li>
                                                <li>Phone Number</li>
                                                <li>Email Address</li>
                                                <li>Contact Addresses</li>
                                             </ul>
                                             <p><strong>Why We Hold This Information</strong><br />
                                                We are always at par to provide the most relevant and valuable information to the customer. That’s not all, to serve better by bringing you up to date information and getting you the most valuable yet reliable deals, we hold this information so we can keep you in the loop.
                                             </p>
                                             <p><strong>How Do We Confirm Your Information is Secured</strong><br />
                                                We use SSL (Secure Socket Layer) encryption on all our web pages. SSL is the most secure mechanism that ensures the site you are using is reliable and will prevent your inserted information from theft.
                                             </p>
                                             <p><strong>Can I Change Information Passed on Any Phase</strong><br />
                                                Tech team of CustomHomeTex made sure that you can access your information at all times without any impediment or obstacle. We’ve done this to give you the liberty on modifying and update your information accordingly. In order to get this done, you will have to get login by supplying the details you already provided while signing up.<br />
                                                In case, there’s something special you want to add or remove speak with one of our team mates and we will be happy to assist you.
                                             </p>
                                             <p><strong>Please Note:-</strong><br />
                                                CustoHomeTex equally understands the importance of GDPR 2018 and thus follow the cookie guidelines strictly.
                                             </p>
                                             <p><strong>Want to Know What Are Some Principles We Walk On?</strong><br />
                                                Here at CustomHomeTex, we work on a single principle, i.e. to get best thing for our prestigious clients.<br />
                                                Moving on 3Ps Approach of Purity, Precision and Perfection, we are always amid to get you a thing that should leave an everlasting impression on you.
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- .entry-content -->
      </div>
      <!-- #content -->           
   </div>
   <!-- #primary -->
</div>
<!-- .full-width-page -->
@endsection