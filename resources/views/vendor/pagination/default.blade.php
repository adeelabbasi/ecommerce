@if ($paginator->hasPages())
    <ul class="pages" role="navigation">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
                <a class="btn btn-outline-secondary btn-sm" href="#" disabled rel="prev" aria-label="@lang('pagination.previous')">&lsaquo; Previous</a>
        @else
                <a class="btn btn-outline-secondary btn-sm" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo; Previous</a>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="disabled" aria-disabled="true"><span>{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="active" aria-current="page"><a>{{ $page }}</a></li>
                    @else
                        <li><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
                <a class="btn btn-outline-secondary btn-sm" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">Next &rsaquo;</a>
        @else
            <a class="btn btn-outline-secondary btn-sm" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')" disabled>Next &rsaquo;</a>
        @endif
    </ul>
@endif
