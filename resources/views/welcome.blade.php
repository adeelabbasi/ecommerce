@extends("layouts.app")
@push('styles')
<style>
</style>
@endpush
@section('content')
    <!-- Featured Categories-->
    <section class="container-fluid pt-30 padding-bottom-2x">
      <div class="row justify-content-center">
      	<div class="col-lg-10">
          <p style="text-align:center"><img src="{{asset('home/images/final.png')}}" alt="" width="488" height="407"></p>
          <div class="categories_grid">
            <div class="category_1">
               <div class="category_grid_box">
                  <span class="category_item_bkg" style="background-image:url({{ asset("home/images/clothing-for-woman-cateogry.jpg") }})"></span> 
                  <a href="{{ url('product-category/clothing-for-women') }}" class="category_item" >
                  <span class="category_name">Clothing for women																	</span>
                  </a>
               </div>
            </div>
            <div class="category_2">
               <div class="category_grid_box">
                  <span class="category_item_bkg" style="background-image:url({{ asset("home/images/clothing-for-man.jpg") }})"></span> 
                  <a href="{{ url('product-category/clothing-for-men') }}" class="category_item" >
                  <span class="category_name">Clothing for men																	</span>
                  </a>
               </div>
            </div>
            <div class="category_3">
               <div class="category_grid_box">
                  <span class="category_item_bkg" style="background-image:url({{ asset("home/images/shoe-category.jpg") }})"></span> 
                  <a href="{{ url('product-category/shoes') }}" class="category_item" >
                  <span class="category_name">Shoes																	</span>
                  </a>
               </div>
            </div>
            <div class="category_4">
               <div class="category_grid_box">
                  <span class="category_item_bkg" style="background-image:url({{ asset("home/images/jewelry.jpeg") }})"></span> 
                  <a href="{{ url('product-category/jewelry') }}" class="category_item" >
                  <span class="category_name">Jewelry																	</span>
                  </a>
               </div>
            </div>
            <div class="category_5">
               <div class="category_grid_box">
                  <span class="category_item_bkg" style="background-image:url({{ asset("home/images/outdoor-category.jpeg") }})"></span> 
                  <a href="{{ url('product-category/outdoor-gears') }}" class="category_item" >
                  <span class="category_name">Outdoor Gears																	</span>
                  </a>
               </div>
            </div>
            <div class="category_6">
               <div class="category_grid_box">
                  <span class="category_item_bkg" style="background-image:url({{ asset("home/images/krista-mangulsone-53122-unsplash.jpg") }})"></span> 
                  <a href="{{ url('product-category/pet-care-supplies') }}" class="category_item" >
                  <span class="category_name">Pet Care &amp; Supplies																	</span>
                  </a>
               </div>
            </div>
            <div class="category_more_than_6">
               <div class="category_grid_box">
                  <span class="category_item_bkg" style="background-image:url({{ asset("home/images/home-and-kitchen.jpg") }})"></span> 
                  <a href="{{ url('product-category/home-kitchen') }}" class="category_item" >
                  <span class="category_name">Home &amp; Kitchen																	</span>
                  </a>
               </div>
            </div>
            <div class="category_more_than_6">
               <div class="category_grid_box">
                  <span class="category_item_bkg" style="background-image:url({{ asset("home/images/baby-category.jpg") }})"></span> 
                  <a href="{{ url('product-category/baby-care') }}" class="category_item" >
                  <span class="category_name">Baby																	</span>
                  </a>
               </div>
            </div>
            <div class="category_more_than_6">
               <div class="category_grid_box">
                  <span class="category_item_bkg" style="background-image:url({{ asset("home/images/beauty-personal-care-category.jpg") }})"></span> 
                  <a href="{{ url('product-category/beauty-personal-care') }}" class="category_item" >
                  <span class="category_name">Beauty &amp; Personal Care																	</span>
                  </a>
               </div>
            </div>
            <div class="category_more_than_6">
               <div class="category_grid_box">
                  <span class="category_item_bkg" style="background-image:url({{ asset("home/images/AdobeStock_163763734.jpeg") }})"></span> 
                  <a href="{{ url('product-category/signs-displays') }}" class="category_item" >
                  <span class="category_name">Signs &amp; Displays																	</span>
                  </a>
               </div>
            </div>
            <div class="category_more_than_6">
               <div class="category_grid_box">
                  <span class="category_item_bkg" style="background-image:url({{ asset("home/images/teens.jpg") }})"></span> 
                  <a href="{{ url('product-category/teens') }}" class="category_item" >
                  <span class="category_name">Teens																	</span>
                  </a>
               </div>
            </div>
            <div class="category_more_than_6">
               <div class="category_grid_box">
                  <span class="category_item_bkg" style="background-image:url({{ asset("home/images/kids.jpg") }})"></span> 
                  <a href="{{ url('product-category/kids') }}" class="category_item" >
                  <span class="category_name">Kids																	</span>
                  </a>
               </div>
            </div>
            <div class="clearfix"></div>
          </div>
      	</div>
      </div>	
    </section>
    <!-- Popular Brands-->
    <section class="fw-section bg-secondary padding-top-3x padding-bottom-3x">
      <div class="container">
        <h3 class="text-center mb-30">Popular Brands</h3>
        <div class="owl-carousel pt-2" data-owl-carousel="{ &quot;nav&quot;: false, &quot;dots&quot;: false, &quot;loop&quot;: true, &quot;autoplay&quot;: true, &quot;autoplayTimeout&quot;: 4000, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:2}, &quot;470&quot;:{&quot;items&quot;:3},&quot;630&quot;:{&quot;items&quot;:4},&quot;991&quot;:{&quot;items&quot;:5},&quot;1200&quot;:{&quot;items&quot;:6}} }"><a class="d-block w-150 opacity-60" href="#"><img class="d-block m-auto" src="{{ asset("home/img/brands/01.svg") }}" alt="Brand"></a><a class="d-block w-150 opacity-60" href="#"><img class="d-block m-auto" src="{{ asset("home/img/brands/02.svg") }}" alt="Brand"></a><a class="d-block w-150 opacity-60" href="#"><img class="d-block m-auto" src="{{ asset("home/img/brands/03.svg") }}" alt="Brand"></a><a class="d-block w-150 opacity-60" href="#"><img class="d-block m-auto" src="{{ asset("home/img/brands/04.svg") }}" alt="Brand"></a><a class="d-block w-150 opacity-60" href="#"><img class="d-block m-auto" src="{{ asset("home/img/brands/05.svg") }}" alt="Brand"></a><a class="d-block w-150 opacity-60" href="#"><img class="d-block m-auto" src="{{ asset("home/img/brands/06.svg") }}" alt="Brand"></a></div>
      </div>
    </section>
    <!-- Features-->
    <section class="container padding-top-3x padding-bottom-3x">
      <div class="row pt-2">
        <div class="col-md-3 col-sm-6 text-center mb-30"><span class="d-block display-4 text-gray-light mb-4"><i class="material-icons flight"></i></span>
          <h4 class="h6 mb-2">Free World Wide Shipping</h4>
          <p class="text-sm text-muted mb-0">Free shipping on all orders over $999</p>
        </div>
        <div class="col-md-3 col-sm-6 text-center mb-30"><span class="d-block display-4 text-gray-light mb-4"><i class="material-icons autorenew"></i></span>
          <h4 class="h6 mb-2">Money Back Guarantee</h4>
          <p class="text-sm text-muted mb-0">We return money within 30 days</p>
        </div>
        <div class="col-md-3 col-sm-6 text-center mb-30"><span class="d-block display-4 text-gray-light mb-4"><i class="material-icons headset_mic"></i></span>
          <h4 class="h6 mb-2">24/7 Online Support</h4>
          <p class="text-sm text-muted mb-0">Friendly 24/7 customer support</p>
        </div>
        <div class="col-md-3 col-sm-6 text-center mb-30"><span class="d-block display-4 text-gray-light mb-4"><i class="material-icons credit_card"></i></span>
          <h4 class="h6 mb-2">Secure Online Payments</h4>
          <p class="text-sm text-muted mb-0">We posess SSL / Secure Certificate</p>
        </div>
      </div>
    </section>
@endsection