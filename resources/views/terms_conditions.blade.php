@extends("layouts.app")
@section('content')
<div class="full-width-page page-title-shown">
   <div id="primary" class="content-area">
      <div id="content" class="site-content" role="main">
         <header class="entry-header " >
            <div class="page_header_overlay"></div>
            <div class="row">
               <div class="large-12 columns">
                  <h1 class="page-title">Terms &#038; Conditions</h1>
               </div>
            </div>
         </header>
         <!-- .entry-header -->
         <div class="entry-content">
            <div class="boxed-row">
               <div style=""class="normal_height vc_row wpb_row vc_row-fluid vc_custom_1487365316112">
                  <div class="wpb_column vc_column_container vc_col-sm-12">
                     <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                           <div class="row vc_row wpb_row vc_inner vc_row-fluid vc_custom_1410358939666">
                              <div class="wpb_column vc_column_container vc_col-sm-12">
                                 <div class="vc_column-inner vc_custom_1443415906747">
                                    <div class="wpb_wrapper">
                                       <div class="wpb_text_column wpb_content_element  terms_con_txt" >
                                          <div class="wpb_wrapper">
                                             <h3><span style="color: #2fb77c;"><strong>Terms</strong></span></h3>
                                             <p>By accessing this web site, you are agreeing to be bound by these web site Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this web site are protected by applicable copyright and trade mark law.</p>
                                             <h3><span style="color: #2fb77c;"><span style="caret-color: #2fb77c;">User Lisence</span></span></h3>
                                             <p>Permission is granted to temporarily download one copy of the materials (information or software) on CustomHomeTex’s web site for personal, non-commercial transitory viewing only. This is the grant of a license, not a transfer of title, and under this license you may not:</p>
                                             <p>modify or copy the materials; use the materials for any commercial purpose, or for any public display (commercial or non-commercial); attempt to decompile or reverse engineer any software contained on CustomHomeTex’s web site; remove any copyright or other proprietary notations from the materials; or transfer the materials to another person or &#8220;mirror&#8221; the materials on any other server.</p>
                                             <p>This license shall automatically terminate if you violate any of these restrictions and may be terminated by CustomHomeTex at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format.</p>
                                             <h3><span style="color: #2fb77c;"><span style="caret-color: #2fb77c;">Disclaimer</span></span></h3>
                                             <p>The materials on CustomHomeTex`s web site are provided &#8220;as is&#8221;. CustomHomeTex makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties, including without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Further, CustomHomeTex does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its Internet web site or otherwise relating to such materials or on any sites linked to this site.</p>
                                             <h3><span style="color: #2fb77c;"><span style="caret-color: #2fb77c;">Limitations</span></span></h3>
                                             <p>In no event shall CustomHomeTex or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption,) arising out of the use or inability to use the materials on CustomHomeTex web site, even if CustomHomeTex or a CustomHomeTex authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you.</p>
                                             <h3><span style="color: #2fb77c;"><span style="caret-color: #2fb77c;">Revisions and Errata</span></span></h3>
                                             <p>The materials appearing on CustomHomeTex’s web site could include technical, typographical, or photographic errors. CustomHomeTex does not warrant that any of the materials on its web site are accurate, complete, or current. CustomHomeTex may make changes to the materials contained on its web site at any time without notice. CustomHomeTex does not, however, make any commitment to update the materials.</p>
                                             <h3><span style="color: #2fb77c;"><strong>Links</strong></span></h3>
                                             <p>CustomHomeTex has not reviewed all of the sites linked to its web site and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by CustomHomeTex of the site. Use of any such linked web site is at the user&#8217;s own risk.</p>
                                             <h3><strong><span style="color: #2fb77c;">Site Terms of <span style="caret-color: #2fb77c;">Use</span> Modifications</span></strong></h3>
                                             <p>CustomHomeTex may revise these terms of use for its web site at any time without notice. By using this web site you are agreeing to be bound by the then current version of these Terms and Conditions of Use.</p>
                                             <h3><span style="color: #2fb77c;"><strong>Comunications</strong></span></h3>
                                             <p>When you use our service, or send e-mails, text messages, and other communications from your desktop or mobile device to us, you are communicating with us electronically. You consent to receive communications from us electronically, such as e-mails, SMS messages, mobile push notices, or notices and messages on this site, and you can retain copies of these communications for your records. By registering to our service, you may also receive communications regarding news, products, offerings and newsletters relating to our website. You may unsubscribe to promotional emails by clicking the Unsubscribe link on your email or in your account page. You may not unsubscribe to transactional emails that have important information regarding your order(s) or account.</p>
                                             <h3><span style="color: #2fb77c;"><strong>Governing Law</strong></span></h3>
                                             <p>Any claim relating to CustomHomeTex web site shall be governed by the laws of the state of Canada without regard to its conflict of law provisions.</p>
                                             <h3><span style="color: #2fb77c;"><strong>Shipping &amp; Handling</strong></span></h3>
                                             <p>Customer satisfaction is our top priority which is why all orders are fulfilled with utmost care. Orders are shipped from our US warehouse and CustomHomeTex makes it easy for you to track your order from the time it leaves our warehouse up to delivery.</p>
                                             <p>We ship orders out as soon as possible, but this will not always be the same day as your order. In general, please add 2-3 business days to allow for processing time. If we anticipate a longer lead time, it will be noted in the item description.</p>
                                             <h3><span style="color: #2fb77c;"><strong>Shipping Rates</strong></span></h3>
                                             <p>Shipping and handling rates vary depending on the destination of the order and are based on the total cost of the order. All shipping costs are calculated using a U.S. Postal Service shipping module.</p>
                                             <h3><span style="color: #2fb77c;"><strong>Order Tracking</strong></span></h3>
                                             <p>As soon as your order leaves our warehouse, you should receive an email confirmation which will include a tracking # for your reference. You may view all updates to your order status as well as your package’s location at any time from your account page. For any further assistance on your order status, you can contact CustomHomeTex support at any time.</p>
                                             <h3><span style="color: #2fb77c;"><strong>Avaiability</strong></span></h3>
                                             <p>Stock and availability shown on this site is for your reference only. While we strive to maintain the most accurate and timely stock and availability information, availability information may become out of date and may change between the time you added an item to cart and the time your order is processed.</p>
                                             <p>In rare cases, an item may go out of stock, or the number of units you ordered may not be available. In that case, we will contact you about how best to proceed.</p>
                                             <h3><span style="color: #2fb77c;"><strong>Handling and Delivery</strong></span></h3>
                                             <p>All our goods are inspected at customs by our appointed clearance agent, after which it is packed and handed over to a courier for final delivery. If any physical damage is found in the packaging upon delivery, please notify the deliverer immediately and you may return the package immediately.</p>
                                             <h3><span style="color: #2fb77c;"><span style="caret-color: #2fb77c;">Other</span></span></h3>
                                             <p>Business days do not include weekends. Orders placed on a Friday after 12PM PST or over the weekend will begin processing on the following Monday.</p>
                                             <p>For more information on our shipping policy, check ‘Terms &amp; Conditions’ or simply contact us for further assistance.</p>
                                             <h3><span style="color: #2fb77c;"><span style="caret-color: #2fb77c;">Payments</span></span></h3>
                                             <p>What are your payment options?</p>
                                             <p>CustomHomeTex currently accepts payment from any valid credit or debit card.</p>
                                             <p>Do you offer Cash-On-Delivery?</p>
                                             <p>CustomHomeTex does not support cash on delivery for various reasons. At CustomHomeTex, our priority is selling great products and providing the best customer experience. Being cashless helps ensure smoother and faster deliveries which means lower prices for our customers. Cash also cannot provide the full customer protection services that you get from your bank and card provider.</p>
                                             <p>Do you support international cards?</p>
                                             <p>You can use any credit or debit card with a Visa/Mastercard logo in the front of the card. All charges to your card will be made in CAD, so if your card is issued in another country, your bank may charge an extra foreign conversion fee.</p>
                                             <p>Why was my card declined?</p>
                                             <p>Declines can happen for a variety of reasons. When we submit a charge to your bank, they have automated systems that determine whether or not to accept the charge. These systems take various signals into account, such as your spending habits, account balance, and card information like the expiration date and CVC. Since these signals are constantly changing, a previously successful card might be declined in the future. Even if all of the card information is correct, and you previously had a successful payment, a future charge can still be declined by a bank’s overzealous fraud systems. We show as much information as we receive from your bank about a decline. Unfortunately most declines are generic, so we don’t have much information as to why a charge was specifically declined. If all of the card information seems correct, it is best to contact your bank, inquire for more information, and ask for future charges to be accepted.</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- .entry-content -->
      </div>
      <!-- #content -->           
   </div>
   <!-- #primary -->
</div>
<!-- .full-width-page -->
@endsection