@extends("layouts.app")
@section('content')
<div class="page-title">
  <div class="container">
    <h1>Sorry, your order failed. No charges were made.</h1>
    <ul class="breadcrumbs">
      <li><a href="{{ url('/') }}">Back to home</a>
      </li>
    </ul>
  </div>
</div>
    
@endsection