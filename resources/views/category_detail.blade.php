@extends("layouts.app")
@section('content')
	<?php
    if(isset($_GET["page"]))
	{
		$fromItem = (($_GET["page"]-1)*60)+1;
		$toItem = (($_GET["page"])*60);
	}
	else
	{
		$fromItem = 1;
		$toItem = 60;
	}
	?>
	<!-- Page Title-->
    <div class="page-title">
      <div class="container">
        <h1>{{ $category_name }}</h1>
        <ul class="breadcrumbs">
          <li><a href="{{ url("/") }}">Home</a>
          </li>
          <li class="separator">&nbsp;/&nbsp;</li>
          <li>{{ $category_name }}</li>
        </ul>
      </div>
    </div>
    <!-- Page Content-->
    <div class="container-fluid padding-bottom-3x mb-1">
      <div class="row">
        <!-- Content-->
        <div class="col-xl-10 col-lg-9 col-md-8 order-md-2">
          <!-- Search-->
          <div class="d-flex flex-wrap-reverse flex-md-nowrap justify-content-center justify-content-sm-between align-items-center mb-30">
            <div class="pt-3 pb-1 pb-sm-3 text-sm text-center text-sm-left"><span class="text-muted mr-2">Showing</span>{{$fromItem}}- {{$toItem}} items</div>
            <!--<form class="input-group shop-search-box" method="get"><span class="input-group-btn">
                <button type="submit"><i class="material-icons search"></i></button></span>
              <input class="form-control" type="search" placeholder="Search shop">
            </form>-->
          </div>
          <!-- Products Grid-->
          <div class="row mb-2">
            <!-- Item-->
            @foreach($info_Products as $info_Product)
            <div class="col-xl-3 col-lg-4 col-sm-6">
              <div class="product-card mb-30">
                <div class="product-card-thumb"> <span class="product-badge text-danger">Sale</span><a class="product-card-link" href="{{ url('/product/').'/'.$info_Product->slug }}"></a><img src="{{ $info_Product->a_img }}" alt="Product">
                  <div class="product-card-buttons">
                     <a href="{{ url('/product/').'/'.$info_Product->slug }}" class="btn btn-white btn-sm" data-toggle="tooltip" title="Detail"><i class="material-icons pageview"></i></a>
                    <button id="AddtoCart" class="btn btn-primary btn-sm" data-cart="{{ url('cart').'/'.$info_Product->slug}}" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="material-icons check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>
                  </div>
                </div>
                <div class="product-card-details">
                  <h3 class="product-card-title"><a href="{{ url('/product/').'/'.$info_Product->slug }}">{{ $info_Product->title }}</a></h3>
                  <h4 class="product-card-price">
                    @if($info_Product->price_2!="")
                    <del>${{ $info_Product->price_2 }}</del>
                    @endif
                    ${{ $info_Product->price }}
                  </h4>
                </div>
              </div>
            </div>
            @endforeach
          </div>
          <!-- Pagination-->
          <nav class="pagination">
            <div class="column text-right">
              {{ $info_Products->links('vendor.pagination.default') }}
            </div>
          </nav>
        </div>
        <!-- Sidebar          -->
        <div class="col-xl-2 col-lg-3 col-md-4 order-md-1">
          <div class="sidebar-toggle position-left"><i class="material-icons filter_list"></i></div>
          <aside class="sidebar sidebar-offcanvas position-left"><span class="sidebar-close"><i class="material-icons icon_close"></i></span>
            <!-- Widget Categories-->
            <section class="widget widget-categories pt-0">
              <h3 class="widget-title">Shop Categories</h3>
              <ul>
              	@foreach($info_SubCategories as $info_SubCategory)
					<li class=""><a href="{{ url("product-category/")."/".$info_SubCategory->slug }}">{{ $info_SubCategory->category }}</a> <span>({{ $info_SubCategory->count }})</span></li>
               	@endforeach
              </ul>
            </section>
            <!-- Widget Sorting-->
            <!--<section class="widget widget-icon-list">
              <h3 class="widget-title">Sort By</h3>
              <ul>
                <li><a href="#"><i class="material-icons sort"></i>Default</a></li>
                <li><a href="#"><i class="material-icons favorite_border"></i>Popularity</a></li>
                <li><a href="#"><i class="material-icons vertical_align_top"></i>Last entries</a></li>
                <li><a href="#"><i class="material-icons star_border"></i>Average rating</a></li>
                <li><a href="#"><i class="material-icons sort_by_alpha"></i>Alphabetically</a></li>
              </ul>
            </section>-->
            <!-- Widget Price Range-->
            <section class="widget widget-categories">
              <h3 class="widget-title">Price Range</h3>
              <form class="price-range-slider" method="get" data-start-min="<?php echo isset($_GET["price_from"])?$_GET["price_from"]:250; ?>" data-start-max="<?php echo isset($_GET["price_to"])?$_GET["price_to"]:650; ?>" data-min="0" data-max="1000" data-step="1" method="get" action="{{ url("product-category/")."/".$slug }}">
                <div class="ui-range-slider"></div>
                <footer class="ui-range-slider-footer">
                  <div class="column">
                    <button class="btn btn-outline-primary btn-sm" type="submit">Filter</button>
                  </div>
                  <div class="column">
                    <div class="ui-range-values">
                      <div class="ui-range-value-min">$<span></span>
                        <input type="hidden" name="price_from" value="<?php echo isset($_GET["price_from"])?$_GET["price_from"]:250; ?>">
                      </div>&nbsp;-&nbsp;
                      <div class="ui-range-value-max">$<span></span>
                        <input type="hidden" name="price_to" value="<?php echo isset($_GET["price_to"])?$_GET["price_to"]:650; ?>">
                      </div>
                    </div>
                  </div>
                </footer>
              </form>
            </section>
            <!-- Color Range-->
            <section class="widget widget-categories">
              <h3 class="widget-title">Color</h3>
              <ul>
              	@foreach($info_Colors as $info_Color)
                    <li class=""><a rel="nofollow" href="{{ url("product-category/")."/".$slug }}/?filter_color={{ $info_Color->id }}">{{ $info_Color->color }}</a> <span>({{ $info_Color->count }})</span></li>
               	@endforeach
              </ul>
            </section>
            <!-- Size Range-->
            <section class="widget widget-categories">
              <h3 class="widget-title">Size</h3>
              <ul>
              	@foreach($info_Sizes as $info_Size)
                    <li class=""><a rel="nofollow" href="{{ url("product-category/")."/".$slug }}/?filter_size={{ $info_Size->id }}">{{ $info_Size->size }}</a> <span>({{ $info_Size->count }})</span></li>
               	@endforeach
              </ul>
            </section>
          </aside>
        </div>
      </div>
    </div>
@endsection
@push('scripts')
<script language="javascript">
	$(document).on('click', "#AddtoCart[data-cart]", function () {		
		var url = $(this).data('cart');
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'json',
			data: {"_token": "{{ csrf_token() }}" , submit: true},
			success: function(data)
			{
				var url = "{{ url('/').'/' }}";
				var urlImage = "";
				var cartUrl = "{{ url('/cart') }}";
				var checkoutUrl = "{{ url('/checkout') }}";
				var response = '';
				for(var x in data.cartsData) 
				{						
					
					response = response + '<tr><td><div class="product-item"><a class="product-thumb" href="'+url+data.cartsData[x]['slug']+'"><img src="'+urlImage+data.cartsData[x]['image']+'" alt="Product"></a><div class="product-info"><h4 class="product-title"><a href="'+url+data.cartsData[x]['slug']+'">'+data.cartsData[x]['name']+'</a></h4><span><em>Price:</em> $'+data.cartsData[x]['price']+'</span><span><em>Quantity:</em> '+data.cartsData[x]['qty']+'</span></div></div></td><td class="text-center"><a class="remove-from-cart" data-cart="'+data.cartsData[x]['id']+'" href="#"><i class="material-icons icon_close"></i></a></td></tr>';		
				}
				$('.cart-subtotal').html(data.totalAmount);
				$('.cart-data').html(response);
				$('.cart-count').html(data.counts);
				$('#cartAdded').val(1);	
			},
			error: function (result, status, err) {
				console.log(result.responseText);
			}
		});
	});
</script>
@endpush