@extends("layouts.app")
@section('content')
<div class="boxed-page page-title-hidden">
   <div class="row">
      <div class="large-8 columns large-centered">
         <div id="primary" class="content-area">
            <div id="content" class="site-content" role="main">
               <header class="entry-header " >
                  <div class="page_header_overlay"></div>
                  <div class="row">
                     <div class="large-12 columns">
                     </div>
                  </div>
               </header>
               <!-- .entry-header -->
               <div class="entry-content">
                  <div style=""data-vc-full-width="true" data-vc-full-width-init="false" class="normal_height vc_row wpb_row vc_row-fluid vc_custom_1537535064038 vc_row-has-fill contact-banner">
                     <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner">
                           <div class="wpb_wrapper">
                              <div class="row vc_row wpb_row vc_inner vc_row-fluid">
                                 <div class="wpb_column vc_column_container vc_col-sm-2">
                                    <div class="vc_column-inner">
                                       <div class="wpb_wrapper"></div>
                                    </div>
                                 </div>
                                 <div class="wpb_column vc_column_container vc_col-sm-8">
                                    <div class="vc_column-inner">
                                       <div class="wpb_wrapper">
                                          <div class="wpb_text_column wpb_content_element " >
                                             <div class="wpb_wrapper">
                                                <h1 style="text-align: center;"><span style="color: #000;">We are looking forward to hearing from you</span></h1>
                                             </div>
                                          </div>
                                          <div class="wpb_text_column wpb_content_element " >
                                             <div class="wpb_wrapper">
                                                <p style="text-align: center;"><span style="color: #000;">181 S. Whitfield St. Unit 8</span><br />
                                                   <span style="color: #000;">Nazareth PA , USA</span>
                                                </p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="wpb_column vc_column_container vc_col-sm-2">
                                    <div class="vc_column-inner">
                                       <div class="wpb_wrapper"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="vc_row-full-width vc_clearfix"></div>
                  <div style=""class="normal_height vc_row wpb_row vc_row-fluid vc_custom_1485437570456">
                     <div class="wpb_column vc_column_container vc_col-sm-4">
                        <div class="vc_column-inner">
                           <div class="wpb_wrapper">
                              <div class="wpb_text_column wpb_content_element " >
                                 <div class="wpb_wrapper">
                                    <h5>ADDRESS</h5>
                                    <p>181 S. Whitfield St. Unit 8<br />
                                       Nazareth PA , USA
                                    </p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="wpb_column vc_column_container vc_col-sm-4">
                        <div class="vc_column-inner">
                           <div class="wpb_wrapper">
                              <div class="wpb_text_column wpb_content_element " >
                                 <div class="wpb_wrapper">
                                    <h5>PHONE</h5>
                                    <p><a href="tel:1-610-3652156">1-610-3652156</a></p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="wpb_column vc_column_container vc_col-sm-4">
                        <div class="vc_column-inner">
                           <div class="wpb_wrapper">
                              <div class="wpb_text_column wpb_content_element " >
                                 <div class="wpb_wrapper">
                                    <h5>MAIL</h5>
                                    <p><a href="mailto:info@customhometex.com">info@customhometex.com</a></p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- .entry-content -->
            </div>
            <!-- #content -->           
         </div>
         <!-- #primary -->
      </div>
      <!--.large-8-->
   </div>
   <!--.row-->
</div>
<!-- .boxed-page -->
@endsection