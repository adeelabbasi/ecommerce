@extends("layouts.app")
@section('content')
<div class="full-width-page page-title-hidden">
   <div id="primary" class="content-area">
      <div id="content" class="site-content" role="main">
         <header class="entry-header " >
            <div class="page_header_overlay"></div>
            <div class="row">
               <div class="large-12 columns">
               </div>
            </div>
         </header>
         <!-- .entry-header -->
         <div class="entry-content">
            <div style=""class="adjust_cols_height vc_row wpb_row vc_row-fluid vc_row-o-full-height vc_row-o-columns-stretch vc_row-o-equal-height vc_row-o-equal-height vc_row-o-content-middle vc_row-flex">
               <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6 vc_col-has-fill">
                  <div class="vc_column-inner vc_custom_1537948109034 about-banner">
                     <div class="wpb_wrapper">
                        <div class="vc_empty_space"   style="height: 350px" ><span class="vc_empty_space_inner"></span></div>
                     </div>
                  </div>
               </div>
               <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                  <div class="vc_column-inner vc_custom_1484433918406" style="margin:110px">
                     <div class="wpb_wrapper">
                        <div class="wpb_text_column wpb_content_element " >
                           <div class="wpb_wrapper">
                              <h1 class="page-title on-shop" style="text-align: center;">Custom Home Tex</h1>
                           </div>
                        </div>
                        <div class="wpb_text_column wpb_content_element " >
                           <div class="wpb_wrapper">
                              <p style="text-align: center;"><span style="color: #808080;"><span class="char" title="Em Dash" data-decimal="8212" data-entity="&amp;#8212;" data-id="46410">—</span> Shop online</span></p>
                           </div>
                        </div>
                        <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_separator_no_text vc_sep_color_grey" ><span class="vc_sep_holder vc_sep_holder_l"><span  class="vc_sep_line"></span></span><span class="vc_sep_holder vc_sep_holder_r"><span  class="vc_sep_line"></span></span></div>
                        <div class="row vc_row wpb_row vc_inner vc_row-fluid vc_custom_1443414812412">
                           <div class="wpb_column vc_column_container vc_col-sm-12">
                              <div class="vc_column-inner">
                                 <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element " >
                                       <div class="wpb_wrapper">
                                          <p>Call us at CustomHomeTex or your first stop for your daily life needs. We hold a renowned name in offering the widest range of products ranging from shower curtains to rugs and from wall decals to mats.</p>
                                          <p>We don’t claim to be a decade old, neither we claim to be something out of this world, team CustomHomeTex is a team of ordinary guys who have been offering something exciting and a little different from traditional online stores.</p>
                                          <p>Be it anything or you need to grab something really cool to add much more glam to your room, home, shop or your personal life, tailor-made designs, state of the art materials involve and our love mingled with the 30 days money back guarantee will make you fall in love with our artistry.</p>
                                       </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element " >
                                       <div class="wpb_wrapper">
                                          <p><strong>Want to Know What Are Some Principles We Walk On?</strong></p>
                                          <p>Here at CustomHomeTex, we work on a single principle, i.e. to get the best thing for our prestigious clients.</p>
                                          <p>Moving on 3Ps Approach of Purity, Precision, and Perfection, we are always amid to get you a thing that should leave an everlasting impression on you</p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_separator_no_text vc_sep_color_grey" ><span class="vc_sep_holder vc_sep_holder_l"><span  class="vc_sep_line"></span></span><span class="vc_sep_holder vc_sep_holder_r"><span  class="vc_sep_line"></span></span></div>
                        <div class="row vc_row wpb_row vc_inner vc_row-fluid vc_custom_1412679223514">
                           <div class="wpb_column vc_column_container vc_col-sm-6">
                              <div class="vc_column-inner">
                                 <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element " >
                                       <div class="wpb_wrapper">
                                          <p><strong>CUSTOMER SERVICE</strong></p>
                                          <p>Phone: (1) 610-3652156</p>
                                          <p>mail: info@customhometex.com</p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="wpb_column vc_column_container vc_col-sm-6">
                              <div class="vc_column-inner">
                                 <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element " >
                                       <div class="wpb_wrapper">
                                          <p><strong>WHERE WE ARE</strong></p>
                                          <p>181 S. Whitfield St. Unit 8 Nazareth PA</p>
                                          <p>18064 USA</p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- .entry-content -->
      </div>
      <!-- #content -->           
   </div>
   <!-- #primary -->
</div>
</div>
<!-- #page_wrapper -->
@endsection