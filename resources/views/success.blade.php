@extends("layouts.app")
@section('content')
<div class="page-title">
  <div class="container">
    <h1>Thank you for your purchase!</h1>
    <ul class="breadcrumbs">
      <li><a href="{{ url('/') }}">Back to home</a>
      </li>
    </ul>
  </div>
</div>
    
@endsection