@extends("layouts.app")
@section('content')
<!-- Page Title-->
<div class="page-title">
  <div class="container">
    <h1>Checkout</h1>
    <ul class="breadcrumbs">
      <li><a href="index.html">Home</a>
      </li>
      <li class="separator">&nbsp;/&nbsp;</li>
      <li>Checkout</li>
    </ul>
  </div>
</div>
<!-- Page Content-->
<script src='https://js.stripe.com/v2/' type='text/javascript'></script>
{!! Form::open([ 'url' => '/payment', 'method' => 'post', 'id' => 'checkout', 'data-cc-on-file' => 'false', 'data-stripe-publishable-key' => env('STRIPE_PUB_KEY') ]) !!}
<div class="container padding-bottom-3x mb-2">
  <div class="row">
    <!-- Checkout Address-->
    <div class="col-xl-8 col-lg-7">
      <h4>Check out</h4>
      <hr class="padding-bottom-1x">
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label for="checkout-fn">First Name</label>
            <input class="form-control" required type="text" id="checkout-fn" name="first_name">
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label for="checkout-ln">Last Name</label>
            <input class="form-control" required type="text" id="checkout-ln"  name="last_name">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label for="checkout-email">E-mail Address</label>
            <input class="form-control" required type="email" id="checkout-email" name="email">
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label for="checkout-phone">Phone Number</label>
            <input class="form-control" required type="text" id="checkout-phone" name="phone">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label for="checkout-company">Company</label>
            <input class="form-control" type="text" id="checkout-company" name="company_name">
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label for="checkout-country">Country</label>
            <select class="form-control" required id="checkout-country" name="country">
              <option value="">Choose country</option>
              @foreach($Countries as $Country)
              <option value="{{ $Country->id }}">{{ $Country->name }}</option>
              @endforeach
            </select>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label for="checkout-city">City</label>
            <select class="form-control" required id="checkout-city" name="city">
              <option>Choose city</option>
            </select>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label for="checkout-zip">ZIP Code</label>
            <input class="form-control" type="text" id="checkout-zip" name="zip">
          </div>
        </div>
      </div>
      <div class="row padding-bottom-1x">
        <div class="col-sm-6">
          <div class="form-group">
            <label for="checkout-address1">Address 1</label>
            <input class="form-control" required type="text" id="checkout-address1" name="address">
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label for="checkout-address2">Address 2</label>
            <input class="form-control" type="text" id="checkout-address2" name="address2">
          </div>
        </div>
      </div>
      <hr class="padding-bottom-1x">
      <div class="d-flex justify-content-between"><a class="btn btn-outline-secondary m-0" href="{{ url('/cart') }}">Back To Cart</a>
      @if(Cart::Count()>0)
      <button type="submit" id="submitOrder" class="btn btn-primary m-0" href="{{ url('/') }}">Submit Order</button>
      @endif
      </div>
    </div>
    <!-- Sidebar          -->
    <div class="col-xl-4 col-lg-5">
      <aside class="sidebar">
        <div class="padding-top-2x hidden-lg-up"></div>
        <!-- Order Summary Widget-->
        <section class="widget widget-order-summary bg-secondary border-0 p-4">
          <h3 class="widget-title">Order Summary</h3>
          <table class="table text-sm mb-0">
            <tr>
              <td>Subtotal:</td>
              <td class="text-medium">${{ Cart::subtotal() }}</td>
            </tr>
            <tr>
              <td>Shipping:</td>
              <?php
              $shipping_cost = 0;
			  foreach(Cart::content() as $row)
              {
              	$shipping_cost+=$row->options->shipping_cost;
              }
              ?>            
              <td class="text-medium">${{ $shipping_cost }}</td>
            </tr>
            <tr>
              <td>Total:</td>
              <td class="text-lg text-medium">${{ Cart::subtotal()+$shipping_cost }}</td>
            </tr>
          </table>
        </section>
        <hr class="padding-bottom-1x">
        <section class="widget widget-order-summary bg-secondary border-0 p-4">
            <h3 class="widget-title">Payment Method</h3>
            <table class="table text-sm mb-0">
            	<tr>
            		<th>
            			<input type="radio" id="paymentType" name="paymentType" value="1" checked="checked"/> PayPal
            		</th>
            		<th>
            			<input type="radio" id="paymentType" name="paymentType" value="2"/> Stripe
            		</th>
            	<tr>
                <tr id="stripRow" style="display:none">
                	<td colspan="2">
                        <div class="row">
                            <div class="form-group col-sm-12">
                            	<img class="d-inline-block align-middle" src="{{ asset('home/img/cards.png') }}" style="width: 187px;" alt="Cerdit Cards">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                              <input class="form-control" type="text" id="cc-number" name="cc-number" placeholder="Card Number" value="4242 4242 4242 4242">
                            </div>
                            <div class="form-group col-sm-12">
                              <input class="form-control" type="text" id="cc-name" name="cc-name" placeholder="Full Name">
                            </div>
                            <div class="form-group col-sm-6">
                              <input class="form-control" type="text" id="cc-expiry-month" name="cc-expiry-month" placeholder="MM">
                            </div>
                            <div class="form-group col-sm-6">
                              <input class="form-control" type="text" id="cc-expiry-year" name="cc-expiry-year" placeholder="YY">
                            </div>
                            <div class="form-group col-sm-6">
                              <input class="form-control" type="text" id="cc-cvc" name="cc-cvc" placeholder="CVC">
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </section>
        
      </aside>
    </div>
  </div>
</div>
</form>
@endsection

@push('scripts')

<script language="javascript">
$( document ).ready(function() {
    $('#checkout-country').on('change', function (e) { 
		var country = $('#checkout-country option:selected').val();
		var url = "{{ url('/') }}"+"/country/cities/"+country;
		if(country=="")
			return false;
		//alert(url)
		$.ajax({
			url: url,
			type: 'GET',
			dataType: 'json',
			data: {method: '_GET', "_token": "{{ csrf_token() }}" , submit: true},
			success: function (response) {
				$("#checkout-city").empty();
				$("#checkout-city").append('<option>Choose city</option>');
				$.each(response,function(key,value){
					$("#checkout-city").append('<option value="'+key+'">'+value+'</option>');
				});
			},
			error: function (result, status, err) {
				console.log(result.responseText);
			},
		});
	});
	
	 $('input[type=radio][name=paymentType]').on('change', function() {
		if($(this).val()==2)
		{
			$("#stripRow").fadeIn();
			$('#ccname').prop('required',true);
			$('#ccnumber').prop('required',true);
			$('#ccexpiry').prop('required',true);
			$('#cccvc').prop('required',true);
		}
		else
		{
			$("#stripRow").fadeOut();
			$('#ccname').prop('required',false);
			$('#ccnumber').prop('required',false);
			$('#ccexpiry').prop('required',false);
			$('#cccvc').prop('required',false);
		
		}
	});
	 
	var $form = $("#checkout");
	$form.on('submit', function(e) {
		event.preventDefault();
		$("#submitOrder").attr("disabled", "disabled");
    	// fields validation
		if (!$form.data('cc-on-file')) {
		  e.preventDefault();
		  Stripe.setPublishableKey($form.data('stripe-publishable-key'));
		  Stripe.createToken({
			number: $('#cc-number').val(),
			cvc: $('#cc-cvc').val(),
			exp_month: $('#cc-expiry-month').val(),
			exp_year: $('#cc-expiry-year').val()
		  }, stripeResponseHandler);
		}
	});
	function stripeResponseHandler(status, response) {
		var token = response['id'];
		// insert the token into the form so it gets submitted to the server
		$form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
		$form.get(0).submit();
	}
});
</script>

@endpush