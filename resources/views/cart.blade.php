@extends("layouts.app")
@section('content')
<!-- Page Title-->
<div class="page-title">
  <div class="container">
    <h1>Cart</h1>
    <ul class="breadcrumbs">
      <li><a href="{{ url('/') }}">Home</a>
      </li>
      <li class="separator">&nbsp;/&nbsp;</li>
      <li>Cart</li>
    </ul>
  </div>
</div>
<form class="woocommerce-cart-form" action="{{ url('update-cart') }}" method="post">
@csrf
<!-- Page Content-->
<div class="container padding-bottom-3x mb-1">
  <!-- Shopping Cart-->
  <div class="table-responsive shopping-cart">
    <table class="table">
      <thead>
        <tr>
          <th>Product Name</th>
          <th class="text-center">Quantity</th>
          <th class="text-center">Price</th>
          <th class="text-center">Subtotal</th>
          <!--<th class="text-center"><a class="btn btn-sm btn-outline-danger" href="#">Clear Cart</a></th>-->
        </tr>
      </thead>
      <tbody>
        @foreach(Cart::content() as $row)
        <?php
			$color = "";
			$size = "";
			$sizeData = App\Models\Size::where('id',$row->options->size)->First();
			if($sizeData)
				$size = $sizeData->size;
			$colorData = App\Models\Color::where('id',$row->options->color)->First();
			if($colorData)
				$color = $colorData->size;
		?>
        <tr>
          <td>
            <div class="product-item"><a class="product-thumb" href="{{ url('/product/').'/'.$row->options->slug}}"><img src="{{ $row->options->image }}" alt="Product"></a>
              <div class="product-info">
                <h4 class="product-title"><a href="{{ url('/product/').'/'.$row->options->slug}}">{{ $row->name }}</a></h4><span><em>Color:</em> {{ $color }}</span><span><em>Size:</em> {{ $size }}</span>
              </div>
            </div>
          </td>
          <td class="text-center">
            <input type="hidden" name="id[]" value="{{ $row->id }}" />
            <input max="10" min="1" name="quantity[]" class="form-control" type="number" id="number-input" value="{{ $row->qty }}" style="width:100px">
          </td>
          <td class="text-center text-lg text-medium">${{ $row->price }}</td>
          <td class="text-center text-lg text-medium">${{ $row->price*$row->qty }}</td>
          <td class="text-center"><a class="remove-from-cart reload-cart" data-cart="{{ $row->id }}" href="#"><i class="material-icons icon_close"></i></a></td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <?php
  $shipping_cost = 0;
  foreach(Cart::content() as $row)
  {
	$shipping_cost+=$row->options->shipping_cost;
  }
  ?> 
  <div class="shopping-cart-footer">
    <div class="column text-lg">Subtotal: <span class="text-medium text-dark">${{ Cart::subtotal() }}</span><br />Shipping: <span class="text-medium text-dark">${{ $shipping_cost }}</span></div>
  </div>
  <div class="shopping-cart-footer">
    <div class="column text-lg">Total: <span class="text-medium text-dark">${{ Cart::subtotal()+$shipping_cost }}</span></div>
  </div>
  <div class="shopping-cart-footer">
    <div class="column"><a class="btn btn-outline-secondary" href="{{ url('/') }}"><i class="icon-arrow-left"></i>&nbsp;Back to Shopping</a></div>
    <div class="column"><button type="submit" class="btn btn-primary" href="#">Update Cart</button><a class="btn btn-success" href="{{ url('/checkout') }}">Checkout</a></div>
  </div>
</div>
</form>
@endsection

@push('scripts')

<script language="javascript">
	$(".reload-cart").on('click', function(e){
		window.location.reload();
	});
</script>

@endpush