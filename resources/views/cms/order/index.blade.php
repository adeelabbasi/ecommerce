<?php
$extendDomain = "cms.layout.app";
?>

@extends($extendDomain)
@section('content-header')
    <h1>
        Order
        <small>View all orders</small>
    </h1>
    <ol class="breadcrumb">
	    <li><a href="/cms"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><a href="/cms/order"><i class="fa fa-flag-checkered"></i> <span>Order</span></a></li>
    </ol>
@endsection
@section('content')
		<!-- TABLE: LATEST ORDERS -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Orders</h3>
              <div class="pull-right">
                  <div id="showForm">
                    <button class="btn btn-purple" id="viewButton" data-view="all">All</button>
                    <button class="btn btn-primary" id="viewButton" data-view="pending">Pending</button>
                    <button class="btn btn-info" id="viewButton" data-view="shipped">Shipped</button>
                    <button class="btn btn-success" id="viewButton" data-view="delivered">Delivered & Cash Received</button>
                    <button class="btn btn-danger" id="viewButton" data-view="return">Return</button>
                    <button class="btn btn-danger" id="viewButton" data-view="canceled">Canceled</button>
                  </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="viewForm" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Order#</th>                    
                    <th>Name</th>
                    <th>Contact#</th>
                    <th>Items</th>
                    <th>Sub Total</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
@endsection


@push('scripts')

<script type="text/javascript">
    $('#viewForm').DataTable({
        "processing": true,
        "serverSide": true,
		"ajax": "{{url('/cms/order/grid/all')}}",
        "columns": [
			{ data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
			{ data: 'phone', name: 'phone' },
			{ data: 'itemcounts', name: 'itemcounts' },
			{ data: 'amount', name: 'amount' },
			{ data: 'status', name: 'status' },
		]
    });
	
	$('#viewForm').on('change', '#status[data-id]', function (e) { 
		e.preventDefault();
		var url = "{{url('/')}}"+"/cms/order/"+$(this).data('id');
		//alert(url)
		//alert($(this).val())
		status = $(this).val();
		// confirm then
		$.ajax({
			url: url,
			type: 'PATCH',
			data: {"status":status , "_token": "{{ csrf_token() }}" , submit: true , "_method":'PATCH'},
			success: function(res)
			{
				$('#orderCounts').html(res);
				$('#viewForm').DataTable().draw(false);
			},
			error: function (result, status, err) {
				alert(result.responseText);
				alert(status.responseText);
				alert(err.Message);
			}
		})
		
	});
	
	$('#showForm').on('click', '#viewButton[data-view]', function (e) { 
		e.preventDefault();		 
		var view = $(this).data('view');
		
		$('#viewForm').DataTable().ajax.url( "{{url('/cms/order/grid/')}}/"+view ).load();
	});
</script>

@endpush