<?php
$extendDomain = "cms.layout.print";
?>

@extends($extendDomain)

@section('content')
	<!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> Custom Home Tex
            <small class="pull-right">Date: {{ date('d/m/Y') }}</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
          <address>
            <strong>Custom Home Tex</strong><br>
            181 S. Whitfield St. Unit 8 Nazareth PA , USA<br>
            Phone: 1-610-3652156<br>
            Email: info@customhometex.com
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong>{{ $info_Order->User()->First()->first_name." ".$info_Order->User()->First()->last_name }}</strong><br>
            {{ $info_Order->User()->First()->address }}<br>
            Phone: {{ $info_Order->User()->First()->phone }}<br>
            Email: {{ $info_Order->User()->First()->email }}
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <br>
          <b>Order #</b> {{ $info_Order->id }}<br>
          <b>Amount:</b> {{ $info_Order->amount }}
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Product</th>
              <th>Quantity</th>
              <th>Size</th>
              <th>Color</th>
              <th>Price</th>
              <th>Shipping Cost</th>
              <th>Amount</th>
            </tr>
            </thead>
            <tbody>
            <?php 
				$info_Order_Details = $info_Order->Order_detail;
				$i=0; 
				$shipping_cost=0;
			?>
            @foreach($info_Order_Details as $key => $info_Order_Detail)
                    <tr>
                      <td>{{ $info_Order_Detail->Product()->First()->title }}</td>
                      <td>{{ $info_Order_Detail->quantity }}</td>
                      <td>{{ $info_Order_Detail->size }}</td>
                      <td>{{ $info_Order_Detail->color }}</td>
                      <td>{{ $info_Order_Detail->Product()->First()->price }}</td>
                      <td>{{ $info_Order_Detail->Product()->First()->shipping_cost }}</td>
                      <td>{{ ($info_Order_Detail->quantity*$info_Order_Detail->Product()->First()->price)+$info_Order_Detail->Product()->First()->shipping_cost }}</td>
                    </tr>
            <?php
				$shipping_cost = $shipping_cost + $info_Order_Detail->Product()->First()->shipping_cost;
			?>
          	@endforeach
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <!--<p class="lead">Payment Methods:</p>
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Cash on delivery
          </p>-->
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Subtotal:</th>
                <td>Rs. {{ number_format((int)str_replace(',', '', $info_Order->amount),2) }}</td>
              </tr>
              <tr>
                <th style="width:50%">Shipping Cost:</th>
                <td>Rs. {{ $shipping_cost }}</td>
              </tr>
              <tr>
                <th>Total:</th>
                <td>Rs. {{ number_format(((int)str_replace(',', '', $info_Order->amount)) + $shipping_cost,2) }}</td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection