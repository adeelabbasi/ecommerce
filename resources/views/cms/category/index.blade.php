@extends("cms.layout.app")

@section('content-header')
    <h1>
        Categories
        <small>Categories detail</small>
    </h1>
    <ol class="breadcrumb">
    	<li><a href="/cms"><i class="fa fa-home"></i> Dashboard</a></li>
        <li class="active">Categories detail</li>
    </ol>
@endsection

@section('content')


@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif
 <div class="box">
    <div class="box-header">
      <h3 class="box-title">Categories detail</h3>
      &nbsp;&nbsp;&nbsp;&nbsp;
      <a href="{{ url('/cms/category/create') }}" class="btn btn-primary" style="float:right">Add new category</a>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
   
      <table id="viewForm" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>Category</th>
            <th>Slug</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->


@endsection

@push('scripts')

<script type="text/javascript">
    $('#viewForm').DataTable({
        "processing": true,
        "serverSide": true,
		"ajax": "{{url('/cms/category/grid')}}",
        "columns": [
            { data: 'category', name: 'category' },
			{ data: 'slug', name: 'slug' },
			{ data: 'edit', name: 'edit', orderable: false, searchable: false }
		]
    });
	
	$('#viewForm').on('click', '#btnDelete[data-remote]', function (e) { 
		if (confirm("Are you sure to delete category?")) {		
			e.preventDefault();		 
			var url = '{{url("/")}}'+$(this).data('remote');
			// confirm then
			$.ajax({
				url: url,
				type: 'DELETE',
				dataType: 'json',
				data: {method: '_DELETE', "_token": "{{ csrf_token() }}" , submit: true},
				error: function (result, status, err) {
					//alert(result.responseText);
					//alert(status.responseText);
					//alert(err.Message);
				},
			}).always(function (data) {
				$('#viewForm').DataTable().draw(false);
			});
		}
		return false;
	});
</script>

@endpush