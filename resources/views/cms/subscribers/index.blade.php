<?php
$extendDomain = "cms.layouts.cms";
?>

@extends($extendDomain)

@section('content-header')
    <h1>
        Customers
        <small>Show all customers</small>
    </h1>
    <ol class="breadcrumb">
	    <li><a href="/cms"><i class="fa fa-home"></i>Home</a></li>
        <li class=""><a href="/cms/customers"><i class="fa fa-circle-o"></i> <span>Customers</span></a></li>
        <li class="active">All Customers</li>
    </ol>
@endsection

@section('content')


@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif

 <div class="box">
    <div class="box-header">
      <h3 class="box-title">Customers Detail</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
   
      <table id="viewForm" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Mobile</th>
            <th>Address</th>
            <th>City</th>
        </tr>
        </thead>
        <tbody>
        </tfoot>
      </table>
      <table id="" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
        <tr>
            <td style="text-align:right">
            	<a href="{{ url('/cms/customers/export') }}" class="btn btn-primary"
                    onclick="event.preventDefault();
                             document.getElementById('export-form').submit();">
                    Export
                </a>

                <form id="export-form" action="{{ url('/cms/customers/export') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </td>
        </tr>
        </thead>
        <tbody>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</div>
@endsection

@push('scripts')

<script type="text/javascript">
    $('#viewForm').DataTable({
        "processing": true,
        "serverSide": true,
		"ajax": "{{url('/cms/customers/grid')}}",
        "columns": [
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
			{ data: 'mobile', name: 'mobile' },
			{ data: 'address', name: 'address' },
			{ data: 'city', name: 'city' },
		]
    });
	
	$('#viewForm').on('click', '#btnDelete[data-remote]', function (e) {
		if (confirm("Are you sure to delete this customer?")) {
			e.preventDefault();		 
			var url = $(this).data('remote');
			// confirm then
			$.ajax({
				url: url,
				type: 'DELETE',
				dataType: 'json',
				data: {method: '_DELETE', "_token": "{{ csrf_token() }}" , submit: true}
			}).always(function (data) {
				$('#viewForm').DataTable().draw(false);
			});
		}
		return false;
	});
</script>

@endpush