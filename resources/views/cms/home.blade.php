@extends("cms.layout.app")

@section('content-header')
    <h1>
        ERizQ
        <small>Version 1.1.0</small>
    </h1>
    <ol class="breadcrumb">
	    <li><a href="/cms/"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
@endsection

@section('content')
   	<div class="row">  
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Sales</span>
              <span class="info-box-number">0</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-orange"><i class="fa fa-flag-checkered"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Order</span>
              <span class="info-box-number">0</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

         <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Items Purchased</span>
              <span class="info-box-number">
              	0
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Products</span>
              <span class="info-box-number">0</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

	 <div class="row">
        <div class="col-md-12">
		<!-- TABLE: LATEST ORDERS -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Latest Orders</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="viewForm" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Order#</th>                    
                    <th>Name</th>
                    <th>Contact#</th>
                    <th>Items</th>
                    <th>Sub Total</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href="{{ url('/cms/order') }}" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
       </div>
       <!-- /.row --> 
        
@endsection



@push('scripts')


<script type="text/javascript">
    $('#viewForm').DataTable({
        "processing": true,
        "serverSide": true,
		"ajax": "{{url('/cms/order/grid/pending')}}",
        "columns": [
			{ data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
			{ data: 'phone', name: 'phone' },
			{ data: 'itemcounts', name: 'itemcounts' },
			{ data: 'amount', name: 'amount' },
			{ data: 'status', name: 'status' },
		]
    });
	
	$('#viewForm').on('change', '#status[data-id]', function (e) { 
		e.preventDefault();
		var url = "{{url('/')}}"+"/cms/order/"+$(this).data('id');
		//alert(url)
		//alert($(this).val())
		status = $(this).val();
		// confirm then
		$.ajax({
			url: url,
			type: 'PATCH',
			data: {"status":status , "_token": "{{ csrf_token() }}" , submit: true , "_method":'PATCH'},
			success: function(res)
			{
				$('#orderCounts').html(res);
				$('#viewForm').DataTable().draw(false);
			},
			error: function (result, status, err) {
				//alert(result.responseText);
				//alert(status.responseText);
				//alert(err.Message);
			}
		})
		
	});
	
</script>

@endpush