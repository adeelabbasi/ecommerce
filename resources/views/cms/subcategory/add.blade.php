@extends("cms.layout.app")

@section('content-header')
    <h1>
        Content
        <small>Add Subcategories</small>
    </h1>
    <ol class="breadcrumb">
    	<li><a href="/cms"><i class="fa fa-home"></i> Dashboard</a></li>
		<li class=""><a href="/cms/category"><i class="fa fa-circle-o"></i> <span>Subcategories</span></a></li>
        <li class="active">Add Subcategories</li>
    </ol>
@endsection

@section('content')
 {!! Form::open([
  'url' => '/cms/subcategory',
  'id' => 'main-form'
]) !!}

<div class="row">
<div class="col-md-6">
  <div class="box box-primary">
    <div class="box-header">
        
      <h3 class="box-title">
        <i class="fa fa-list"></i>
        Edit Subcategories
      </h3>
    </div>
    <div class="box-body">
      <div class="form-group">
        <label class="control-label col-sm-4"><span style="color:red;">*</span>Category:</label>
		<?php
            $defaultSelection = [''=>'Please Select'];
            foreach($info_Categories as $Categories)
            {
                $defaultSelection = $defaultSelection +  array($Categories->id => ($Categories->category));
            }
        ?>
        <div class="input-group afield col-sm-8">
            {!! Form::select('parent_id', $defaultSelection, null, array('class' => 'form-control select2' , 'id' => 'parent_id', 'required')) !!}
            @if ($errors->has('parent_id'))<p style="color:red;">Please select category</p>@endif
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-4 col-xs-12"><span style="color:red;">*</span>Category Name:</label>
        <div class="input-group afield col-sm-8 col-xs-12  my-colorpicker2">
            {!! Form::text('category', null, ['class' => 'form-control' , 'placeholder' => 'Category Name' , 'required', 'maxlength' => '50', 'id' => 'category']) !!}
            @if ($errors->has('category'))<p style="color:red;">{!!$errors->first('category')!!}</p>@endif
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-4 col-xs-12"><span style="color:red;">*</span>Slug:</label>
        <div class="input-group afield col-sm-8 col-xs-12  my-colorpicker2">
            {!! Form::text('slug', null, ['class' => 'form-control' , 'placeholder' => 'Slug' , 'required', 'maxlength' => '50', 'id' => 'slug']) !!}
            @if ($errors->has('slug'))<p style="color:red;">{!!$errors->first('slug')!!}</p>@endif
        </div>
      </div>
      <!-- /.form group -->
      <div class="col-md-2">
          <label></label>
          {!! Form::submit('Save', ['class' => 'form-control btn btn-primary']) !!}
      </div>
   </div>
  </div>
</div>
<!-- /.col -->
</div>
  <!-- /.row -->
  
  
{!! Form::close() !!}

@endsection