@extends("cms.layout.app")

@section('content-header')
    <h1>
        Content
        <small>Add Brands</small>
    </h1>
    <ol class="breadcrumb">
    	<li><a href="/cms"><i class="fa fa-home"></i> Dashboard</a></li>
		<li class=""><a href="/cms/brand"><i class="fa fa-circle-o"></i> <span>Brands</span></a></li>
        <li class="active">Add Brands</li>
    </ol>
@endsection

@section('content')
 {!! Form::open([
  'url' => '/cms/brand',
  'id' => 'main-form'
]) !!}

<div class="row">
<div class="col-md-6">
  <div class="box box-primary">
    <div class="box-header">
        
      <h3 class="box-title">
        <i class="fa fa-list"></i>
        Edit Brands
      </h3>
    </div>
    <div class="box-body">
      <div class="form-group">
        <label class="control-label col-sm-4 col-xs-12"><span style="color:red;">*</span>Brand Name:</label>
        <div class="input-group afield col-sm-8 col-xs-12  my-colorpicker2">
            {!! Form::text('brand', null, ['class' => 'form-control' , 'placeholder' => 'Brand Name' , 'required', 'maxlength' => '50', 'id' => 'brand']) !!}
            @if ($errors->has('brand'))<p style="color:red;">{!!$errors->first('brand')!!}</p>@endif
        </div>
      </div>
      <!-- /.form group -->
      <div class="col-md-2">
          <label></label>
          {!! Form::submit('Save', ['class' => 'form-control btn btn-primary']) !!}
      </div>
   </div>
  </div>
</div>
<!-- /.col -->
</div>
  <!-- /.row -->
  
  
{!! Form::close() !!}

@endsection