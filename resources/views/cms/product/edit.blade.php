@extends("cms.layout.app")

@section('content-header')
    <h1>
        Product
        <small>Add product (Fields marked with (<span style="color:red;">*</span>) are mandatory.)</small>
    </h1>
    <ol class="breadcrumb">
	    <li><a href="/cms"><i class="fa fa-home"></i> Dashboard</a></li>
        <li><a href="/cms/product"><i class="fa fa-product-hunt"></i> <span>Product</span></a></li>
        <li class="active">Add Product</li>
    </ol>
@endsection

@section('content')
  <div class="row">
	<div class="col-md-12">
      <div class="box box-info">
        <div class="box-header">
        	
          <h3 class="box-title">
            <i class="fa fa-list"></i>
            Product Info
          </h3>
        </div>
        <div class="box-body">          
          {!! Form::model($info_Product, [
                'method' => 'PATCH',
                'files' => true,
                'url' => ['/cms/product', $info_Product->id]
          ]) !!}          
          <div class="form-group">
          	<div class="row">
                <div class="col-md-6">
                    <label class="control-label col-sm-4"><span style="color:red;">*</span>Title:</label>
                    <div class="input-group afield col-sm-8">
                        {!! Form::text('title', null, ['class' => 'form-control' , 'placeholder' => 'Enter Title' , 'required' , 'maxlength' => '100']) !!}
                    </div>
                    @if ($errors->has('title'))<p style="color:red;">{!!$errors->first('title')!!}</p>@endif
                </div>
                <div class="col-md-6">
                    <label class="control-label col-sm-4"><span style="color:red;">*</span>Slug:</label>
                    <div class="input-group afield col-sm-8">
                        {!! Form::text('slug', null, ['class' => 'form-control' , 'placeholder' => 'Enter Slug' , 'required' , 'maxlength' => '100']) !!}
                    </div>
                    @if ($errors->has('slug'))<p style="color:red;">{!!$errors->first('slug')!!}</p>@endif
                </div>
            </div>
          </div>
          <!-- /.form group -->
          
		  <div class="form-group">
          	<div class="row">
                <div class="col-md-6">
                    <label class="control-label col-sm-4"><span style="color:red;">*</span>Category:</label>
                    <?php
                        $defaultSelection = [''=>'Please Select'];
                        foreach($info_Categories as $Categories)
                        {
                            $defaultSelection = $defaultSelection +  array($Categories->id => ($Categories->category));
                        }
                    ?>
                    <div class="input-group afield col-sm-8">
                        {!! Form::select('category_id', $defaultSelection, null, array('class' => 'form-control' , 'id' => 'Category', 'required')) !!}
                    </div>
                    @if ($errors->has('category_id'))<p style="color:red;">Please select category</p>@endif
                </div>
                <div class="col-md-6">
                    <label class="control-label col-sm-4"><span style="color:red;">*</span>Subcategory:</label>
                    <?php
                        $defaultSelection = [''=>'Please Select'];
                        foreach($info_SubCategories as $Categories)
                        {
                            $defaultSelection = $defaultSelection +  array($Categories->id => ($Categories->category));
                        }
                    ?>
                    <div class="input-group afield col-sm-8">
                        {!! Form::select('sub_category_id', $defaultSelection, null, array('class' => 'form-control' , 'id' => 'SubCategory', 'required')) !!}
                    </div>
                    @if ($errors->has('sub_category_id'))<p style="color:red;">Please select category</p>@endif
                </div>
            </div>
          </div>
          <!-- /.form group -->
          <div class="form-group">
          	<div class="row">
                <div class="col-md-6">
                    <label class="control-label col-sm-4 col-xs-12"><span style="color:red;">*</span>Price:</label>
                    <div class="input-group afield col-sm-8  col-xs-12 ">
                      {!! Form::text('price', null, ['class' => 'form-control' , 'placeholder' => 'Enter Price' , 'required', 'maxlength' => '6', 'id' => 'price']) !!}
                    </div>
                      @if ($errors->has('price'))<p style="color:red;">{!!$errors->first('price')!!}</p>@endif
                </div>
                <div class="col-md-6">
					<label class="control-label col-sm-4 col-xs-12">Cross Price:</label>
                    <div class="input-group afield col-sm-8  col-xs-12 ">
                      {!! Form::text('price_2', null, ['class' => 'form-control' , 'placeholder' => 'Enter Cross Price', 'maxlength' => '6', 'id' => 'price_2']) !!}
                    </div>
                      @if ($errors->has('price_2'))<p style="color:red;">Please select cross price</p>@endif

                </div>
            </div>
          </div>
          <!-- /.form group -->
          
          <div class="form-group">
          	<div class="row">
                <div class="col-md-6">
                    <label class="control-label col-sm-4 col-xs-12">Shipping Cost:</label>
                    <div class="input-group afield col-sm-8  col-xs-12 ">
                      {!! Form::text('shipping_cost', null, ['class' => 'form-control' , 'placeholder' => 'Enter Cost', 'maxlength' => '6']) !!}
                    </div>
                      @if ($errors->has('shipping_cost'))<p style="color:red;">{!!$errors->first('shipping_cost')!!}</p>@endif
                </div>
                <div class="col-md-6">
                    <label class="control-label col-sm-4">Brand:</label>
                    <?php
                        $defaultSelection = [''=>'Please Select'];
                        foreach($info_Brands as $Brands)
                        {
                            $defaultSelection = $defaultSelection +  array($Brands->id => ($Brands->brand));
                        }
                    ?>
                    <div class="input-group afield col-sm-8">
                        {!! Form::select('brand_id', $defaultSelection, null, array('class' => 'form-control' , 'id' => 'brand_id')) !!}
                    </div>
                    @if ($errors->has('brand_id'))<p style="color:red;">Please select brand</p>@endif
                </div>
            </div>
          </div>
          <!-- /.form group -->
          
          <div class="form-group">
          	<div class="row">
                <div class="col-md-6">
                    <label class="control-label col-sm-4 col-xs-12">Quantity:</label>
                    <div class="input-group afield col-sm-8  col-xs-12 ">
                      {!! Form::text('quantity', null, ['class' => 'form-control' , 'placeholder' => 'Enter Quantity', 'maxlength' => '4']) !!}
                    </div>
                      @if ($errors->has('quantity'))<p style="color:red;">{!!$errors->first('quantity')!!}</p>@endif
                </div>
                <div class="col-md-6">
                     <label class="control-label col-sm-4 col-xs-12">Warranty:</label>
                    <div class="input-group afield col-sm-8  col-xs-12 ">
                      {!! Form::text('warranty', null, ['class' => 'form-control' , 'placeholder' => 'Enter Warranty( e.g 2 Years)' , 'maxlength' => '10']) !!}
                    </div>
                      @if ($errors->has('warranty'))<p style="color:red;">{!!$errors->first('warranty')!!}</p>@endif
                </div>
            </div>
          </div>
          <!-- /.form group -->
                   
          <div class="form-group">
          	<div class="row">
                <div class="col-md-6">
                    <label class="control-label col-sm-4 col-xs-12">Weight:</label>
                    <div class="input-group afield col-sm-8  col-xs-12 ">
                      {!! Form::text('weight', null, ['class' => 'form-control' , 'placeholder' => 'Enter Weight']) !!}
                    </div>
                      @if ($errors->has('weight'))<p style="color:red;">{!!$errors->first('weight')!!}</p>@endif
                </div>
                <div class="col-md-6">
                    <label class="control-label col-sm-4 col-xs-12"><span style="color:red;">*</span>SKU:</label>
                    <div class="input-group afield col-sm-8  col-xs-12 ">
                      {!! Form::text('sku', null, ['class' => 'form-control' , 'placeholder' => 'Enter SKU', 'required']) !!}
                    </div>
                      @if ($errors->has('sku'))<p style="color:red;">{!!$errors->first('sku')!!}</p>@endif
                </div>
            </div>
          </div>
          <!-- /.form group -->
          <div class="form-group">
          	<div class="row">
                <div class="col-md-6">
                    <label class="control-label col-sm-4 col-xs-12">Dimension:</label>
                    <div class="input-group afield col-sm-8  col-xs-12 ">
                      {!! Form::text('dimension', null, ['class' => 'form-control' , 'placeholder' => 'Enter Dimension']) !!}
                    </div>
                      @if ($errors->has('dimension'))<p style="color:red;">{!!$errors->first('dimension')!!}</p>@endif
                </div>
                <div class="col-md-6">
                    <label class="control-label col-sm-4 col-xs-12">Material:</label>
                    <div class="input-group afield col-sm-8  col-xs-12 ">
                      {!! Form::text('material', null, ['class' => 'form-control' , 'placeholder' => 'Enter Material']) !!}
                    </div>
                      @if ($errors->has('material'))<p style="color:red;">{!!$errors->first('material')!!}</p>@endif
                </div>
            </div>
          </div>
          <!-- /.form group -->
          <div class="form-group">
          	<div class="row">
            	<div class="col-md-6">
                    <label class="control-label col-sm-4 col-xs-12">Manufacturer:</label>
                    <div class="input-group afield col-sm-8  col-xs-12 ">
                      {!! Form::text('manufacturer', null, ['class' => 'form-control' , 'placeholder' => 'Enter Manufacturer']) !!}
                    </div>
                      @if ($errors->has('manufacturer'))<p style="color:red;">{!!$errors->first('manufacturer')!!}</p>@endif
                </div>
                <div class="col-md-6">
                    <label class="control-label col-sm-4 col-xs-12">Parent:</label>
                    <div class="input-group afield col-sm-8  col-xs-12 ">
                      {!! Form::text('parent', null, ['class' => 'form-control' , 'placeholder' => 'Enter Parent Slug']) !!}
                    </div>
                      @if ($errors->has('parent'))<p style="color:red;">{!!$errors->first('parent')!!}</p>@endif
                </div>
            </div>
          </div>
          <!-- /.form group -->
          <div class="form-group">
          	<div class="row">
            	<div class="col-md-6">
                    <label class="control-label col-sm-4 col-xs-12">Manufacturer:</label>
                    <div class="input-group afield col-sm-8  col-xs-12 ">
                      {!! Form::text('manufacturer', null, ['class' => 'form-control' , 'placeholder' => 'Enter Manufacturer']) !!}
                    </div>
                      @if ($errors->has('manufacturer'))<p style="color:red;">{!!$errors->first('manufacturer')!!}</p>@endif
                </div>
                <div class="col-md-6">
                    <label class="control-label col-sm-4 col-xs-12">Status:</label>
                    <div class="input-group afield col-sm-8  col-xs-12 ">
                      {!! Form::radio('status', '1', ['selected']) !!}&nbsp;Active&nbsp;&nbsp;
                      {!! Form::radio('status', '0') !!}&nbsp;Inactive
                    </div>
                      @if ($errors->has('status'))<p style="color:red;">{!!$errors->first('status')!!}</p>@endif
                </div>
            </div>
          </div>
          <!-- /.form group -->
          <div class="form-group">
          	<div class="row">
                <div class="col-md-6">
                    <label class="control-label col-sm-4 col-xs-12">Color If Any:</label>
                    <div class="input-group afield col-sm-8  col-xs-12 ">
                      <?php
                        $defaultSelection = [];
                        foreach($info_Colors as $Colors)
                        {
                            $defaultSelection = $defaultSelection +  array($Colors->id => ($Colors->color));
                        }
                      ?>
                      {!! Form::select('color_if_any[]', $defaultSelection, null, array('class' => 'form-control select2' , 'id' => 'color_if_any' , 'multiple' => 'multiple' )) !!}
                    </div>
                      @if ($errors->has('color_if_any'))<p style="color:red;">{!!$errors->first('color_if_any')!!}</p>@endif
                </div>
                <div class="col-md-6">
                    <label class="control-label col-sm-4 col-xs-12">Size If Any:</label>
                    <div class="input-group afield col-sm-8  col-xs-12 ">
                      <?php
                        $defaultSelection = [];
                        foreach($info_Sizes as $Sizes)
                        {
                            $defaultSelection = $defaultSelection +  array($Sizes->id => ($Sizes->size));
                        }
                      ?>
                      {!! Form::select('size_if_any[]', $defaultSelection, null, array('class' => 'form-control select2' , 'id' => 'size_if_any' , 'multiple' => 'multiple')) !!}
                    </div>
                      @if ($errors->has('size_if_any'))<p style="color:red;">{!!$errors->first('size_if_any')!!}</p>@endif
                  </div>
            </div>
          </div>
          <!-- /.form group -->
          <div class="form-group">
            <label class="control-label col-sm-2 col-xs-12"><span style="color:red;">*</span>Desription:</label>
            <div class="input-group afield col-sm-10  col-xs-12 ">
            	{!! Form::textarea('description', null, ['id' => 'description' , 'class' => 'form-control' , 'placeholder' => 'Enter Description (sperate by "|")' , 'required' , 'maxlength' => '255' , 'rows' => '3']) !!}
            </div>
            @if ($errors->has('description'))<p style="color:red;">{!!$errors->first('description')!!}</p>@endif
          </div>
          <!-- /.form group -->    
          <div class="form-group">
            <label class="control-label col-sm-2 col-xs-12"><span style="color:red;">*</span>Feature:</label>
            <div class="input-group afield col-sm-10  col-xs-12 ">
            	{!! Form::textarea('feature', null, ['id' => 'feature' , 'class' => 'form-control' , 'placeholder' => 'Enter Feature (sperate by "|")' , 'required' , 'maxlength' => '255' , 'rows' => '3']) !!}
            </div>
            @if ($errors->has('description'))<p style="color:red;">{!!$errors->first('description')!!}</p>@endif
          </div>
          <!-- /.form group -->   
          
          <div class="form-group">
          	<div class="row">
                <div class="col-md-12">
                    <label class="control-label col-sm-2 col-xs-12">Images:</label>
                    <div class="col-sm-3  col-xs-12 ">
                      	{!! Form::file('a_img', null, ['class' => 'form-control']) !!}
                        Image 1
                    	@if ($errors->has('a_img'))<p style="color:red;">{!!$errors->first('a_img')!!}</p>@endif
                    </div>
                    <div class="col-sm-3  col-xs-12 ">
                      	{!! Form::file('b_img', null, ['class' => 'form-control']) !!}
                        Image 2
                    	@if ($errors->has('b_img'))<p style="color:red;">{!!$errors->first('b_img')!!}</p>@endif
                    </div>
                    <div class="col-sm-3  col-xs-12 ">
                      	{!! Form::file('c_img', null, ['class' => 'form-control']) !!}
                        Image 3
                    	@if ($errors->has('c_img'))<p style="color:red;">{!!$errors->first('c_img')!!}</p>@endif
                    </div>
                </div>
            </div>
          </div>
          <div class="form-group">
          	<div class="row">
                <div class="col-md-12">
                    <label class="control-label col-sm-2 col-xs-12"></label>
                    <div class="col-sm-3  col-xs-12 ">
                      	{!! Form::file('d_img', null, ['class' => 'form-control']) !!}
                        Image 1
                    	@if ($errors->has('d_img'))<p style="color:red;">{!!$errors->first('d_img')!!}</p>@endif
                    </div>
                    <div class="col-sm-3  col-xs-12 ">
                      	{!! Form::file('e_img', null, ['class' => 'form-control']) !!}
                        Image 2
                    	@if ($errors->has('e_img'))<p style="color:red;">{!!$errors->first('e_img')!!}</p>@endif
                    </div>
                </div>
            </div>
          </div>
          <div class="form-group">
          	<div class="row">
                <div class="col-md-12">
                    <label class="control-label col-sm-2 col-xs-12">Previous Images:</label>
                    <div class="col-sm-3  col-xs-12 ">
                      	<img src="{{ url('images/products/'.$info_Product->a_img) }}" alt="None" width="100%"/>
                    </div>
                    <div class="col-sm-3  col-xs-12 ">
                      	<img src="{{ url('images/products/'.$info_Product->b_img) }}" alt="None" width="100%"/>
                    </div>
                    <div class="col-sm-3  col-xs-12 ">
                      	<img src="{{ url('images/products/'.$info_Product->c_img) }}" alt="None" width="100%"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
          	<div class="row">
                <div class="col-md-12">
                    <label class="control-label col-sm-2 col-xs-12"></label>
                    <div class="col-sm-3  col-xs-12 ">
                      	<img src="{{ url('images/products/'.$info_Product->d_img) }}" alt="None" width="100%"/>
                    </div>
                    <div class="col-sm-3  col-xs-12 ">
                      	<img src="{{ url('images/products/'.$info_Product->e_img) }}" alt="None" width="100%"/>
                    </div>
                </div>
            </div>
          </div>
          <!-- /.form group -->
          
          <div class="col-md-1">
              <label></label>
              {!! Form::submit('Save', ['class' => 'form-control btn btn-primary']) !!}
          </div>
  
		  {!! Form::close() !!}
          
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
	</div>
    <!-- /.col-md-12 -->
  </div>
  <!-- /.row -->
</div>

@endsection

@push('scripts')

<script type="text/javascript">
	
	$('#main-form').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                validators: {
                    notEmpty: {
                        message: 'Please enter title'
                    }
                }
            },
			slug: {
                validators: {
                    notEmpty: {
                        message: 'Please enter slug'
                    }
                }
            },
			sku: {
                validators: {
                    notEmpty: {
                        message: 'Please enter sku'
                    }
                }
            },
			description: {
                validators: {
                    notEmpty: {
                        message: 'Please enter description'
                    }
                }
            },
			feature: {
                validators: {
                    notEmpty: {
                        message: 'Please enter description'
                    }
                }
            },
			category_id: {
                validators: {
                    notEmpty: {
                        message: 'Please select category'
                    }
                }
            },
			sub_category_id: {
                validators: {
                    notEmpty: {
                        message: 'Please select sub category'
                    }
                }
            },
			price: {
                validators: {
                    notEmpty: {
                        message: 'Please enter price in number'
                    },
					integer: {
                        message: 'Please enter price in number'
                    }
                }
            },
			price_2: {
                validators: {
					integer: {
                        message: 'Please enter price 2 in number'
                    }
                }
            },
			quantity: {
                validators: {
					integer: {
                        message: 'Please enter quantity in number'
                    }
                }
            },
			weight: {
                validators: {
					integer: {
                        message: 'Please enter weight in number'
                    }
                }
            },
			shipping_cost: {
                validators: {
					integer: {
                        message: 'Please enter shipping cost in number'
                    }
                }
            },
        }
    });

	//CKEDITOR.replace('description');
	
	$('#Category').on('change', function (e) { 
		var category_id = $('#Category option:selected').val();
		if(category_id=="")
		{
			return false;
		}
		
		var baseUrl = $('meta[name="base-url"]').attr('content');
		var url = baseUrl+'/cms/category/subcategory/'+category_id;
		//alert(url)
		$.ajax({
			url: url,
			type: 'GET',
			dataType: 'json',
			data: {method: '_GET', "_token": "{{ csrf_token() }}" , submit: true},
			success: function (response) {
				$("#SubCategory").empty();
				$("#SubCategory").append('<option>Please Select</option>');

				$.each(response,function(key,value){
					$("#SubCategory").append('<option value="'+key+'">'+value+'</option>');
				});
			},
			error: function (result, status, err) {
				alert(result.responseText);
				alert(status.responseText);
				alert(err.Message);
			},
		});
	});

</script>

@endpush