@extends("cms.layout.app")

@section('content-header')
    <h1>
        Product
        <small>Add product</small>
    </h1>
    <ol class="breadcrumb">
	    <li><a href="/cms"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="/cms/product"><i class="fa fa-product-hunt"></i> <span>Product</span></a></li>
        <li class="active">Import(Bulk) Products</li>
    </ol>
@endsection

@section('content')
@if(Session::has('error'))
    <div class="alert alert-danger">
        {{ Session::get('error') }}
    </div>
@endif

  <div class="row">
	<div class="col-md-12">
      <div class="box box-info">
        <div class="box-header">
        	
          <h3 class="box-title">
            <i class="fa fa-list"></i>
            Import Bulk Products
          </h3>
        </div>
        <div class="box-body">          
          {!! Form::open(['url' => '/cms/importProducts', 'files' => true , 'id' => 'main-form']) !!}
                    
          <div class="form-group">
            <label class="control-label col-sm-2 col-xs-12"><span style="color:red;">*</span>Excel file:</label>
            <div class="input-group afield col-sm-8 col-xs-12 ">
                {!! Form::file('importProducts') !!}
            </div>
            @if ($errors->has('importProducts'))<p style="color:red;">{!!$errors->first('importProducts')!!}</p>@endif
          </div>
          <!-- /.form group -->
          <div class="form-group">
            <div class="input-group afield col-sm-10 col-xs-12 ">
            	<p></p>
                <p>Upload bulk products, Please follow the (<a href="{{ asset('/') }}upload.xls">Sample</a>) excel format</p>
                <p>For update products, please enter Products Id's in Product_ID fields</p>
                <p>You can get all of your products with product Id's from <a href="{{ url('/cms/products/showExportProducts') }}"> Back up(Export)</a> </p>
            </div>
          </div>
          <!-- /.form group -->
          {!! Form::submit('Upload', ['class' => 'btn btn-primary']) !!}
  
		  {!! Form::close() !!}
          
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
	</div>
    <!-- /.col-md-12 -->
  </div>
  <!-- /.row -->
</div>

@endsection

@push('scripts')

<script type="text/javascript">
	
	
	$('#main-form').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            bulk_products: {
                validators: {
                    notEmpty: {
                        message: 'Please select excel file'
                    }
                }
            },
        }
    });

</script>

@endpush