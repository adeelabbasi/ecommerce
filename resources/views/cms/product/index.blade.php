@extends("cms.layout.app")

@section('content-header')
    <h1>
        Product
        <small>Show all products</small>
    </h1>
    <ol class="breadcrumb">
	    <li><a href="/cms"><i class="fa fa-home"></i> Dashboard</a></li>
        <li class="active">All Products</li>
    </ol>
@endsection

@section('content')


@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif

 <div class="box">
    <div class="box-header">
      <h3 class="box-title">Products Detail</h3>
      &nbsp;&nbsp;&nbsp;&nbsp;
      <a href="{{ url('/cms/product/create') }}" class="btn btn-primary" style="float:right">Add new product</a>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
   
      <table id="viewForm" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
        <tr>
        	<th width="5%">ID</th>
            <th>Title</th>
            <th>Price</th>
            <th>Category</th>
            <th>Subcategory</th>
            <th width="20%">Image</th>
            <th width="5%">Status</th>
            <th width="10%">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($info_Products as $Products)
        	<tr>
                <td>{{ $Products->id }}</td>
                <td>{{ $Products->title }}</td>
                <td>{{ $Products->price }}</td>
                <td>{{ $Products->Category()->First()->category }}</td>
                <td>{{ $Products->Subcategory()->First()->category }}</td>
                <td><img src="{{ $Products->a_img }}" width="100%"/></td>
                <td>{{ $Products->status }}</td>
                <td>
                	<div class="btn-group btn-group-action">
                            <a class="btn btn-info" style="margin-right:2px;" href="{{ url('/cms/product/'.$Products->id.'/edit') }}" title="Edit Data"><i class="fa fa-pencil"></i></a> 
                            <a class="btn btn-danger" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-remote="/cms/product/{{ $Products->id }}"><i class="fa fa-trash"></i></a>
                    </div>
               	</td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        	<tr>
            	<td colspan="8" align="right">{{ $info_Products->links() }}</td>
            </tr>
        </tfoot>
      </table>     
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
@endsection

@push('style')
@endpush
@push('scripts')

<script type="text/javascript">
	$('#viewForm').on('click', '#btnDelete[data-remote]', function (e) {
		if (confirm("Are you sure to delete this product?")) {
			e.preventDefault();		 
			var url = $(this).data('remote');
			// confirm then
			$.ajax({
				url: url,
				type: 'DELETE',
				dataType: 'json',
				data: {method: '_DELETE', "_token": "{{ csrf_token() }}" , submit: true}
			});
			$(this).parents("tr").fadeOut();
		}
		return false;
	});
</script>

@endpush