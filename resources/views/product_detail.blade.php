@extends("layouts.app")
@section('content')
<!-- Leave a Review-->
{!! Form::open(['url' => 'review', 'files' => true , 'class' => 'modal fade',  'id' => 'leaveReview']) !!} 
  <input type="hidden" name="pid" value="{{  $info_Product->id }}" />
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Leave a Review</h4>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for="review-name">Your Name</label>
              <input name="name" class="form-control" type="text" id="review-name" required>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="review-rating">Rating</label>
              <select name="rating" class="form-control" id="review-rating">
                <option value="5">5 Stars</option>
                <option value="4">4 Stars</option>
                <option value="3">3 Stars</option>
                <option value="2">2 Stars</option>
                <option value="1">1 Star</option>
              </select>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="review-message">Review</label>
          <textarea class="form-control" name="message" id="review-message" rows="8" required></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="submit">Submit Review</button>
      </div>
    </div>
  </div>
</form>
<!-- Page Content-->
<div class="bg-secondary pb-4 padding-top-3x">
  <div class="container">
    <div class="row">
      <!-- Product Gallery-->
      <div class="col-md-6 mb-30">
        <div class="product-gallery"><!--<span class="product-badge text-danger">Sale</span>-->
          <div class="product-carousel owl-carousel gallery-wrapper">
            @if($info_Product->a_img!="")
            <div class="gallery-item" data-hash="one"><a href="{{ $info_Product->a_img }}" data-size="555x480"><img src="{{ $info_Product->a_img }}" alt="Product"></a></div>
            @endif
            @if($info_Product->b_img!="")
            <div class="gallery-item" data-hash="two"><a href="{{ $info_Product->b_img }}" data-size="555x480"><img src="{{ $info_Product->b_img }}" alt="Product"></a></div>
            @endif
            @if($info_Product->c_img!="")
            <div class="gallery-item" data-hash="three"><a href="{{ $info_Product->c_img }}" data-size="555x480"><img src="{{ $info_Product->c_img }}" alt="Product"></a></div>
            @endif
            @if($info_Product->d_img!="")
            <div class="gallery-item" data-hash="four"><a href="{{ $info_Product->d_img }}" data-size="555x480"><img src="{{ $info_Product->d_img }}" alt="Product"></a></div>
            @endif
            @if($info_Product->e_img!="")
            <div class="gallery-item" data-hash="five"><a href="{{ $info_Product->e_img }}" data-size="555x480"><img src="{{ $info_Product->e_img }}" alt="Product"></a></div>
            @endif
          </div>
          <ul class="product-thumbnails">
            @if($info_Product->a_img!="")
            <li class="active"><a href="#one"><img src="{{ $info_Product->a_img }}" alt="Product"></a></li>
            @endif
            @if($info_Product->b_img!="")
            <li class="active"><a href="#two"><img src="{{ $info_Product->b_img }}" alt="Product"></a></li>
            @endif
            @if($info_Product->c_img!="")
            <li class="active"><a href="#three"><img src="{{ $info_Product->c_img }}" alt="Product"></a></li>
            @endif
            @if($info_Product->d_img!="")
            <li class="active"><a href="#four"><img src="{{ $info_Product->d_img }}" alt="Product"></a></li>
            @endif
            @if($info_Product->e_img!="")
            <li class="active"><a href="#five"><img src="{{ $info_Product->e_img }}" alt="Product"></a></li>
            @endif
          </ul>
        </div>
      </div>
      <!-- Product Info-->
      <div class="col-md-6 mb-30">
        <div class="card border-default bg-white pt-2 box-shadow">
          <div class="card-body">
            <h2 class="mb-3">{{ $info_Product->title }}</h2>
            <h3 class="text-normal">
                @if($info_Product->price_2!="")
                <del class='text-muted'>${{ $info_Product->price_2 }}</del> 
                @endif
            	${{ $info_Product->price }}
            </h3>
            <p class="text-sm text-muted">{!! str_replace("|", "<br />", $info_Product->description) !!}</p>
            <div class="row">
              @if($info_Product->Color()->Get()->Count()>0)
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="sp-color">Color:</label>
                      <select id="color" name="color" class="form-control" id="sp-color">
                        @foreach($info_Product->Color()->Get() as $Color)
                        <option value="{{ $Color->id }}">{{ $Color->color }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
              @endif
              @if($info_Product->Size()->Get()->Count()>0)
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="sp-color">Size:</label>
                      <select id="size" name="size" class="form-control" id="sp-color">
                        @foreach($info_Product->Size()->Get() as $Size)
                        <option value="{{ $Size->id }}">{{ $Size->size }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
              @endif
            </div>
            <div class="row align-items-end mb-4">
              <div class="col-sm-4">
                <div class="form-group mb-0">
                  <label for="sp-quantity">Quantity:</label>
                  <select id="quantity" name="quantity" class="form-control" id="sp-quantity">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
              </div>
              <div class="col-sm-8">
                <div class="pt-4 hidden-sm-up"></div>
                <button class="btn btn-primary btn-block my-0" id="AddtoCart" data-type="1" data-cart="{{ url('cart').'/'.$info_Product->slug}}" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="material-icons check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add To Cart</button>
              </div>
            </div>
            <ul class="list-unstyled text-sm mb-4">
              <li><span class='text-dark text-medium'>SKU:</span> {{ $info_Product->sku }}</li>
              <li><span class='text-dark text-medium'>Category:</span> <a href='{{ url('/').'/'.$info_Product->Subcategory()->First()->slug }}' class='navi-link'>{{ $info_Product->Subcategory()->First()->category }}</a></li>
            </ul>
            <div class="d-flex flex-wrap justify-content-between align-items-center"><!--<a class="btn btn-outline-secondary btn-sm text-danger" href="#"><i class="material-icons favorite_border"></i>&nbsp;Save To Wishlist</a>-->
              <div class="py-2"><span class="d-inline-block align-middle text-sm text-muted mr-2">Share:</span><a class="social-button shape-rounded sb-facebook" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(Request::url()) }}" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="socicon-facebook"></i></a><a class="social-button shape-rounded sb-twitter" href="https://twitter.com/intent/tweet?url={{ urlencode(Request::url()) }}" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="socicon-twitter"></i></a><a class="social-button shape-rounded sb-google-plus" href="https://plus.google.com/share?url={{ urlencode(Request::url()) }}" data-toggle="tooltip" data-placement="top" title="Google +"><i class="socicon-googleplus"></i></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container padding-bottom-3x pt-5 mb-1">
  <!-- Details-->
  <div class="row pt-2">
    <div class="col-md-6">
      <h3>Details</h3>
      <p class="text-sm mb-4">{!! str_replace("|", "<br />", $info_Product->description) !!}</p>
      <h3>Features</h3>
      <p class="text-sm mb-4">{!! str_replace("|", "<br />", $info_Product->feature) !!}</p>
    </div>
    <div class="col-md-6">
      <h3>Specifications</h3>
      <ul class="list-unstyled text-sm mb-4">
        <li><strong>Weight:</strong> {{ $info_Product->weight }}</li>
        <li><strong>Dimensions:</strong> {{ $info_Product->dimension }}</li>
        <li><strong>Materials:</strong> {{ $info_Product->material }}</li>
        <li><strong>Colors:</strong> {{ implode(",", $info_Product->Color()->Pluck('color')->ToArray()) }}</li>
        <li><strong>Brand:</strong> 
			@if($info_Product->Brand()->First())
            	{{ $info_Product->Brand()->First()->name }}
            @endif
        </li>
        <li><strong>Manufacturer:</strong> {{ $info_Product->manufacturer }}</li>
      </ul>
      <!--<h3>Shipping Options:</h3>
      <ul class="list-unstyled text-sm mb-4">
        <li><strong>Courier:</strong> 2 - 4 days, $22.50</li>
        <li><strong>Local Shipping:</strong> up to one week, $10.00</li>
        <li><strong>UPS Ground Shipping:</strong> 4 - 6 days, $18.00</li>
        <li><strong>Unishop Global Export:</strong> 3 - 4 days, $25.00</li>
      </ul>-->
    </div>
  </div>
  <!-- Reviews-->
  <div class="row mt-30">
    <div class="col-md-4 mb-4">
      <div class="card border-default">
        <div class="card-body">
          <div class="text-center">
            <div class="d-inline align-baseline display-2 mr-1">{{ number_format($info_Product->Review->avg('rating'),1)}}</div>
            @if($info_Product->Review->count()>0)
            	<div class="d-inline align-baseline text-sm text-warning mr-1">
                <?php
					$rating = round($info_Product->Review->avg('rating'));
					$Empty = 5;
					for($i=1; $i<=$rating; $i++)
					{
						$Empty--;
				?>
					<i class="material-icons star"></i>
				<?php
					}
				?>
                <?php
					for($i=1; $i<=$Empty; $i++)
					{
				?>
					<i class="material-icons star_border"></i>
				<?php
					}
				?>
                </div>
            @else
            	<div class="d-inline align-baseline text-sm text-warning mr-1"><i class="material-icons star_border"></i><i class="material-icons star_border"></i><i class="material-icons star_border"></i><i class="material-icons star_border"></i><i class="material-icons star_border"></i></div>
            @endif
          </div>
          <div class="pt-3">
            <label class="text-medium text-sm">5 stars <span class='text-muted'>- {{ $info_Product->Review->Where('rating',5)->Count() }}</span></label>
            <div class="progress margin-bottom-1x">
              <div class="progress-bar bg-warning" role="progressbar" style="width: {{ ($info_Product->Review->Count()>0?round(($info_Product->Review->Where('rating',5)->Count()/$info_Product->Review->Count())*100):0) }}%; height: 2px;" aria-valuenow="{{ ($info_Product->Review->Count()>0?round(($info_Product->Review->Where('rating',5)->Count()/$info_Product->Review->Count())*100):0) }}" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <label class="text-medium text-sm">4 stars <span class='text-muted'>- {{ $info_Product->Review->Where('rating',4)->Count() }}</span></label>
            <div class="progress margin-bottom-1x">
              <div class="progress-bar bg-warning" role="progressbar" style="width: {{ ($info_Product->Review->Count()>0?round(($info_Product->Review->Where('rating',4)->Count()/$info_Product->Review->Count())*100):0) }}%; height: 2px;" aria-valuenow="{{ ($info_Product->Review->Count()>0?round(($info_Product->Review->Where('rating',4)->Count()/$info_Product->Review->Count())*100):0) }}" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <label class="text-medium text-sm">3 stars <span class='text-muted'>- {{ $info_Product->Review->Where('rating',3)->Count() }}</span></label>
            <div class="progress margin-bottom-1x">
              <div class="progress-bar bg-warning" role="progressbar" style="width: {{ ($info_Product->Review->Count()>0?round(($info_Product->Review->Where('rating',3)->Count()/$info_Product->Review->Count())*100):0) }}%; height: 2px;" aria-valuenow="{{ ($info_Product->Review->Count()>0?round(($info_Product->Review->Where('rating',3)->Count()/$info_Product->Review->Count())*100):0) }}" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <label class="text-medium text-sm">2 stars <span class='text-muted'>- {{ $info_Product->Review->Where('rating',2)->Count() }}</span></label>
            <div class="progress margin-bottom-1x">
              <div class="progress-bar bg-warning" role="progressbar" style="width: {{ ($info_Product->Review->Count()>0?round(($info_Product->Review->Where('rating',2)->Count()/$info_Product->Review->Count())*100):0) }}%; height: 2px;" aria-valuenow="{{ ($info_Product->Review->Count()>0?round(($info_Product->Review->Where('rating',2)->Count()/$info_Product->Review->Count())*100):0) }}" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <label class="text-medium text-sm">1 star <span class='text-muted'>- {{ $info_Product->Review->Where('rating',1)->Count() }}</span></label>
            <div class="progress mb-2">
              <div class="progress-bar bg-warning" role="progressbar" style="width: {{ ($info_Product->Review->Count()>0?round(($info_Product->Review->Where('rating',1)->Count()/$info_Product->Review->Count())*100):0) }}%; height: 2px;" aria-valuenow="{{ ($info_Product->Review->Count()>0?round(($info_Product->Review->Where('rating',1)->Count()/$info_Product->Review->Count())*100):0) }}" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
          </div>
          <div class="pt-2"><a class="btn btn-warning btn-block" href="#" data-toggle="modal" data-target="#leaveReview">Leave a Review</a></div>
        </div>
      </div>
    </div>
    <div class="col-md-8" style="height:500px; overflow:scroll;">
      <h3 class="padding-bottom-1x">Latest Reviews</h3>
      @foreach($info_Product->Review as $review)
      <!-- Review-->
      <div class="comment">
        <div class="comment-body">
          <div class="comment-header d-flex flex-wrap justify-content-between">
            <h4 class="comment-title">by {{ $review->name }}</h4>
            <div class="mb-2">
              <div class="product-rating text-warning">
              <?php
					$Empty = 5;
					for($i=1; $i<=$review->rating; $i++)
					{
						$Empty--;
				?>
					<i class="material-icons star"></i>
				<?php
					}
				?>
                <?php
					for($i=1; $i<=$Empty; $i++)
					{
				?>
					<i class="material-icons star_border"></i>
				<?php
					}
				?>
              </div>
            </div>
          </div>
          <p class="comment-text">{{ $review->message }}</p>
          <div class="comment-footer"><span class="comment-meta">{{ date_format($review->created_at , 'd M Y h:m') }}</span></div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
  <!-- Related Products Carousel-->
  <h3 class="text-center padding-top-2x mt-2 padding-bottom-1x">You May Also Like</h3>
  <!-- Carousel-->
  <div class="owl-carousel" data-owl-carousel="{ &quot;nav&quot;: false, &quot;dots&quot;: true, &quot;margin&quot;: 30, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1},&quot;576&quot;:{&quot;items&quot;:2},&quot;768&quot;:{&quot;items&quot;:3},&quot;991&quot;:{&quot;items&quot;:4},&quot;1200&quot;:{&quot;items&quot;:4}} }">
    @foreach($info_Products as $info_Product)
    <!-- Product-->
    <div class="product-card">
      <div class="product-card-thumb"> <span class="product-badge text-danger">Sale</span><a class="product-card-link" href="{{ url('/product/').'/'.$info_Product->slug }}"></a><img src="{{ $info_Product->a_img }}" alt="Product">
          <div class="product-card-buttons">
            <a href="{{ url('/product/').'/'.$info_Product->slug }}" class="btn btn-white btn-sm" data-toggle="tooltip" title="Detail"><i class="material-icons pageview"></i></a>
            <button id="AddtoCart" class="btn btn-primary btn-sm" data-type="2" data-cart="{{ url('cart').'/'.$info_Product->slug}}" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="material-icons check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>
          </div>
        </div>
        <div class="product-card-details">
          <h3 class="product-card-title"><a href="{{ url('/product/').'/'.$info_Product->slug }}">{{ $info_Product->title }}</a></h3>
          <h4 class="product-card-price">
            @if($info_Product->price_2!="")
            <del>${{ $info_Product->price_2 }}</del>
            @endif
            ${{ $info_Product->price }}
          </h4>
        </div>
    </div>
    @endforeach
  </div>
</div>

<!-- Photoswipe container-->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="pswp__bg"></div>
  <div class="pswp__scroll-wrap">
    <div class="pswp__container">
      <div class="pswp__item"></div>
      <div class="pswp__item"></div>
      <div class="pswp__item"></div>
    </div>
    <div class="pswp__ui pswp__ui--hidden">
      <div class="pswp__top-bar">
        <div class="pswp__counter"></div>
        <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
        <button class="pswp__button pswp__button--share" title="Share"></button>
        <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
        <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
        <div class="pswp__preloader">
          <div class="pswp__preloader__icn">
            <div class="pswp__preloader__cut">
              <div class="pswp__preloader__donut"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
        <div class="pswp__share-tooltip"></div>
      </div>
      <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
      <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
      <div class="pswp__caption">
        <div class="pswp__caption__center"></div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('scripts')
<script language="javascript">
	$(document).on('click', "#AddtoCart[data-cart]", function () {		
		var url = $(this).data('cart');
		var type = $(this).data('type');
		var color = "";
		var size = "";
		var quantity = "";
		if(type==1)
		{
			color = $('#color option:selected').val();
			size = $('#size option:selected').val();
			quantity = $('#quantity option:selected').val();
		}
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'json',
			data: {"color": color, "size": size, "quantity": quantity, "_token": "{{ csrf_token() }}" , submit: true},
			success: function(data)
			{
				var url = "{{ url('/').'/' }}";
				var urlImage = "";
				var cartUrl = "{{ url('/cart') }}";
				var checkoutUrl = "{{ url('/checkout') }}";
				var response = '';
				for(var x in data.cartsData) 
				{						
					
					response = response + '<tr><td><div class="product-item"><a class="product-thumb" href="'+url+'product/'+data.cartsData[x]['slug']+'"><img src="'+urlImage+data.cartsData[x]['image']+'" alt="Product"></a><div class="product-info"><h4 class="product-title"><a href="'+url+'product/'+data.cartsData[x]['slug']+'">'+data.cartsData[x]['name']+'</a></h4><span><em>Price:</em> $'+data.cartsData[x]['price']+'</span><span><em>Quantity:</em> '+data.cartsData[x]['qty']+'</span></div></div></td><td class="text-center"><a class="remove-from-cart" data-cart="'+data.cartsData[x]['id']+'" href="#"><i class="material-icons icon_close"></i></a></td></tr>';	
				}
				$('.cart-subtotal').html(data.totalAmount);
				$('.cart-data').html(response);
				$('.cart-count').html(data.counts);
				$('#cartAdded').val(1);	
			},
			error: function (result, status, err) {
				console.log(result.responseText);
			}
		});
	});
</script>
@endpush