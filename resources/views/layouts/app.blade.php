<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Custom Home Tex
    </title>
    <!-- SEO Meta Tags-->
    <meta name="description" content="Custom Home Tex">
    <meta name="keywords" content="shop, e-commerce, modern, flat style, online store, business, mobile">
    <meta name="author" content="Rokaux">
    <!-- Mobile Specific Meta Tag-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Favicon and Apple Icons-->
    <link rel="icon" type="image/x-icon" href="{{ asset("home/favicon.ico") }}">
    <link rel="icon" type="image/png" href="{{ asset("home/favicon.png") }}">
    <link rel="apple-touch-icon" href="{{ asset("home/touch-icon-iphone.png") }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset("home/touch-icon-ipad.png") }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset("home/touch-icon-iphone-retina.png") }}">
    <link rel="apple-touch-icon" sizes="167x167" href="{{ asset("home/touch-icon-ipad-retina.png") }}">
    <!-- Vendor Styles including: Bootstrap, Font Icons, Plugins, etc.-->
    <link rel="stylesheet" media="screen" href="{{ asset("home/css/vendor.min.css") }}">
    <!-- Main Template Styles-->
    <link id="mainStyles" rel="stylesheet" media="screen" href="{{ asset("home/css/styles.min.css") }}">
    <link id="mainStyles" rel="stylesheet" media="screen" href="{{ asset("home/css/custom.css") }}">
    <!-- Modernizr-->
    <script src="{{ asset("home/js/modernizr.min.js") }}"></script>
	@stack('styles')
  </head>
  <!-- Body-->
  <body>
    
    <!-- Navbar-->
    <!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
    <header class="navbar navbar-sticky">
      <!-- Site Branding-->
      <div class="site-branding" style="padding: 10px 0 25px 30px;"><a class="site-logo hidden-xs-down" href="{{ asset("/") }}"><img src="{{ asset("home/images/final.png") }}" alt="Unishop"></a><a class="site-logo logo-sm hidden-sm-up" href="{{ asset("/") }}"><img src="{{ asset("home/images/final.png") }}" alt="Unishop"></a>
        <!--<div class="lang-currency-switcher">
          <div class="lang-currency-toggle"><img src="{{ asset("home/img/flags/GB.png") }}" alt="English"><span>USD</span><i class="material-icons arrow_drop_down"></i>
            <ul class="lang-currency-dropdown">
              <li>
                <select class="form-control form-control-sm">
                  <option value="usd">$ USD</option>
                  <option value="usd">€ EUR</option>
                  <option value="usd">£ UKP</option>
                  <option value="usd">¥ JPY</option>
                </select>
              </li>
              <li><a href="home/#"><img src="{{ asset("home/img/flags/FR.png") }}" alt="Français">Français</a></li>
              <li><a href="home/#"><img src="{{ asset("home/img/flags/DE.png") }}" alt="Deutsch">Deutsch</a></li>
              <li><a href="home/#"><img src="{{ asset("home/img/flags/IT.png") }}" alt="Italiano">Italiano</a></li>
            </ul>
          </div>
        </div>-->
      </div>
      <!-- Main Navigation-->
      <nav class="site-menu">
        <ul>
          <li class="active"><a href="{{ url("/") }}"><span>Home</span></a></li>
          <li><a href="{{ asset("home/shop-boxed-ls.html") }}"><span>Shop</span></a>
            <ul class="sub-menu">
              	<li><a href="#">Shop Categories</a></li>
                <li><a href="{{ url('product-category/clothing-for-men') }}">Clothing for men</a></li>
                <li><a href="{{ url('product-category/clothing-for-women') }}">Clothing for women</a></li>
                <li><a href="{{ url('product-category/beauty-personal-care') }}">Beauty &#038; Personal Care</a></li>
                <li><a href="{{ url('product-category/shoes') }}">Shoes</a></li>
                <li><a href="{{ url('product-category/jewelry') }}">Jewelry</a></li>
                <li><a href="{{ url('product-category/outdoor-gears') }}">Outdoor Gears</a></li>
                <li><a href="{{ url('product-category/home-kitchen') }}">Home &#038; Kitchen</a></li>
                <li><a href="{{ url('product-category/pet-care-supplies') }}">Pet Care &#038; Supplies</a></li>
                <li><a href="{{ url('product-category/baby-care') }}">Baby</a></li>
                <li><a href="{{ url('product-category/kids') }}">Kids</a></li>
                <li><a href="{{ url('product-category/teens') }}">Teens</a></li>
            </ul>
          </li>
          <li class=""><a href="{{ url("/about") }}"><span>About Us</span></a></li>
          <li class=""><a href="{{ url("/faqs") }}"><span>F.A.Q.s</span></a></li>
          <li class=""><a href="{{ url("/contact") }}"><span>Contact</span></a></li>
        </ul>
      </nav>
      <!-- Toolbar-->
      <div class="toolbar">
        <div class="inner">
        <a class="toolbar-toggle mobile-menu-toggle" href="#mobileMenu"><i class="material-icons menu"></i></a><a class="toolbar-toggle search-toggle" href="#search"><i class="material-icons search"></i></a>
        <!--<a class="toolbar-toggle" href="#account"><i class="material-icons person"></i></a>-->
        <a class="toolbar-toggle" href="#cart"><i><span class="material-icons shopping_basket"></span><span class="count cart-count">{{ Cart::count() }}</span></i></a></div>
        <!-- Toolbar Dropdown-->
        <div class="toolbar-dropdown">
          <!-- Mobile Menu Section-->
          <div class="toolbar-section" id="mobileMenu">
            <!-- Currency / Language Siwtcher-->
            <!--<div class="text-center mb-4">
              <div class="lang-currency-switcher">
                <div class="lang-currency-toggle"><img src="{{ asset("home/img/flags/GB.png") }}" alt="English"><span>USD</span><i class="material-icons arrow_drop_down"></i>
                  <ul class="lang-currency-dropdown">
                    <li>
                      <select class="form-control form-control-sm">
                        <option value="usd">$ USD</option>
                        <option value="usd">€ EUR</option>
                        <option value="usd">£ UKP</option>
                        <option value="usd">¥ JPY</option>
                      </select>
                    </li>
                    <li><a href="#"><img src="{{ asset("home/img/flags/FR.png") }}" alt="Français">Français</a></li>
                    <li><a href="#"><img src="{{ asset("home/img/flags/DE.png") }}" alt="Deutsch">Deutsch</a></li>
                    <li><a href="#"><img src="{{ asset("home/img/flags/IT.png") }}" alt="Italiano">Italiano</a></li>
                  </ul>
                </div>
              </div>
            </div>-->
            <!-- Search Box-->
            <!--<form class="input-group form-group" method="get"><span class="input-group-btn">
                <button type="submit"><i class="material-icons search"></i></button></span>
              <input class="form-control" type="search" placeholder="Search website">
            </form>-->
            <!-- Slideable (Mobile) Menu-->

            <nav class="slideable-menu mt-4">
              <ul class="menu">
                <li class="active"><span><a href="{{ url("/") }}"><span>Home</span></a></span></li>
                <li class="has-children"><span><a href="#!"><span>Shop</span></a><span class="sub-menu-toggle"></span></span>
                  <ul class="slideable-submenu">
                    <li><a href="#">Shop Categories</a></li>
                    <li><a href="{{ url('product-category/clothing-for-men') }}">Clothing for men</a></li>
                    <li><a href="{{ url('product-category/clothing-for-women') }}">Clothing for women</a></li>
                    <li><a href="{{ url('product-category/beauty-personal-care') }}">Beauty &#038; Personal Care</a></li>
                    <li><a href="{{ url('product-category/shoes') }}">Shoes</a></li>
                    <li><a href="{{ url('product-category/jewelry') }}">Jewelry</a></li>
                    <li><a href="{{ url('product-category/outdoor-gears') }}">Outdoor Gears</a></li>
                    <li><a href="{{ url('product-category/home-kitchen') }}">Home &#038; Kitchen</a></li>
                    <li><a href="{{ url('product-category/pet-care-supplies') }}">Pet Care &#038; Supplies</a></li>
                    <li><a href="{{ url('product-category/baby-care') }}">Baby</a></li>
                    <li><a href="{{ url('product-category/kids') }}">Kids</a></li>
                    <li><a href="{{ url('product-category/teens') }}">Teens</a></li>
                  </ul>
                </li>
                <li class=""><span><a href="{{ url("/about") }}"><span>About Us</span></a></span></li>
                <li class=""><span><a href="{{ url("/faqs") }}"><span>F.A.Q.s</span></a></span></li>
                <li class=""><span><a href="{{ url("/contact") }}"><span>Contact</span></a></span></li>
              </ul>
            </nav>
          </div>
          <!-- Search Section-->
          <div class="toolbar-section" id="search">
              <table style="width:100%">
              	<tr>
                	<td class="search-form">
                    	<input id="searchTxt" type="search" placeholder="Type search query"  style="padding-left:0px">
                    </td>
                    <td>
                    	<a id="searchBtn" class="btn btn-outline-primary" href="#!"><i class="material-icons search"></i></a>
                    </td>
                </tr>
              </table>
            <!-- Products-->
            <div class="widget widget-featured-products loadSearch">

            </div>
          </div>
          <!-- Account Section-->
          <!--<div class="toolbar-section" id="account">
            <ul class="nav nav-tabs nav-justified" role="tablist">
              <li class="nav-item"><a class="nav-link active" href="#login" data-toggle="tab" role="tab">Log In</a></li>
              <li class="nav-item"><a class="nav-link" href="#signup" data-toggle="tab" role="tab">Sign Up</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane fade show active" id="login" role="tabpanel">
                <form autocomplete="off" id="login-form">
                  <div class="form-group input-group">
                    <input class="form-control" type="email" placeholder="Email" required><span class="input-group-addon"><i class="material-icons mail"></i></span>
                  </div>
                  <div class="form-group input-group">
                    <input class="form-control" type="password" placeholder="Password" required><span class="input-group-addon"><i class="material-icons lock"></i></span>
                  </div>
                  <div class="custom-control custom-checkbox form-group">
                    <input class="custom-control-input" type="checkbox" id="logged" checked>
                    <label class="custom-control-label" for="logged">Keep me logged in</label>
                  </div>
                  <button class="btn btn-primary btn-block" type="submit">Log In</button>
                </form>
              </div>
              <div class="tab-pane fade" id="signup" role="tabpanel">
                <form autocomplete="off" id="signup-form">
                  <div class="form-group">
                    <input class="form-control" type="text" placeholder="Full Name" required>
                  </div>
                  <div class="form-group">
                    <input class="form-control" type="email" placeholder="Email" required>
                  </div>
                  <div class="form-group">
                    <input class="form-control" type="password" placeholder="Password" required>
                  </div>
                  <div class="form-group">
                    <input class="form-control" type="password" placeholder="Confirm Password" required>
                  </div>
                  <button class="btn btn-primary btn-block" type="submit">Sign Up</button>
                  <p class="text-muted text-sm mt-4">OR sign up with your social account</p><a class="media-btn media-facebook" href="#"><i class="socicon-facebook"></i><span>Signup with Facebook</span></a><a class="media-btn media-google" href="#"><i class="socicon-googleplus"></i><span>Signup with Google+</span></a><a class="media-btn media-twitter" href="#"><i class="socicon-twitter"></i><span>Signup with Twitter</span></a>
                </form>
              </div>
            </div>
          </div>-->
          <!-- Shopping Cart Section-->
          <div class="toolbar-section" id="cart">
            <div class="table-responsive shopping-cart mb-0">
              <table class="table">
                <thead>
                  <tr>
                    <th colspan="2">
                      <div class="d-flex justify-content-between align-items-center">Products<a class="navi-link text-uppercase" href="{{ url("/cart") }}"><span class="text-xxs">Expand Cart</span><i class="material-icons keyboard_arrow_right"></i></a></div>
                    </th>
                  </tr>
                </thead>
                <tbody class="cart-data">
                  @foreach(Cart::content() as $row)
                  	<tr>
                        <td>
                          <div class="product-item"><a class="product-thumb" href="{{ url('/product/').'/'.$row->options->slug }}"><img src="{{ $row->options->image }}" alt="Product"></a>
                            <div class="product-info">
                              <h4 class="product-title"><a href="{{ url('/product/').'/'.$row->options->slug }}">{{ $row->name }}</a></h4><span><em>Price:</em> ${{ $row->price }}</span><span><em>Quantity:</em> {{ $row->qty }}</span>
                            </div>
                          </div>
                        </td>
                        <td class="text-center"><a class="remove-from-cart" data-cart="{{ $row->id }}" href="#"><i class="material-icons icon_close"></i></a></td>
                     </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <hr class="mb-3">
            <div class="d-flex flex-wrap justify-content-between align-items-center">
              <div class="pr-2 py-1 text-sm">Subtotal: <span class='text-dark text-medium cart-subtotal'>${{ Cart::subtotal() }}</span></div><a class="btn btn-sm btn-success mb-0 mr-0" href="{{ url("/checkout") }}">Checkout</a>
            </div>
          </div>
        </div>
      </div>
    </header>
    <!-- Page Content-->
    @yield('content')
    <!-- Site Footer-->
    <footer class="site-footer">
      <div class="column text-center">
        <p class="text-sm mb-4">Need Support? Call<span class="text-primary">&nbsp;001 (917) 555-4836</span></p><a class="social-button sb-skype" href="#" data-toggle="tooltip" data-placement="top" title="Skype"><i class="socicon-skype"></i></a><a class="social-button sb-facebook" href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="socicon-facebook"></i></a><a class="social-button sb-google-plus" href="#" data-toggle="tooltip" data-placement="top" title="Google +"><i class="socicon-googleplus"></i></a><a class="social-button sb-twitter" href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="socicon-twitter"></i></a><a class="social-button sb-instagram" href="#" data-toggle="tooltip" data-placement="top" title="Instagram"><i class="socicon-instagram"></i></a>
        <p class="text-xxs text-muted mb-0 mt-3">© All rights CustomHomeTex.</p>
        <p class="text-xxs text-muted mb-0 mt-3"><a href="{{ url("/terms-conditions/") }}">Terms &amp; Conditions</a> | <a href="{{ url("/privacy-policy/") }}">Privacy Policy</a> | <a href="{{ url("/returns-and-exchanges/") }}">Returns &amp; Exchanges</a></p>
      </div>
      <div class="column">
        <h3 class="widget-title text-center">Subscription<small>To receive latest offers and discounts from the shop.</small></h3>
        <form class="subscribe-form input-group" action="//rokaux.us12.list-manage.com/subscribe/post?u=c7103e2c981361a6639545bd5&amp;id=1194bb7544" method="post" target="_blank" novalidate><span class="input-group-btn">
            <button type="submit"><i class="material-icons send"></i></button></span>
          <input class="form-control" type="email" name="EMAIL" placeholder="Your e-mail">
          <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
          <div style="position: absolute; left: -5000px;" aria-hidden="true">
            <input type="text" name="b_c7103e2c981361a6639545bd5_1194bb7544" tabindex="-1" value>
          </div>
        </form>
      </div>
      <div class="column">
        <h3 class="widget-title text-center">Payment Methods<small>We support one of the following payment methods.</small></h3>
        <div class="footer-cards"><img src="{{ asset("home/img/cards.png") }}" alt="Payment Methods">
        </div>
      </div>
    </footer>
    <!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="material-icons trending_flat"></i></a>
    <!-- Backdrop-->
    <div class="site-backdrop"></div>
    <!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
    <script src="{{ asset("home/js/vendor.min.js") }}"></script>
    <script src="{{ asset("home/js/scripts.min.js") }}"></script>
    <script>
		$("#searchBtn").on('click', function () {	
			var searchTxt = $("#searchTxt").val();
			if(searchTxt=="")
			{
				return;
			}
			var url = "{{ url('search-product/').'/' }}"+searchTxt;
			//alert(url);
			$('.loadSearch').html("<img src='{{ url('/home/img/').'/'.'loader.gif' }}' />");
			$.ajax({
				url: url,
				type: 'POST',
				dataType: 'json',
				data: {"_token": "{{ csrf_token() }}" , submit: true},
				success: function(data)
				{
					var url = "{{ url('/').'/' }}";
					var urlImage = "{{ url('images/products/').'/' }}";
					var response = '<h3 class="widget-title">Found in Products</h3>';
					for(var x in data.searchData) 
					{						
						response = response + '<div class="entry"><div class="entry-thumb"><a href="'+url+'product/'+data.searchData[x]['slug']+'"><img src="'+urlImage+data.searchData[x]['image']+'" alt="Product"></a></div><div class="entry-content"><h4 class="entry-title"><a href="'+url+'product/'+data.searchData[x]['slug']+'"> <span>'+data.searchData[x]['title']+'</span></a></h4><span class="entry-meta">$'+data.searchData[x]['price']+'</span></div></div>';
					}
					$('.loadSearch').html("");
					$('.loadSearch').html(response);
				},
				error: function (result, status, err) {
					alert(result.responseText);
				}
			});
		});
		$(document).on('click', ".remove-from-cart[data-cart]", function () {	
			var url = "{{ url('remove-cart/').'/' }}"+$(this).data('cart');
			$.ajax({
				url: url,
				type: 'POST',
				dataType: 'json',
				data: {"_token": "{{ csrf_token() }}" , submit: true},
				success: function(data)
				{
					var url = "{{ url('/').'/' }}";
					var urlImage = "";
					var cartUrl = "{{ url('/cart') }}";
					var checkoutUrl = "{{ url('/checkout') }}";
					var response = '';
					for(var x in data.cartsData) 
					{						
						
						response = response + '<tr><td><div class="product-item"><a class="product-thumb" href="'+url+'product/'+data.cartsData[x]['slug']+'"><img src="'+urlImage+data.cartsData[x]['image']+'" alt="Product"></a><div class="product-info"><h4 class="product-title"><a href="'+url+'product/'+data.cartsData[x]['slug']+'">'+data.cartsData[x]['name']+'</a></h4><span><em>Price:</em> $'+data.cartsData[x]['price']+'</span><span><em>Quantity:</em> '+data.cartsData[x]['qty']+'</span></div></div></td><td class="text-center"><a class="remove-from-cart" data-cart="'+data.cartsData[x]['id']+'" href="#"><i class="material-icons icon_close"></i></a></td></tr>';		
					}
					$('.cart-subtotal').html(data.totalAmount);
					$('.cart-data').html(response);
					$('.cart-count').html(data.counts);
					$('#cartAdded').val(1);	
				},
				error: function (result, status, err) {
					console.log(result.responseText);
				}
			});
		});
	</script>
	@stack('scripts')
  </body>
</html>