<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
			$table->string('parent')->nullable()->default(null);
			$table->string('slug', 100)->index();
            $table->string('title', 100);
			 $table->string('sku', 45);
            $table->mediumText('description');
			$table->mediumText('feature');
            $table->string('a_img')->nullable()->default(null);
            $table->string('b_img')->nullable()->default(null);
            $table->string('c_img')->nullable()->default(null);
			$table->string('d_img')->nullable()->default(null);
			$table->string('e_img')->nullable()->default(null);
            $table->integer('brand_id')->unsigned()->index()->nullable();
            $table->foreign('brand_id')
                ->references('id')
                ->on('brands')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->integer('category_id')->unsigned()->index();
            $table->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');
			$table->integer('sub_category_id')->unsigned()->index();
            $table->foreign('sub_category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->integer('quantity')->nullable()->default(null);
            $table->float('price');
			$table->float('price_2')->nullable();
			$table->float('shipping_cost')->nullable()->default(0);
			$table->string('warranty', '50')->nullable()->default(null);
			$table->string('weight', 50)->nullable()->default(null);
			$table->string('dimension', 50)->nullable()->default(null);
			$table->string('material', 50)->nullable()->default(null);
			$table->string('manufacturer', 50)->nullable()->default(null);
			$table->smallInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
