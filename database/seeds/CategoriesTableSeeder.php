<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();
		DB::table('categories')->insert(['category' => 'SHOP']);
		
		DB::table('categories')->insert(['category' => 'Clothing for women', 'slug' => 'clothing-for-women',  'm_img' => 'clothing-for-woman-cateogry.jpg', 'parent_id' => 1]);
		DB::table('categories')->insert(['category' => 'Clothing for men', 'slug' => 'clothing-for-men',  'm_img' => 'clothing-for-man.jpg', 'parent_id' => 1]);
		DB::table('categories')->insert(['category' => 'Shoes', 'slug' => 'shoes',  'm_img' => 'shoe-category.jpg', 'parent_id' => 1]);
		DB::table('categories')->insert(['category' => 'Jewelry', 'slug' => 'jewelry',  'm_img' => 'jewelry.jpeg', 'parent_id' => 1]);
		DB::table('categories')->insert(['category' => 'Outdoor Gears', 'slug' => 'outdoor-gears',  'm_img' => 'outdoor-category.jpeg', 'parent_id' => 1]);
		DB::table('categories')->insert(['category' => 'Pet Care & Supplies', 'slug' => 'pet-care-supplies',  'm_img' => 'krista-mangulsone-53122-unsplash.jpg', 'parent_id' => 1]);
		DB::table('categories')->insert(['category' => 'Home & Kitchen', 'slug' => 'home-kitchen',  'm_img' => 'home-and-kitchen.jpg', 'parent_id' => 1]);
		DB::table('categories')->insert(['category' => 'Baby', 'slug' => 'baby-care',  'm_img' => 'baby-care.html', 'parent_id' => 1]);
		DB::table('categories')->insert(['category' => 'Beauty & Personal Care', 'slug' => 'beauty-personal-care',  'm_img' => 'beauty-personal-care-category.jpg', 'parent_id' => 1]);
		DB::table('categories')->insert(['category' => 'Signs & Displays', 'slug' => 'signs-displays',  'm_img' => 'AdobeStock_163763734.jpeg', 'parent_id' => 1]);
		DB::table('categories')->insert(['category' => 'Teens', 'slug' => 'teens',  'm_img' => 'teens.jpg', 'parent_id' => 1]);
		DB::table('categories')->insert(['category' => 'Kids', 'slug' => 'kids',  'm_img' => 'kids.jpg', 'parent_id' => 1]);
    }
}
