<?php

use Illuminate\Database\Seeder;

class ProductsSizesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=1; $i<5000; $i++)
		{
			DB::table('product_sizes')->insert(['product_id' => $i, 'size_id' => rand(1,110)],['product_id' => $i, 'size_id' => rand(111,220)]);
		}
    }
}
