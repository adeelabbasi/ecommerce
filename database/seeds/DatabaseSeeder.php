<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
		$this->call(UsersTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(StatesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
		$this->call(ColorsTableSeeder::class);
		$this->call(SizesTableSeeder::class);
		$this->call(CategoriesTableSeeder::class);
		$this->call(BrandsTableSeeder::class);
		//$this->call(ProductsTableSeeder::class);
		//$this->call(ProductsSizesTableSeeder::class);
		//$this->call(ProductsColorsTableSeeder::class);
    }
}
