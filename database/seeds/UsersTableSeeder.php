<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->delete();
		DB::table('admins')->insert(['name' => 'Adeel Abbasi', 'email' => 'adeel696@yahoo.com', 'password' => '$2y$10$vvTR6ABhGHqtyePfi0ve3.gkBQf6RZvDeGKg1wRPXyDr2CeSdWQyq', 'created_at' => date('Y-m-d')]);
    }
}
