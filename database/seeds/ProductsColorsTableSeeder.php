<?php

use Illuminate\Database\Seeder;

class ProductsColorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=1; $i<5000; $i++)
		{
			DB::table('product_colors')->insert(['product_id' => $i, 'color_id' => rand(1,70)],['product_id' => $i, 'color_id' => rand(71,140)]);
		}
    }
}
